select id_hero from HeroBaseInfo where name_dota_2 in ('Chaos Knight',
'Huskar',
'Lone Druid',
'Lycan',
'Templar Assassin',
'Ursa')

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_chaos_knight','- Storm Dispel Magic combined with Thunder Clap deals high damage against Chaos Knight illusions even in the late game.
- Chaos Knight reliance on physical damage is easily hindered by Drunken Brawler.
- Chaos Knight lack of area effect and mobility makes him easily overwhelmed by Primal Split.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_huskar','- Brewmaster can use Cyclone to disable Huskar healer for much longer than most other heroes can, to make Huskar vulnerable.
- Huskar has no way to stop Brewmaster from using Primal Split by himself, but can cut a good amount of his high health away if Huskar team wants to burst Brewmaster down during a different disable.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_lone_druid','- Brewmaster can make the Spirit Bear a much smaller threat with Cinder Brew.
- Cyclone removes the bear from a fight so that his team can easily focus on the hero.
- While most heroes would be worn down by a Spirit Bear Radiance (Active) icon.png Radiance in the time it takes to kill Lone Druid, Brewmaster is protected in Primal Split.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_lycan','- Brewmaster can waste a large portion of Lycan ultimate by putting him in Cyclone.
- Lycan has no way to stop Brewmaster from using Primal Split by himself.
- Lycan team often tries to push towers as a group, and Brewmaster excels at winning teamfights while trying to defend a tower.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_templar_assassin','- The three Primal Split brewlings all attacking Templar Assassin at once will remove her Refraction quickly, especially with Fire Permanent Immolation.
- Templar Assassin has no way to stop Brewmaster from using Primal Split by herself, and may get unlucky and miss her Meld if trying to burst him during her ally disable.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_ursa','- If Ursa uses Enrage Brewmaster can use Cyclone to render him harmless during his ultimate.
- Brewmaster has two spells to slow down Ursa, allowing his team to kite around him easily.','b','c');