select id_hero from HeroBaseInfo where name_dota_2 in
('Bane',
'Death Prophet',
'Lion',
'Outworld Devourer',
'Shadow Shaman',
'Skywrath Mage')

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_bane','- After Brewmaster has committed Thunder Clap and Cinder Brew to gank, Bane can use Nightmare on Brewmaster to wait out the debuffs, and might even turn the tables by buying time for allied reinforcements.
- Bane can also prevent Brewmaster from using Primal Split with his long disables.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_death_prophet','- Exorcism kills Primal Split units very quickly.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_lion','- Hex instant cast time will force Brewmaster into the awkward situation of needing some form of spell immunity or block to prevent being immediately hex upon blinking in, or wait until Lion has used all his crowd control before entering a fight. His direct magic burst also makes Brewmaster Drunken Brawler worthless versus Lion.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_outworld_devourer','- Outworld Devourer pure damage will mop up Primal Split units very quickly.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_shadow_shaman','- Hex instant cast time will force Brewmaster into the awkward situation of needing some form of spell immunity or block to prevent being immediately hex upon blinking in, or wait until Shaman has used all his crowd control before entering a fight.
- Brewmaster also has no defense against Shadow Shaman lockdowns.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_brewmaster','id_skywrath_mage','- Ancient Seal instant cast time will often silence Brewmaster before he can do anything.
- Brewmaster also has no defense against Skywrath Mage massive magical damage.','b','c');