select id_hero from HeroBaseInfo where name_dota_2 in
(
'Anti-Mage',
'Arc Warden',
'Axe',
'Bristleback',
'Elder Titan',
'Ember Spirit',
'Faceless Void',
'Invoker',
'Lone Druid',
'Meepo',
'Omniknight',
'Pudge',
'Tidehunter',
'Timbersaw',
'Tinker'
)
 
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_antimage','- Doom prevents Anti-Mage from maneuvering around a fight or escaping from an initiation using Blink. Additionally, Doom deals pure damage, which is not affected by Counterspell. This can quickly whittle away Anti-Mage naturally poor health pool.
- Be careful when using Doom as a well-timed Counterspell can block and reflect it back against Doom himself.
- An Aghanim Scepter upgraded Infernal Blade will also disable Mana Break and Counterspell, which makes Anti-Mage significantly less effective in fights and more susceptible to the high magical damage of Doom abilities and any allies.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_axe','- Infernal Blade deals scaling damage and is useful against high HP targets such as Axe.
- Doom has low attack speed because of his high Base-Attack-Time, poor agility gain and limited attack-speed granting items which are commonly built. This means Doom will trigger Counter Helix less often and thus is an unideal target for initiation.
- Doom has naturally high HP so it takes longer for Doom to be brought down to the killing threshold of Culling Blade.
- Axe is highly reliant on his abilities and items to be effective during a fight. Without the usage of Berserker Call and Blink Dagger, Axe offers very little in the way of damage.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_bristleback','- Infernal Blade deals scaling damage and is useful against high HP targets such as Bristleback.
- If Doom gets an Aghanim Scepter, he can disable Bristleback, taking away much of his durability.
- Doom prevents Bristleback from casting his two low cooldown abilities, which then prevents him from utilizing Warpath.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_elder_titan','- Infernal Blade deals scaling damage and is useful against high HP targets such as Elder Titan.
- Doom has very low base armor and naturally high HP, so he is not significantly affected by Natural Order.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_ember_spirit','- Ember Spirit cannot use his Activate Fire Remnant while Doomed to escape from Doom.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_void','- Faceless Void relies on Time Walk for mobility and survivability but he can be prevented from utilizing it through Doom.
- Doom has a high cooldown duration for every ability except Infernal Blade, so he is not significantly affected by Time Dilation.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_invoker','- Doom completely takes Invoker out of a team fight, a hero who relies on using abilities and items to contribute.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_lone_druid','Doom is potentially good versus every hero since Doom will render any hero helpless for a prolonged 15 seconds. This is particularly true for fragile heroes that heavily rely on mobility to stay alive.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_meepo','Doom is potentially good versus every hero since Doom will render any hero helpless for a prolonged 15 seconds. This is particularly true for fragile heroes that heavily rely on mobility to stay alive.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_pudge','- Infernal Blade deals high damage to Pudge due to his high strength grow with Flesh Heap.
- Using Doom with timing against Pudge who had just activated Rot would usually kill him.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_timbersaw','- Infernal Blade deals scaling damage and is useful against high HP targets such as Timbersaw.
- Timbersaw is extremely reliant on his abilities to be effective during a teamfight - both for mobility, damage and crowd control. Timbersaw offers very little impactful right-click damage, and thus Doom is particularly crippling against this hero.
- Additionally, when upgraded with Aghanim Scepter, Infernal Blade breaks Reactive Armor, which effectively removes the durability which Timbersaw is commonly known for.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_tinker','- Tinker is heavily dependent on active abilities and Doom prevents him from using any of them.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_tidehunter','- Infernal Blade deals scaling damage and is useful against high HP targets such as Tidehunter.
- If Doom gets an Aghanim Scepter, he can also disable Kraken Shell, reducing his survivability and tankiness.
- Doom prevents Tidehunter from casting his Gush, Anchor Smash and Ravage, making him useless in team fights.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_omniknight','- Doom applies a basic dispel upon cast, so it can remove Guardian Angel.
- Casting Doom on Omniknight will prevent him from healing allies with Purification or casting buffs like Heavenly Grace and Guardian Angel.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_arc_warden','- Arc Warden relies on items and abilities, as well as his Tempest Double, to deal damage to enemies. Doom effectively denies that and makes Arc Warden useless in fights.','b','c');

