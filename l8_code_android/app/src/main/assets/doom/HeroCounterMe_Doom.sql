select id_hero from HeroBaseInfo where name_dota_2 in ('Alchemist',
'Centaur Warrunner',
'Chen',
'Dazzle',
'Drow Ranger',
'IO',
'Lifestealer',
'Lone Druid',
'Medusa',
'Necrophos',
'Omniknight',
'Oracle',
'Phantom Lancer',
'Sniper',
'Terrorblade',
'Viper',
'Winter Wyvern',
'Witch Doctor',
'Wraith King')

 
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_alchemist','- Chemical Rage out-heals Doom to a large degree throughout all stages of the game. Should Alchemist be chemically enraged before being Doomed, Alchemist can become a difficult target to effectively lockdown and kill, thanks to the health regeneration and movement speed bonuses granted by his ultimate.
- Alchemist is an ineffective target for Doom. Alchemist is not totally reliant on his skills to be effective during a teamfight, due to the right-click nature of the hero.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_centaur_warrunner','- Stampede can be used to free an ally from the grasp of Doom. Without any means of effective lockdown, kills can be difficult to secure when against an enemy Centaur Warrunner.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_chen','Heroes with the ability to heal allies, or those with good defensive abilities','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_dazzle','Heroes with the ability to heal allies, or those with good defensive abilities','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_drow_ranger','- Drow Ranger can be difficult to get on top of, thanks to her Frost Arrows and Gust. With these two abilities, Drow Ranger is able to effectively 'kite' Doom and minimize his impact during team fights and laning stage.
- Much of Drow Ranger effectiveness comes from her passive, which is not disabled by Doom.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_life_stealer','- Feast deals damage and heals Lifestealer based on the target maximum HP, which is greatly effective against naturally high HP heroes such as Doom.
- Rage allows Lifestealer to become spell immune to negate the magical damage of Infernal Blade and Scorched Earth.
- Doom has no effective means of escape from Open Wounds. This is particularly devastating in lane, where Doom may have to play a much more passive, conservative play style as a result.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_lone_druid','- Lone Druid is essentially two heroes. Dooming one over the other still leaves Lone Druid or his bear able to do their job during a fight. Additionally, Lone Druid is not hindered as much as other heroes by the silence and mute of Doom since the hero relies more on attacks than abilities.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_medusa','- Doom does not disable Mana Shield. As such, Doom damage is largely mitigated by her mana shield. Additionally, Medusa is not reliant on her abilities to be effective during a teamfight, making Medusa an unproductive hero to target.
- Medusa is an ideal Linken Sphere carrier. Should one be purchased, and without any additional disables, Doom may find that few opportunities exist to effectively lockdown, disable, and kill Medusa.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_necrophos','- Due to Doom naturally high health, strength gain and common itemization for durability, Heartstopper Aura is particularly effective. The health loss is also large enough that, throughout all stages of the game, the aura effectively neuters the health regeneration granted by Devour.
- Necrophos can help to keep both himself and his allies alive during Doom through his natural regeneration granted by Heartstopper Aura (which is not disabled by Doom), or in the case of an ally, through his Death Pulse. Additionally, Necrophos is an ideal Mekansm carrier. Should one be bought, kills against Necropho team can be difficult to secure due to the damage-over-time nature of Doom and his abilities.
- Doom typically wants to be a hero that soaks attention from the enemy team and takes the role of an initiator and disabler. As such, Doom is an ideal target for Reaper Scythe, as it is likely Doom will be at the fore-front of any fight.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_phantom_lancer','- Phantom Lancer illusions can make it difficult to Doom the original. Should Doom figure out the original hero, the illusions can make it difficult to cast any abilities, as their hitboxes can be used to body-block the Phantom Lancer.
- Phantom Lancer is an ideal Diffusal Blade carrier. With it, he can quite easily deplete Doom already poor mana pool, potentially making him unable to cast his abilities to escape, retaliate or heal.
- Doom lacks any effective ability to clear waves of creeps and illusions, as the vast majority of his abilities are single-target. Without an answer, Doom may find it difficult to deal with Phantom Lancer presence.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_sniper','- Due to the nature of the hero, Sniper is typically situated in the back of the fight, potentially making him a difficult target to initiate on and fight.
- Sniper is largely a right-click hero, and as such is not as an effective of a Doom target as other heroes.
- Thanks to Shrapnel and Headshot, Sniper is able to effectively 'kite' Doom, preventing him from closing the distance between the two.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_terrorblade','Terrorblade can use Sunder to save his allies who are Doomed.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_viper','- The slow applied from Corrosive Skin offsets the movement speed bonus granted by Scorched Earth and the tick-based damage from Infernal Blade and Doom will continually slow Doom down. This allows Viper, much like Sniper, to kite Doom and keep him at a safe distance between the two. Additionally, Corrosive Skin applies an attack-speed slow which is fairly crippling to a hero such as Doom that, naturally, has a high base attack-time, poor agility gain and builds little attack-speed items.
- Since Doom does not disable auto-cast abilities, Viper Poison Attack will continue to work. Doom may find it hard to close the distance between himself and Viper, and thusly difficult to effectively lockdown and continually deal damage through his abilities.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_io','Heroes with the ability to heal allies, or those with good defensive abilities','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_witch_doctor','Heroes with the ability to heal allies, or those with good defensive abilities','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_wraith_king','- Wraith King has three passive abilities and only a single active ability, so Doom does not hinder him significantly.
- Doom expires upon death, so it is effectively removed after Reincarnation activates.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_omniknight','Heroes with the ability to heal allies, or those with good defensive abilities','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_oracle','Heroes with the ability to heal allies, or those with good defensive abilities','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_doom_bringer','id_winter_wyvern','Heroes with the ability to heal allies, or those with good defensive abilities','b','c');