SELECT id_item FROM ItemBaseInfo WHERE name in ('Mekansm',
'Guardian Greaves',
'Linken Sphere',
'Lotus Orb',
'Glimmer Cape',
'Orchid Malevolence')

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_doom_bringer','id_mekansm','- Mekansm provide burst-healing to allies, which is particularly effective against Doom and his damage-over-time abilities.','b','c');
INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_doom_bringer','id_orchid_malevolence','- Orchid Malevolence is an effective means of preventing Doom from initiating and casting his abilities during a fight, which he is highly reliant on to deal damage.','b','c');
INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_doom_bringer','id_linken_is_sphere','- Linken Sphere can block Doom. Doom only inherit means of breaking the Linken Sphere is through his Infernal Blade. An initiation created with Infernal Blade followed up by Doom takes a lengthy amount of time, and can potentially allow the enemy team to react.','b','c');
INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_doom_bringer','id_glimmer_cape','- Glimmer Cape can be used to protect an ally from the magical damage from both Infernal Blade and Scorched Earth, and can potentially allow the ally to escape Doom grasp.','b','c');
INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_doom_bringer','id_lotus_orb','- Lotus Orb applied at the right time can reflect Doom back onto himself.','b','c');