SELECT id_hero FROM HeroBaseInfo WHERE name_dota_2 in ('Viper',
'Clinkz',
'Pugna' )



INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_ancient_apparition','Ice Blast cancels out Chemical Rage rapid health regeneration. It can also take advantage of Alchemist Armlet of Mordiggian toggling, as it will make HP drain insanely fast, essentially killing the bearer in no time.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_ember_spirit','Ember magic damage build is able to burst down Alchemist due to his low strength gain and Alchemist normal item pick up.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_enchantress','Untouchable greatly reduces Chemical Rage attack speed advantage.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_invoker','Quas- Exort Invoker can win the mid lane easily against Alchemist because Quas regeneration will nullify Acid Spray damage over time and Exort damage bonus will allow Invoker to out last hit Alchemist.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_kunkka','1. Tidebringer will deal lots of damage to Alchemist before he builds physical survivability items.
2. Torrent and X Marks the Spot can control Alchemist at all stages of the game before he has Black King Bar.
3. Ghostship rum buff will greatly reduce the Alchemist damage output in teamfights including his Radiance.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_life_stealer','1. Feast is good against high health strength heroes such as Alchemist.
2. Rage allows Lifestealer to not be targetable by Unstable Concoction, possibly making Alchemist stun himself.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_necrophos','1. Reaper Scythe can kill Alchemist before Chemical Rage has the time to heal him.
2. Ghost Shroud can prevent Alchemist from attacking Necrophos, as well as make him immune to Acid Spray and Unstable Concoction damage.
3. Heartstopper Aura will do damage as long as Necrophos is alive based on health percentage, which is a problem for a strength hero such as Alchemist.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_razor','1. Alchemist reliance on fast attack speed to do single-target damage makes him vulnerable to Static Link. It also goes through Black King Bar, which is a standard item on Alchemist.
2. Eye of the Storm is great against high health strength heroes as it reduces their survivability to physical damage.
3. Razor has early lane push to counter Acid Spray.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_slardar','1. Corrosive Haze followed by Slithereen Crush takes advantage of Alchemist main weakness, being physical damage before he has the change to improve his surviability with items.
2. Bash of the Deep will deal lots of damage during the laning phase.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_slark','1. Shadow Dance allows Slark to not be targeted by Unstable Concoction, making Alchemist stun himself and making himself vulnerable to the enemy.
2. Dark Pact also allows him to just purge off the stun if timed correctly.
3. Essence Shift will steal stats over long durations at a time should Alchemist lack the damage to kill Slark.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_ursa','1. Chemical Rage will rarely save Alchemist against Ursa Fury Swipes.
2. Enrage allows Ursa to both be a threat to Alchemist and avoid his over time damage.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_clinkz','Clinkz can use Burning Army in teamfights which will cause a problem for Alchemist.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_pugna','Pugna Decrepify makes sure that either Alchemist is unable to hit anyone while also being slowed and susceptible to magical damage, or can protect an ally from physical attacks. Besides, Pugna can push early on, when Alchemist is not strong enough to repel it, so by the time Alchemist comes online, he might be forced to sit in base.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_viper','Viper can bully Alchemist out of mid lane quite easily, forcing him to either retreat into jungle, or stay harassed in lane, and get very few last hits. Vipers break also completely disables Greevil Greed which prevents Alchemist to earn his amazing amounts of money.','b','c');
