SELECT id_hero FROM HeroBaseInfo WHERE name_dota_2 in ('Broodmother',
'Nature Prophet',
'Phoenix',
'Undying',
'Huskar')

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_broodmother','1. Alchemist can easily farm Spiderlings with Acid Spray and Greevil Greed.
2. Alchemist Chemical Rage nullifies Broodmother Insatiable Hunger','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_nature_prophet','Acid Spray and Greevil Greed will quickly destroy pushes with treants.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_huskar','Chemical Rage mitigates damage from Burning Spear.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_phoenix','Chemical Rage kills Phoenix very quickly in Supernova.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_undying','Chemical Rage will take down Tombstone in no time.','b','c');