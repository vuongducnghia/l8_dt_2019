SELECT id_item FROM ItemBaseInfo WHERE name in
('Assault Cuirass ',
'Shiva Guard ',
'Heaven Halberd',
'Scythe of Vyse',
'Spirit Vessel')

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_scythe_of_vyse','Scythe of Vyse disables Alchemist, making sure he will not be able to use his Chemical Rage to heal up, and allows heroes to burst him down fast.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_shiva_is_guard','Shiva Guard does the same thing as Assault Cuirass but it also reduces the attack speed, reducing the effectiveness of Chemical Rage.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_assault_cuirass','Assault Cuirass reduces Alchemist physical survivability and his teammates.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_heaven_is_halberd','Heaven Halberd applies an undispellable disarm so Alchemist cannot attack during Chemical Rage, forcing him to activate Black King Bar beforehand.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_alchemist','id_spirit_vessel','Spirit Vessel reduces Alchemist health regeneration.','b','c');






