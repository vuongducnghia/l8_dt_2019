select id_hero from HeroBaseInfo where name_dota_2 in ('Bane',
'Bounty Hunter',
'Disruptor',
'Lone Druid',
'Pudge',
'Rubick',
'Silencer',
'Slark',
'Techies',
'Ursa',
'Viper ',
'Lifestealer',
'Wraith King',
'Night Stalker',
'Troll Warlord ',
'Beastmaster',
'Bloodseeker',
'Clockwerk',
'Doom',
'Shadow Shaman',
'Drow Ranger',
'Necrophos',
'Crystal Maiden')

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_bane','- Phantasm illusions make it difficult for Bane to target Chaos Knight with Nightmare, Brain Sap and Fiend Grip.
- Chaos Bolt is a comparatively low cooldown tool to interrupt Fiend Grip.
- Nightmare can be transferred by Phantasm illusions.
- Single-target disablers will have difficulty locking down a Chaos Knight with Phantasm active.
- Innately fragile heroes will die almost instantly to Reality Rift while Chaos Knight has illusions active.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_beastmaster','- Single-target disablers will have difficulty locking down a Chaos Knight with Phantasm active.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_bloodseeker','- Single-target disablers will have difficulty locking down a Chaos Knight with Phantasm active.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_bounty_hunter','- Phantasm dispels Track, and the illusions make it difficult to Track the real Chaos Knight.
- Bounty Hunter is fragile and can easily be killed by Chaos Knight and Phantasm illusions.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_crystal_maiden','- Innately fragile heroes will die almost instantly to Reality Rift while Chaos Knight has illusions active.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_disruptor','- Phantasm removes Glimpse and dispels Thunder Strike. Additionally, the Phantasm illusions make it difficult to use Glimpse and Thunder Strike on the real Chaos Knight.
- Disruptor lack of mobility, combined with his lack of innate defense, sets him up for a Reality Rift and a beatdown by Chaos Knight and his illusions.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_doom_bringer','- Single-target disablers will have difficulty locking down a Chaos Knight with Phantasm active.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_drow_ranger','- Innately fragile heroes will die almost instantly to Reality Rift while Chaos Knight has illusions active.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_life_stealer','- Phantasm gives Chaos Knight a significant advantage against single-target damage dealers.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_lone_druid','- Reality Rift lets Chaos Knight bypass the Spirit Bear and jump onto Lone Druid.
- The Spirit Bear lack of scaling means it dies quickly to Chaos Knight during the late-game.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_necrophos','- Innately fragile heroes will die almost instantly to Reality Rift while Chaos Knight has illusions active.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_night_stalker','- Phantasm gives Chaos Knight a significant advantage against single-target damage dealers.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_pudge','- Phantasm illusions make it difficult to land Meat Hook and Dismember.
- Chaos Bolt is a comparatively low cooldown tool to interrupt Dismember.
- Chaos Strike lifesteal offsets Rot damage.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_clockwerk','- Single-target disablers will have difficulty locking down a Chaos Knight with Phantasm active.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_rubick','- While Chaos Knight can land far away from Rubick due to Telekinesis, it can be counteracted by Reality Rift.
- Phantasm dispels Fade Bolt damage reduction debuff.
- Rubick is fragile and can easily be killed by Chaos Knight and Phantasm illusions, before he retaliates.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_shadow_shaman','- Single-target disablers will have difficulty locking down a Chaos Knight with Phantasm active.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_silencer','- Casting Phantasm before a fight minimizes the disruption from Global Silence.
- Because Chaos Knight generally relies on attack damage, Silencer spells won not be as effective against him.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_slark','- Dark Pact is minimally effective against Phantasm illusions, and Slark lacks area damage otherwise.
- Pounce can be counteracted by Reality Rift.
- Slark is naturally frail and dies quickly to Chaos Knight burst damage before he can build Essence Shift stacks.
- Be aware, however, that Shadow Dance can make Slark hard to kill as he can not be targeted during its duration.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_techies','- Phantasm illusions can be used to push towers and force Techies to detonate Remote Mines.
- Phantasm illusions can be used to sweep Proximity Mines and Stasis Traps.
- Chaos Knight can easily kill a lone Techies before they Blast Off!.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_troll_warlord','- Phantasm gives Chaos Knight a significant advantage against single-target damage dealers.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_ursa','- Ursa cannot effectively fight against Phantasm illusions with his single-target damage.
- Chaos Bolt potentially long stun with Chaos Bolt gives Ursa little opportunity to Enrage.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_viper','- As Viper focuses on single-target damage, he has trouble fighting off Phantasm illusions.
- Reality Rift counteracts Viper slows and reduces his durability.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_wraith_king','- Phantasm gives Chaos Knight a significant advantage against single-target damage dealers.','b','c');