SELECT id_item FROM ItemBaseInfo WHERE name in ('Shiva Guard',
'Blade Mail',
'Butterfly',
'Ghost Scepter',
'Manta Style',
'Linken Sphere',
'Battle Fury')

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_chaos_knight','id_ghost_scepter','- Ghost Scepter and Ethereal Blade prevent Chaos Knight from focusing down a target by turning it ethereal.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_chaos_knight','id_battle_fury','- Battle Fury provides cleave damage, which is especially helpful to destroy Chaos Knight illusions for melee heroes relying on attack damage.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_chaos_knight','id_butterfly','- Butterfly allow heroes to evade Chaos Knight attacks, reducing his damage output.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_chaos_knight','id_blade_mail','- Blade Mail is a great item when timed right to destroy all the illusions instantly and it does a great job in countering Chaos Knight high damage output in general.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_chaos_knight','id_shiva_is_guard','- Shiva Guard provides armor and a negative attack speed aura, reducing the damage output of Chaos Knight and his illusions, and can slow and damage them with Arctic Blast.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_chaos_knight','id_manta_style','- Manta Style creates illusions that can make it difficult for Chaos Knight to initiate on the real hero.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_chaos_knight','id_linken_is_sphere','- Linken Sphere prevent Chaos Knight from using his targeted skills effectively.','b','c');







