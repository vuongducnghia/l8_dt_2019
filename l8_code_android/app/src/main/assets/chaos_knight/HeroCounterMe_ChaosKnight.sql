select id_hero from HeroBaseInfo where name_dota_2 in ('Abaddon',
'Tinker',
'Axe',
'Lion',
'Brewmaster',
'Dragon Knight',
'Earthshaker',
'Enigma',
'Jakiro',
'Lich',
'Magnus',
'Monkey King',
'Phantom Lancer',
'Omniknight',
'Pugna',
'Sand King',
'Sven',
'Tidehunter',
'Timbersaw',
'Winter Wyvern',
'Witch Doctor',
'Puck',
'Ember Spirit',
'Legion Commander',
'Phoenix',
'Naga Siren',
'Meepo',
'Broodmother')

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_abaddon','- Abaddon could punish Chaos Knight for using Phantasm and Reality Rift combo on him with his Aphotic Shield. Additionally, it provides extra protection against Chaos Knight potential to take Abaddon down quickly.
- Furthermore, Abaddon Borrowed Time makes it utterly impossible to burst Abaddon down.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_axe','- Berserker Call allows Axe to lock down Chaos Knight and his illusions.
- Counter Helix combined with Blade Mail icon.png Blade Mail clears illusions out in no time.
- Heroes with strong area damage to destroy Phantasm illusions.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_brewmaster','- Dispel Magic from Brewmaster Storm icon.png Storm can instantly kill the Phantasm army early game and can still deal a lot of damage to them late game, revealing the real Chaos Knight.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_broodmother','- Illusion-based and multi-unit heroes can take advantage of Chaos Knight lack of area damage.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_dragon_knight','- Dragon Blood armor bonus greatly reduces the damage dealt by Phantasm.
- Breathe Fire and Elder Dragon Form splash damage quickly destroy illusions and reduces their damage output.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_earthshaker','- Echo Slam will deal a colossal amount of damage to your army, as will Fissure at lower levels.
- The area stuns from Aftershock will always slow down your illusions, since they can not have spell immunity.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_ember_spirit','- Heroes with strong area damage to destroy Phantasm illusions.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_enigma','- Black Hole usually catches Chaos Knight with all his illusions.
- Midnight Pulse deals damage based on max health, quickly killing Phantasm illusions.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_jakiro','- Ice Path and Macropyre punish Reality Rift with a long stun and heavy damage across all illusions.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_legion_commander','- Heroes with strong area damage to destroy Phantasm illusions.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_lich','- Lich Chain Frost, especially after obtaining its respective talent that provides unlimited bounces, can take out all of Chaos Knight illusions quickly.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_lion','- Mana Drain is a real problem to Chaos Knight early game, considering his already low mana pool.
- Aghanim Scepter icon.png Aghanim Scepter upgraded Finger of Death is an AoE ability, which can deal a tremendous amount of damage to his illusions.
- Hex with a level 25 talent instantly disposes Chaos Knight illusions.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_magnus','- Using Reality Rift sets up a Reverse Polarity on Chaos Knight and his illusions.
- Shockwave and Skewer kill Phantasm illusions quickly, as can Empower cast on a melee carry.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_meepo','- Illusion-based and multi-unit heroes can take advantage of Chaos Knight lack of area damage.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_naga_siren','- Illusion-based and multi-unit heroes can take advantage of Chaos Knight lack of area damage.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_phantom_lancer','- Juxtapose allows Phantom Lancer to overwhelm Chaos Knight due to his single-target abilities.
- Doppelganger can be used to dodge Chaos Bolt.
- Spirit Lance can be used to harass Chaos Knight during the early game.
- Since Phantom Lancer build Diffusal Blade 1 icon.png Diffusal Blade often, it can quickly drain Chaos Knight of all his mana, leaving him unable to cast spells.
- Illusion-based and multi-unit heroes can take advantage of Chaos Knight lack of area damage.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_phoenix','- Heroes with strong area damage to destroy Phantasm illusions.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_puck','- Puck area of effect nukes in the form of Illusory Orb and Waning Rift clears Phantasm illusions from the early game to mid game.
- Phase Shift can be used to disjoint Chaos Bolt.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_pugna','- Decrepify turns the target ethereal, protecting them from Chaos Knight and his illusions attacks.
- Life Drain instantly destroys illusions. It can effectively negate Phantasm when upgraded with Aghanim Scepter icon.png Aghanim Scepter.
- Nether Blast quickly clears out illusions and can harass Chaos Knight in lane.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_sand_king','- Sand King massive area damage output, particularly Epicenter, allows him to quickly take down Chaos Knight and his illusions.
- He can apply Caustic Finale to illusions with attacks for extra damage.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_timbersaw','- Strong area damage, particularly from Whirling Death and Chakram, lets Timbersaw clear out illusions quickly.
- Whirling Death directly reduces Strength from both Chaos Knight and his Phantasm illusions, reducing both the damage output and raw health that the hero relies on.
- Reactive Armor quickly builds stacks against Phantasm illusions, making Timbersaw hard to kill.
- Timber Chain allows Timbersaw to escape while surrounded.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_tinker','- Tinker Aghanim Scepter upgraded Laser can blind him and all of his Phantasm illusions.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_sven','- Sven shorter cast time allows him to use Storm Hammer on Chaos Knight and his illusions before he can cast Chaos Bolt.
- Warcry armor bonus counteracts Reality Rift slow and reduces Chaos Knight damage output.
- Great Cleave and God Strength devastates Phantasm illusions.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_tidehunter','- Ravage damages and disables Chaos Knight army.
- Anchor Smash deals respectable area damage at early levels and lowers Chaos Knight damage.
- Kraken Shell dispels Chaos Bolt and Reality Rift while providing protection from damage.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_witch_doctor','- Maledict cannot be dispelled by Phantasm, and can be used to tag the real Chaos Knight hiding within his illusions.
- If Witch Doctor has an Aghanim Scepter icon.png Aghanim Scepter, Death Ward will bounce between Chaos Knight illusions, destroying them quickly.
- However, Death Ward is easily interrupted by Chaos Bolt until Witch Doctor buys Glimmer Cape icon.png Glimmer Cape or Black King Bar icon.png Black King Bar.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_omniknight','- Purification heals a teammate targeted by Reality Rift and heavily damages Chaos Knight and his illusions in the process.
- Guardian Angel blocks all of Chaos Knight damage for a duration.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_winter_wyvern','- Cold Embrace protects targets of Reality Rift.
- Winter Curse turns Phantasm illusions against Chaos Knight or a teammate.
- Splinter Blast deals decent area damage to Phantasm illusions.
- Arctic Burn percentage-based damage quickly wears down Chaos Knight health.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_chaos_knight','id_monkey_king','- Wukong Command and Boundless Strike combo can easily cause havoc on Chaos Knight by destroying all of his powerful illusions.
- Chaos Knight without Phantasm cannot deal with Jingu Mastery at all.','b','c');