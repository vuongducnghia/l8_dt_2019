select id_hero from HeroBaseInfo where name_dota_2 in ('Centaur Warrunner',
'Drow Ranger',
'Earthshaker',
'Enigma',
'Jakiro',
'Legion Commander',
'Leshrac',
'Monkey King',
'Naga Siren',
'Pudge',
'Pugna',
'Sniper',
'Spirit Breaker',
'Tinker',
'Underlord',
'Windranger')

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_centaur_warrunner','- Heroes with long cast animations can be continuously interrupted by Battery Assault','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_drow_ranger','- Getting trapped in Power Cogs usually means the end for Drow Ranger.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_earthshaker','- Battery Assault will continuously interrupt Earthshaker long cast animations.
- Power Cogs can also trap Earthshaker after he initiates.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_enigma','- Black Hole requires channelling, making it easy to be interrupted with Hookshot.
- Enigma lacks mobility without a Blink Dagger and also is squishy, making him fall easily when ganked by Clockwerk.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_jakiro','- Heroes with long cast animations can be continuously interrupted by Battery Assault','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_legion_commander','- Battery Assault mini-stuns can severely disrupt Legion Commander attack animation when she enters a Duel, potentially saving Clockwerk (or his allies) and possibly even giving him an edge in securing a win himself.
- Power Cogs pushes Legion Commander away from Clockwerk allies when she enters a Duel.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_leshrac','- Heroes with long cast animations can be continuously interrupted by Battery Assault','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_naga_siren','- Heroes with long cast animations can be continuously interrupted by Battery Assault','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_pudge','- Meat Hooking a Clockwerk can backfire for Pudge, as Battery Assault and Blade Mail will hurt Pudge more than he hurts Clockwerk.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_pugna','- Pugna squishiness and lack of mobility make him easy prey for a Hookshot initiation.
- Clockwerk high health, low mana costs, and lack of reliance on Blink Dagger make Nether Ward ineffective.
- Battery Assault shuts down Aghanim Scepter upgraded Life Drain, as the repeated ministuns prevent Pugna from channeling for an extended length of time.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_tinker','- Rocket Flare can find Tinker if he blinks into the treeline, and Hookshot will prevent him from teleporting out.
- Battery Assault makes Tinker unable to Rearm by stunning him repeatedly.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_sniper','- Clockwerk is also the least optimal target for Sniper to attack in a teamfight, although more experienced players will rarely make this mistake.
- With no mobility or disable skills, getting trapped in Power Cogs is a death sentence for Sniper.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_spirit_breaker','- Clockwerk can use Power Cogs to repel Spirit Breaker when he charges in with Charge of Darkness.
- Spirit Breaker slow 0.6 second attack animation attacks do little damage against Clockwerk. It can also constantly be interrupted by Battery Assault.
- Nether Strike 1.2 second charge time gives Clockwerk plenty of time to activate Blade Mail and for Battery Assault to cancel it.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_windrunner','- Power Cogs prevent Windranger from fleeing with Windrun.
- Battery Assault repeatedly interrupts Focus Fire, disrupting Windranger damage output.','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_underlord','- Heroes with long cast animations can be continuously interrupted by Battery Assault','b','c');
insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_monkey_king','- Power Cogs pushes Monkey King out of Wukong Command, ending his ultimate.
- Rocket Flare allows Clockwerk to scout Monkey King in the trees, and chase him with Hookshot.','b','c');