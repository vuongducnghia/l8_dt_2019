select id_hero from HeroBaseInfo where name_dota_2 in ('Anti Mage',
'Chen',
'Clinkz',
'Dazzle',
'Disruptor',
'Ember Spirit',
'Huskar',
'Juggernaut',
'Lifestealer',
'Morphling',
'Omniknight',
'Phantom Lancer',
'Phoenix',
'Queen of Pain',
'Terrorblade',
'Ursa',
'Visage',
'Vengeful Spirit',
'Ancient Apparition',
'Skywrath Mage')

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_ancient_apparition','- Cold Feet will freeze Clockwerk in place if he stays inside his Power Cogs. His immobility while in his cogs also makes him a very easy target to Ice Vortex and Ice Blast.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_antimage','- Anti-Mage can easily Blink out of Power Cogs, escaping any ganking or initiation attempts.
- Mana Break depletes Clockwerk much-needed mana.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_chen','- Due to Clockwerk slow damage output, Chen can often save a trapped enemy with Hand of God.
- Penitence damage amplification may allow Chen and his allies to kill Clockwerk quickly after his initiation.
- Chen creeps can be used to block Hookshot initiation from all sides, as well as distribute Battery Assault damage.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_clinkz','- Searing Arrows makes Clinkz one of the hardest heroes for Clockwerk to lane against.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_dazzle','- Shadow Wave will hit Clockwerk every time when he fighting with an enemy inside Power Cogs.
- Due to Clockwerk slow damage output, Dazzle will have more than enough time to Shallow Grave any enemy Clockwerk initiates on.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_disruptor','- Glimpse moves Clockwerk away from allies struck by Hookshot, or under attack by Battery Assault.
- Static Storm can turn Power Cogs into Clockwerk own grave, but Blade Mail icon.png Blade Mail will also return the damage to Disruptor.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_ember_spirit','- Ember Spirit Fire Remnant is an easy way to escape from Clockwerk Power Cogs.
- Flame Guard blocks most of the magical damage Clockwerk does.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_huskar','- Clockwerk cannot go head-to-head with Huskar in Power Cogs, as Berserker Blood and Life Break will destroy him faster than the other way around.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_juggernaut','- Juggernaut is invulnerable while using Omnislash, meaning that neither Blade Mail icon.png Blade Mail or Power Cogs will save Clockwerk, this is especially common during the early laning phase.
- Blade Fury will also ignore any damage dealt by Battery Assault, as well as dealing heavy damage. As long as Juggernaut has mana to cast it, any engagement with Power Cogs will hurt Clockwerk a lot more than Juggernaut.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_life_stealer','- With Feast and Rage, Lifestealer is nearly impossible for Clockwerk to bring down, and is the last hero Clockwerk wants to be trapped with in Power Cogs.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_morphling','- Waveform allows Morphling to easily escape Power Cogs.
- Clockwerk lack of burst damage makes it nearly impossible for him to kill a Morphling using Attribute Shift (Strength Gain).','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_phantom_lancer','- Battery Assault damage is dispersed against Phantom Lancer illusions.
- Phantom Lancer can blink out of Power Cogs with Doppelganger.
- When chasing, a Phantom Lancer can close in with Phantom Rush and burn down Clockwerk mana with Diffusal Blade, rendering him helpless before he has a chance to erect a defensive Power Cogs.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_phoenix','- Clockwerk being confined in his own Power Cogs means Clockwerk is unable to quickly escape from Phoenix Sun Ray and Supernova.
- Sun Ray will also heal the enemy Clockwerk is fighting, making it unlikely for Clockwerk to take an enemy down with him.
- For the same reasons, Phoenix also makes a powerful ally for Clockwerk when the situation is reversed.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_queenofpain','- Blink allows Quen of Pain to easily escape ganking or initiating attempts with Hookshot and Power Cogs.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_skywrath_mage','- Skywrath Mage Mystic Flare can deal huge damage to Clockwerk inside his Power Cogs.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_terrorblade','- Conjure Image and Reflection illusions may give Clockwerk a hard time to find an angle for Hookshot.
- A trapped Terrorblade can turn the tables with Sunder, leaving Clockwerk nowhere to run.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_ursa','- Ursa high physical burst damage makes him a very bad target for Power Cogs.
- Even with Blade Mail, Ursa Enrage will negate most of the reflected damage.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_vengeful_spirit','- Vengeful Spirit can Nether Swap enemies out of Power Cogs.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_visage','- Visage Familiar can spot Clockwerk, allowing Visage allies to dodge or block Hookshot attempts.
- Familiars will not be damaged by Blade Mail or Clockwerk abilities, dealing dangerous amounts of damage to him.','b','c');
insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_clockwerk','id_omniknight','- Purification does great damage to Clockwerk when fighting an enemy inside Power Cogs.
- For the same reasons, Omniknight also makes a powerful ally for Clockwerk when the situation is reversed.','b','c');

