SELECT id_item FROM ItemBaseInfo WHERE name in ('Eul Scepter of Divinity'
'Force Staff',
'Medallion of Courage',)

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_clockwerk','id_medallion_of_courage','- Medallion of Courage will allow enemies to quickly kill Clockwerk, making his initiation short-lived.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_clockwerk','id_force_staff','- Force Staff can push trapped enemies out of Power Cogs, and Hurricane Pike can also do this while pushing Clockwerk on the other side of the cogs.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_clockwerk','id_eul_is_scepter_of_divinity','- Eul Scepter of Divinity will keep enemies safe from Clockwerk until Power Cogs go down.','b','c');