SELECT id_hero FROM HeroBaseInfo WHERE name_dota_2 in ('Ancient Apparition','Axe')

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_dragon_knight','id_dragon_knight','a','b','c');


Ancient Apparition
Bristleback
Lich
Necrophos
Ursa
Outworld Devourer
Razor
Timbersaw
Viper

    In the early game, a Dragon Knight without Black King Bar icon.png Black King Bar is basically defenseless against Viper.

Others

    In general, heroes that can slow down Dragon Knight's movement speed will do well against him. This includes Venomancer minimap icon.png Venomancer, Drow Ranger minimap icon.png Drow Ranger and others in addition to the heroes mentioned above.
    Slardar minimap icon.png Slardar can chase Dragon Knight down with Guardian Sprint and stun him with both Slithereen Crush and his passive Bash of the Deep. He can also use Corrosive Haze to whittle Dragon Knight's armor.
    Shadow Fiend minimap icon.png Shadow Fiend's Presence of the Dark Lord reduces Dragon Knight's armor considerably, and his strong base attacks are augmented by his Necromastery.
    Huskar minimap icon.png Huskar can burn down Dragon Knight's health quickly during the early to mid game, but is not as effective during the late game. However, Life Break remains a constant threat due to the slow and percentage-based damage.
    Abilities that deal pure damage: Impetus, Doom, Rupture.
    Elder Titan minimap icon.png Elder Titan's Natural Order completely nullifies Dragon Knight's high armor.

//----------

Ancient Apparition

    Cold Feet and Ice Vortex both hinder Dragon Knight's movement greatly during teamfights.
    Ice Blast ignores Dragon Knight's armor and kills him prematurely.

Bristleback icon.png
Bristleback

    Viscous Nasal Goo slows down Dragon Knight and lowers armor, locking him down and making him more vulnerable.

Lich icon.png
Lich

    Lich's constant slows with Frost Blast and Chain Frost will slow Dragon Knight to a crawl, especially in a crowded teamfight.

Necrophos icon.png
Necrophos

    Heartstopper Aura and Reaper's Scythe go right through Dragon Knight's armor.

Ursa icon.png
Ursa

    Fury Swipes can tear Dragon knight down as it stacks infinitely.

Outworld Devourer icon.png
Outworld Devourer

    Arcane Orb's constant pure damage output is devastating to Dragon Knight.
    Dragon Knight's relatively low mana pool leads to heavy damage from Sanity's Eclipse, which also bypasses his armor.

Razor icon.png
Razor

    Static Link saps Dragon Knight of his already low base damage, making him even more useless in a fight if he gets kited.
    Storm Surge makes Razor much faster than Dragon Knight, and hard to catch.

Timbersaw icon.png
Timbersaw

    Timbersaw's pure damage will kill Dragon Knight surprisingly fast, especially if he gets caught in a bad position while laning.

Viper icon.png
Viper

    In the early game, a Dragon Knight without Black King Bar icon.png Black King Bar is basically defenseless against Viper.

Others

    In general, heroes that can slow down Dragon Knight's movement speed will do well against him. This includes Venomancer minimap icon.png Venomancer, Drow Ranger minimap icon.png Drow Ranger and others in addition to the heroes mentioned above.
    Slardar minimap icon.png Slardar can chase Dragon Knight down with Guardian Sprint and stun him with both Slithereen Crush and his passive Bash of the Deep. He can also use Corrosive Haze to whittle Dragon Knight's armor.
    Shadow Fiend minimap icon.png Shadow Fiend's Presence of the Dark Lord reduces Dragon Knight's armor considerably, and his strong base attacks are augmented by his Necromastery.
    Huskar minimap icon.png Huskar can burn down Dragon Knight's health quickly during the early to mid game, but is not as effective during the late game. However, Life Break remains a constant threat due to the slow and percentage-based damage.
    Abilities that deal pure damage: Impetus, Doom, Rupture.
    Elder Titan minimap icon.png Elder Titan's Natural Order completely nullifies Dragon Knight's high armor.