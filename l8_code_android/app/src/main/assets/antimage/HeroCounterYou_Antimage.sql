INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_abaddon','1. Mana Break drains Abaddon mana, preventing him from using Aphotic Shield to zone out enemies in lane.
2. Counterspell decreases the amount of damage received from Aphotic Shield and Mist Coil.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_ancient_apparition','Anti-Mage easily Blinks out of Cold Feet freeze radius.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_bristleback','1. Mana Break quickly eats away at Bristleback small mana pool, preventing him from spamming Viscous Nasal Goo and Quill Sprays.
2. Bristleback has no innate mobility. Bristleback is easily kited by Blink, ignoring the protection from Bristleback.
3. Heroes who build mana boosting items but have low mana regeneration are especially vulnerable against Mana Break and Mana Void','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_clinkz','Heroes who build mana boosting items but have low mana regeneration are especially vulnerable against Mana Break and Mana Void','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_clockwerk','Anti-Mage can Blink into or out of Power Cogs, negating Clockwerk initiation and bypassing his defenses.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_elder_titan','Echo Stomp and Earth Splitter long cast times give Anti-Mage plenty of time to Blink away.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_nature_prophet','Anti-Mage high mobility from Blink and tendency to build Battle Fury makes him strong against split-pushing heroes','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_leshrac','Anti-Mage can withstand magical burst damage much better than other carries, making him effective against nukers','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_lich','Anti-Mage can withstand magical burst damage much better than other carries, making him effective against nukers','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_magnus','Heroes who build mana boosting items but have low mana regeneration are especially vulnerable against Mana Break and Mana Void','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_medusa','1. Anti-Mage easily destroys Mana Shield with Mana Break, especially once he has Manta Style.
2. Anti-Mage effectively avoids Stone Gaze with Blink, and can reinitiate after its duration is over.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_necrophos','Anti-Mage can withstand magical burst damage much better than other carries, making him effective against nukers','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_nevermore','Heroes who build mana boosting items but have low mana regeneration are especially vulnerable against Mana Break and Mana Void','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_nyx_assassin','Heroes who build mana boosting items but have low mana regeneration are especially vulnerable against Mana Break and Mana Void','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_phoenix','1. Anti-Mage dispels Phoenix slows with Manta Style.
2. Anti-Mage can Blink directly next to Supernova, giving him more time to destroy the egg.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_pugna','1. Pugna only deals magical damage, which Anti-Mage is exceptionally resistant due to Counterspell.
2. Anti-Mage can dispel Decrepify with Manta Style.
3. Pugna has a very large mana pool. This allows Anti-Mage to constantly deal bonus damage to Pugna with Mana Break and devastate Pugna entire team with Mana Void.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_rubick','1. Mana Break quickly eats away at Rubick low mana pool, preventing Spell Steal abilities from triggering.
2. Rubick complete lack of mobility makes him very easy for Anti-Mage to chase down with Blink.
3. Anti-Mage dispels Fade Bolt damage reduction debuff with Manta Style or severly reduced by Counterspell.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_timbersaw','Heroes who build mana boosting items but have low mana regeneration are especially vulnerable against Mana Break and Mana Void','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_tinker','Anti-Mage high mobility from Blink and tendency to build Battle Fury makes him strong against split-pushing heroes','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_slark','Heroes who build mana boosting items but have low mana regeneration are especially vulnerable against Mana Break and Mana Void','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_spectre','1. Spectre cannot chase down Anti-Mage with Spectral Dagger because of Blink, though it still grants her team vision over him.
2. Counterspell reduces much of the damage from Dispersion and Spectre Radiance (Active).
3. Anti-Mage usually builds a Manta Style, preventing Spectre from dealing any Desolate damage to him once he summons his illusions.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_storm_spirit','1. Because of the high mana cost of Ball Lightning, Storm Spirit is often vulnerable to Mana Void.
2. Mana Break diminishes Storm Spirit mobility with Ball Lightning, and Anti-Mage can catch him with Blink over short distances.
3. Almost all of Storm Spirit damage is magical, which is severely reduced by Counterspell.
4. Storm Sprit has a very large mana pool which means Anti-Mage can do large amounts of damage to him with Mana Break and Mana Void.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_tidehunter','1. Both Gush and Ravage do little damage against Counterspell.
2. Blink helps Anti-Mage escape through Anchor Smash.
3. Tidehunter small mana pool can be eated by Mana Break, preventing to spam his abilities and ignores the protection from Kraken Shell.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_undying','Tombstone zombies rarely inhibit Anti-Mage, as he can either Blink away or onto the Tombstone and destroy it.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_venomancer','1. Venomancer damage-over-time abilities do little damage against Counterspell.
2. Blink helps Anti-Mage get away from Venomancer slows.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_wraith_king','1. Mana Break quickly eats away at Wraith King small mana pool, preventing Reincarnation from triggering.
2. Wraith King has no innate mobility. Even with Blink Dagger, Wraith King is easily kited by Blink.
3. Anti-Mage can farm much more quickly than Wraith King, allowing Anti-Mage to overpower Wraith King once a significant farm advantage is established.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_zeus','1. Zeus almost exclusively relies on magical damage, which is heavily reduced by Counterspell.
2. Zeus complete lack of mobility makes him very easy for Anti-Mage to chase down with Blink.
3. Zeus likes to stand in the back during team fights, but Anti-Mage can easily Blink to close the distance.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_ogre_magi','1. Ogre Magi only deals magical damage, which Anti-Mage is exceptionally resistant due to Counterspell.
2. Anti-Mage dispels Ignite with Manta Style.
3. Ogre Magi has a very large mana pool which means Anti-Mage can do large amounts of damage to him with Mana Break and Mana Void.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_oracle','Anti-Mage can withstand magical burst damage much better than other carries, making him effective against nukers','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_arc_warden','1. Anti-Mage Blinks directly into Magnetic Field, bypassing its protection.
2. Anti-Mage high mobility allows him to deal with Tempest Double while it is split-pushing.
3. Anti-Mage Battle Fury cleans up Arc Warden Necronomicon summons, and Counterspell protects him from Necronomicon Warrior Last Will.
4. Anti-Mage high mobility from Blink and tendency to build Battle Fury makes him strong against split-pushing heroes','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_','a','b','c');