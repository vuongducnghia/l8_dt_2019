INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_arcane_boots','Arcane Boots keeps the team mana high, reducing the damage from Mana Void.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_orchid_malevolence','Orchid Malevolence prevents Anti-Mage from Blinking away.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_scythe_of_vyse','Scythe of Vyse prevents Anti-Mage from Blinking away.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_abyssal_blade','Abyssal Blade prevents Anti-Mage from Blinking away.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_black_king_bar','Black King Bar protects against Mana Break and Mana Void.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_linken_is_sphere','Linken Sphere blocks Mana Void, and can be used on vulnerable teammates.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_silver_edge','Silver Edge breaks Counterspell spell block, preventing Anti-Mage from blocking spells passively.','b','c');
