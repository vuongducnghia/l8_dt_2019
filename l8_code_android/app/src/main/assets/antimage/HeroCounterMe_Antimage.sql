INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_axe','1. Berserker Call prevents Anti-Mage from escaping with Blink, and works well with Blade Mail against Anti-Mage relatively low health.
2. Counter Helix deals pure damage, ignoring the protection from Counterspell.
3. Counter Helix will trigger even if Axe depleted of mana, which will make Anti-Mage take more damage than he dealing.
4. Axe low mana pool makes him a bad target for using Mana Void.
5. Axe high base health regeneration allows him to dominate the lane against Anti-Mage, especially with a support.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_bane','1. Fiend Grip very long disable prevents Anti-Mage from Blinking away or using Manta Style. It also pierces spell immunity.
2. Nightmare sets Bane team up for chain disables on Anti-Mage.
3. Enfeeble lowers Anti-Mage status resistance and magic resistance, making him more vulnerable to disables and magic damage that normally would not affect him much.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_bloodseeker','1. Rupture deals damage through Blink, dissuading Anti-Mage from using it. This allows Bloodseeker to get early kills on Anti-Mage, preventing him from snowballing.
2. However, Anti-Mage is one of the few heroes who can avoid the damage from Rupture if he does it correctly.
3. Blood Rite silences Anti-Mage during teamfights, preventing him from Blinking away.
4. Bloodseeker abilities also deal pure damage, which bypasses Counterspell.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_disruptor','1. Glimpse brings Anti-Mage back after he Blinks away.
2. Static Storm stops Anti-Mage from Blinking.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_doom_bringer','1. Doom shuts down Anti-Mage escape capabilities with Blink. The ability also deals pure damage, which ignores the protection from Counterspell.
2. If Doom has Aghanim Scepter, Infernal Blade can hit Anti-Mage loses his Mana Break and Counterspell, weakening both his offense and defense.
3. Doom low mana pool causes him to take little damage from Mana Void.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_grimstroke','1. Phantom Embrace stops Anti-Mage from Blinking away.
2. Soulbind stops Blink and lets allies cast abilities on the tethered target to avoid Counterspell. It cannot be reflected by Counterspell.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_legion_commander','1. Duel renders Anti-Mage escape capabilities with Blink and Counterspell useless.
2. A good initiation from Legion Commander and her team is enough to effortlessly kill Anti-Mage.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_meepo','1. Multiple Earthbinds allows Meepo to lock down Anti-Mage almost indefinitely.
2. Meepo clones can only use their mana for Poof, keeping their mana pool nearly full and thus making them a bad target for a Mana Void.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_night_stalker','1. Crippling Fear makes Night Stalker one of the best early game gankers to shut down Anti-Mage.
2. Night Stalker low mana pool causes him to take little damage from Mana Void.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_outworld_devourer','1. Arcane Orb pure damage goes through Counterspell, and deals splash damage to illusions spawned from Manta Style.
2. Sanity Eclipse hits Anti-Mage while he is disabled with Astral Imprisonment, and forces him to take heavy damage due to his low intelligence.
3. Equilibrium keeps Outworld Devourer at high mana, thus greatly reducing the damage from Mana Void.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_phantom_assassin','1. Phantom Assassin has an abysmal mana pool and intelligence gain, causing Mana Break and Mana Void to deal minimal damage.
2. Blur reduces Anti-Mage damage against Phantom Assassin, and forces him to buy a Monkey King Bar.
3. Coup de Grace physical damage burst can kill Anti-Mage before he has a chance to Blink.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_shadow_demon','1. Slithereen Crush and Bash of the Deep prevent Anti-Mage from using Blink, the latter even if Anti-Mage activates Black King Bar.
2. Corrosive Haze allows allies to take down Anti-Mage before he has a chance to Blink, and maintains vision over him even if he does Blink away.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_slardar','1. Pounce prevents Anti-Mage from using Blink, and lets him disengage from Anti-Mage if needed.
2. Essence Shift allows Slark to permanently steal agility from Anti-Mage.
3. Shadow Dance stops Anti-Mage from being able to fight back in a slippery situation.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_slark','1. Disruption sets up Anti-Mage for a chain disable. The illusions will deal extra damage due to Mana Break.
2. Soul Catcher causes Anti-Mage to take a large chunk of damage, and can hit him when he is inside from Disruption.
3. Shadow Poison hits Anti-Mage even after he Blinked away.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_templar_assassin','1. Refraction protects against Anti-Mage low burst damage, and allows her to win most fights against Anti-Mage during the early game.
2. Meld burst damage and armor reduction is often enough to finish off Anti-Mage before he Blinks away.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_troll_warlord','1. Troll Warlord passive root, high attack speed, and blind from Whirling Axes makes him an exceedingly difficult opponent for Anti-Mage to take down in a one-on-one fight.
2. Troll Warlord low mana causes him to take little damage from Mana Void.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_antimage','id_viper','1. Viper skill set makes him an effective early game ganker against Anti-Mage before he is sufficiently farmed.
2. Viper Corrosive Skin and low mana pool causes him to take little damage from Mana Void.
3. Nethertoxin disables Mana Break and Counterspell, weakening both his offense and defense.','b','c');
