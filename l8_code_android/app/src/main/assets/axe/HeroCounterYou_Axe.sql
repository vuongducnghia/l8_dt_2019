select id_hero from HeroBaseInfo where name_dota_2 in ('Anti Mage',
'Broodmother',
'Chaos Knight',
'Dazzle',
'Huskar',
'Morphling',
'Nature Prophet',
'Phantom Assassin',
'Phantom Lancer',
'Slark',
'Terrorblade',
'Troll Warlord',
'Weaver')


insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_antimage','1. Berserker Call is one of the most reliable ways to lock down Anti-Mage, preventing him from Blinking away.
2. Anti-Mage high attack speed, along with illusions from Manta Style, triggers Counter Helix continuously.
3. Axe low mana pool and high health pool makes Mana Void deal very little damage to Axe.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_broodmother','1. Berserker Call can catch Broodmother in her web, as well as groups of spiderlings. This is particularly effective when Broodmother attempts to push towers with her spiderlings.
2. Counter Helix destroys spiderlings very quickly, and earns Axe a good deal of gold.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_chaos_knight','Berserker Call and Counter Helix makes short work of Chaos Knight Phantasms.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_dazzle','Culling Blade ignores the protection from Shallow Grave, and will kill any target whose health is low enough. This makes Axe one of the hardest counters to Dazzle, and he neutralizes one of Dazzle most important abilities.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_nature_prophet','1. Berserker Call and Counter Helix will still hit targets inside of a defensive Sprout.
2. Counter Helix clears treants in no time.
3. Nature Prophet low damage is unlikely to hurt Axe until the very late game.
4. Nature Prophet low mobility makes him very easy for Axe to chase down or initiate on, especially when affected by Battle Hunger.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_huskar','1. Huskar is often played at low health to maximize the bonuses from Berserker Blood, and also because his abilities cost health to use. This makes him especially vulnerable to Culling Blade.
2. Berserker Blood gives Huskar no protection against the pure damage of Culling Blade. Its attack speed bonus also means that Counter Helix will proc more often.
3. Axe tankiness and base health regeneration makes it hard for Huskar to take him down with Burning Spears.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_morphling','1. Jumping on Axe from Waveform he will be an easy target for a counter-initiation with Berserker Call.
2. Morphling is often played at low health to maximize the bonuses from Attribute Shift (Agility Gain), and also because his abilities cost health to use. This makes him especially vulnerable to Culling Blade.
2.2     However, Morphling is common Linken Sphere carrier to block Culling Blade forces Axe to use Battle Hunger to pop the Spell Block first.
3. Attribute Shift (Agility Gain) gives Morphlig no protection against the pure damage of Culling Blade. Its attack speed bonus also means that Counter Helix will proc more often.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_phantom_assassin','1. Phantom Strike extra attack speed causes Phantom Assassin to proc Counter Helix more often. Using it on Axe also makes Phantom Assassin an easy target for a counter-initiation with Berserker Call and Blade Mail icon.png Blade Mail.
2. Most of Axe abilities pierce Black King Bar, allowing him to lock down Phantom Assassin at any time.
3. Berserker Call greatly reduces Phantom Assassin ability to crit hard against Axe, and may cause her to kill herself when used with Blade Mail.
4. Heroes with gap closing abilities will be easy targets for Berserker Call','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_phantom_lancer','1. Berserker Call catches Phantom Lancer when he is hidden among his illusions.
2. Counter Helix quickly destroys Phantom Lancer illusion army. The more illusions there are, the more often Counter Helix is triggered.
3. Phantom Lancer low early game health makes him an easy target for Culling Blade.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_slark','1. Berseker Call catches Slark in Shadow Dance, wasting its duration and preventing him from escaping to regenerate.
2. Unlike most disables, Dark Pact cannot dispel Berserker Call, and the output damage would be returned by Blade Mail.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_terrorblade','1. Terrorblade low base health and lack of mobility make him an easy target for Axe to initiate on, and kill.
2. Counter Helix will trigger from, and destroy many of Terrorblade illusions.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_troll_warlord','Troll Warlord high attack speed means Counter Helix will proc extremely frequently even without creeps near him. This, combined with a Blade Mail means Axe can easily solo kill Troll Warlord even in later stages of the game.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_weaver','1. Berserker Call is one of the best ways to lock down Weaver before he has a chance to use Time Lapse. It can also catch Weaver in Shukuchi.
2. Weaver high attack speed, along with high attack damage from Geminate Attack, triggers Counter Helix continuously and the output damage would be returned by Blade Mail.','b','c');
