SELECT id_item FROM ItemBaseInfo WHERE name in ('Eul Scepter of Divinity',
'Force Staff',
'Silver Edge',
'Mekansm',
'Ethereal Blade')

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_axe','id_mekansm','Mekansm keeps allies health above Culling Blade threshold, and can be used at the last moment to bait Axe into an overly long initiation.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_axe','id_force_staff','Force Staff can help dodge a Berserker Call, or push an ally out of its radius.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_axe','id_eul_is_scepter_of_divinity','Eul Scepter of Divinity wastes the duration of Berserker Call when used on Axe.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_axe','id_ethereal_blade','Ethereal Blade can be used on an Axe to prevent any allied unit from attacking him.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_axe','id_silver_edge','Silver Edge breaks Counter Helix.','b','c');

 





