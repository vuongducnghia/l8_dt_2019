select id_hero from HeroBaseInfo where name_dota_2 in ('Bristleback',
'Crystal Maiden',
'Batrider',
'Arc Warden',
'Winter Wyvern',
'Clockwerk')


insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_doom_bringer','1. Infernal Blade damage is calculated by percentage of maximum health, meaning that it will hurt Axe more than most normal attacks, negating some of his tankiness. With Aghanim Scepter icon.png Aghanim Scepter, Counter Helix is also disabled, making him effectively useless for a lengthy duration.
2. Doom can pick up several different abilities from neutral creeps using Devour, some of which can be annoying to Axe.
2.2 Mud Golem, Dark Troll Summoner, Harpy Stormcrafter, Satyr Mindstealer or Satyr Tormenter are good examples.
3. Doom makes Axe less disruptive, preventing him from using his skills or items.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_jakiro','1. Ice Path neutralizes initiation attempts with Berserker Call, and its long area of effect discourages follow-up from Axe teammates.
2. Ice Path serves as a large obstacle to stop Axe from chasing down fleeing stragglers to kill with Culling Blade.
3. Jakiro many damage-per-second skills prevent Axe from initiating with his Blink Dagger.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_life_stealer','1. Feast lifesteal makes it nearly impossible for Axe to initiate and bring Lifestealer health low enough for Culling Blade, even with many procs of Counter Helix.
2. Feast damage is calculated as a percentage of health, turning Axe high health against him. This allow Lifestealer to hit Axe much harder than most other heroes.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_necrophos','1. Frequent healing from Death Pulse and Mekansm icon.png Mekansm makes it difficult for Axe to finish off targets with Culling Blade.
2. Ghost Shroud instant cast time allows Necrophos to turn ethereal before being initiated on by Berserker Call. This prevents him from attacking Axe while taunted, thus lowering the chance to proc Counter Helix.
3. Axe high health is relatively ineffective against Heartstopper Aura percentage-based health degeneration.
4. Reaper Scythe negates Axe tankiness, and can kill him quickly after initiation.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_outworld_devourer','1. Outworld Devourer pure and magical damage is particularly effective against Axe.
2. Astral Imprisonment is an excellent way to defend against an initiating Axe.
3. Sanity Eclipse deals high damage to Axe due to his low mana pool.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_phoenix','1. Sun Ray allows Phoenix to heal any allies caught in Berserker Call, while burning away Axe high HP at the same time.
2. Axe slow attack speed makes it difficult for him to destroy Supernova on his own.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_pugna','1. Decrepify can be used on allies to prevent them from attacking Axe and triggering Counter Helix during Berserker Call. He can also cast it on Axe to prevent anyone from attacking him.
2. Life Drain can be used to heal himself or allies caught in Berserker Call, preventing them from dropping low enough for Axe to kill them with Culling Blade.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_shadow_demon','1. Disruption will save Shadow Demon teammates from being killed by Counter Helix or Culling Blade.
2. Demonic Purge slows Axe to a crawl, making him nearly useless after initiating. If Shadow Demon has Aghanim Scepter, it will also break Counter Helix.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_timbersaw','1. Whirling Death reduces Axe strength by 15%, making him much less tanky, especially in the early game. It is also stackable.
1.1 Axe Blade Mail icon.png Blade Mail is ineffective against attribute changes, and Timbersaw mobility with Timber Chain means he can wait out Blade Mail duration.
2. Reactive Armor armor and regeneration makes it much harder for Axe to finish Timbersaw with Culling Blade.
3. Chakram disarms Timbersaw, making Counter Helix less effective.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_spectre','Dispersion makes Spectre very tanky, making it much less likely for Axe to bring her low enough for Culling Blade.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_undying','1. Decay stacking strength reduction makes Axe much less dominant in lane.
2. Tombstone zombies take no damage from Counter Helix, and will continuously put Axe Blink Dagger icon.png Blink Dagger on cooldown until dealt with.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_ursa','1. Enrage reduces the damage from Blade Mail icon.png Blade Mail and Counter Helix to a minimum, forcing Axe to play around Ursa ultimate ability.
2. Enrage and Fury Swipes tear through Axe low armor, even if he has relatively high health.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_venomancer','1. Venomancer slows and magical damage-over-time work well against Axe complete lack of mobility.
2. Plague Ward is especially useful for slowing down Axe, and revealing him before he initiates, as well as putting his Blink Dagger icon.png Blink Dagger on cooldown.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_viper','1. Poison Attack and Viper Strike slows Axe significantly, and puts his Blink Dagger icon.png Blink Dagger on cooldown, preventing him from initiating.
2. Nethertoxin can break Counter Helix.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_batrider','Heroes who can slow and kite Axe will severely inhibit his presence in fights','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_bristleback','Heroes who can slow and kite Axe will severely inhibit his presence in fights','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_crystal_maiden','Heroes who can slow and kite Axe will severely inhibit his presence in fights','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_clockwerk','Heroes who can slow and kite Axe will severely inhibit his presence in fights','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_winter_wyvern','Heroes who can slow and kite Axe will severely inhibit his presence in fights','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_arc_warden','Heroes who can slow and kite Axe will severely inhibit his presence in fights','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_drow_ranger','Ranged heroes that can easily harass Axe.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_lion','Ranged heroes that can easily harass Axe.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_axe','id_sniper','Ranged heroes that can easily harass Axe.','b','c');



