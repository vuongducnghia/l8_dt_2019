SELECT id_hero FROM HeroBaseInfo WHERE name_dota_2 in ('Ancient Apparition','Axe')

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_ancient_apparition','1. Chilling Touch damage component can easily overwhelm Aphotic Shield damage block.
2. Ice Blast nullifies Abaddon healing from Borrowed Time and Mist Coil and it is not dispelled by Aphotic Shield.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_axe','1. Culling Blade instant kill is not prevented by Borrowed Time should Abaddon health falls below the threshold.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_brewmaster','1. Brewmaster Storm icon.png Storm can use Cyclone on Abaddon when he activates Borrowed Time, wasting its duration.
2. Drunken Brawler and Thunder Clap hinder Abaddon from applying Curse of Avernus silence on him.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_doom_bringer','1. Doom will make Abaddon unable to use Mist Coil, Aphotic Shield, and the active component of Borrowed Time, which means his allies could be easily disabled or killed.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_axe','1. Lina is a common Eul Scepter of Divinity icon.png Eul Scepter of Divinity carrier which can waste some duration of Borrowed Time and following up with Laguna Blade if timed correctly.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_night_stalker','1. Heroes that have long silences will prevent Abaddon from saving his allies effectively and disable the active component of Borrowed Time.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_nyx_assassin','Nyx Assassin might not kill Abaddon with his nuke, but a support Abaddon will struggle to protect an ally from him.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_outworld_devourer','1. Astral Imprisonment wastes Borrowed Time duration.
2. Outworld Devourer Sanity Eclipse can kill Abaddon before Borrowed Time triggers.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_shadow_demon','1. Shadow Demon with an Aghanim Scepter icon.png Aghanim Scepter will use Demonic Purge to disable Curse of Avernus and the passive component of Borrowed Time. If this is combined with a silence, Borrowed Time will not trigger at all.
2. Even without Aghanim Scepter icon.png Aghanim Scepter, he can still waste Borrowed Time with Disruption. Curse of Avernus fully works on illusions, turning the passive ability against him.','b','c');

INSERT INTO HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_silencer','1. Heroes that have long silences will prevent Abaddon from saving his allies effectively and disable the active component of Borrowed Time.','b','c');



