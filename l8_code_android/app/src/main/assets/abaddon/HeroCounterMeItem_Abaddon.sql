SELECT id_item FROM ItemBaseInfo WHERE name in ('Orchid Malevolence',
'Eul Scepter of Divinity',
'Silver Edge',
'Dagon',
'Spirit Vessel')

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_dagon','Dagon and Ethereal Blade can kill Abaddon before he uses Borrowed Time, forcing him to cast it manually.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_eul_is_scepter_of_divinity','Eul Scepter of Divinity wastes some duration of Borrowed Time.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_orchid_malevolence','Orchid Malevolence and Bloodthorn make a supporting Abaddon useless at crucial moments.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_silver_edge','Silver Edge disables Curse of Avernus and Borrowed Time passive component. When used in combination with Orchid Malevolence, Bloodthorn, or another silence, Borrowed Time will not trigger at all.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_spirit_vessel','Spirit Vessel decreases the amount of healing from Borrowed Time.','b','c');



