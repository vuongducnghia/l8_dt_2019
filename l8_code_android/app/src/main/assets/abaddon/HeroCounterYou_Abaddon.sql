SELECT id_hero FROM HeroBaseInfo WHERE name_dota_2 in ('Death Prophet',
'Mirana',
'Bane',
'Batrider',
'Beastmaster',
'Chen',
'Bloodseeker',
'Necrophos',
'Nyx Assassin',
'Skywrath Mage',
'Techies',
'Juggernaut',
'Shadow Shaman',
'Ogre Magi',
'Huskar',
'Venomancer',
'Phoenix',
'Lion',
'Lina',
'Legion Commander',
'Faceless Void',
'Pudge')

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_bane','Nightmare and Fiend Grip can be instantly dispelled by Aphotic Shield.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_batrider','1. Sticky Napalm, Flaming Lasso, and Flamebreak can all be instantly dispelled by Aphotic Shield.
2. Firefly will continuously heal Abaddon under Borrowed Time.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_beastmaster','1. Primal Roar can be instantly dispelled by Aphotic Shield.
2. The debuffs from Wild Axes and Poison from Call of the Wild: Boar can also be dispelled by Aphotic Shield.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide

,guide_sp,guide_vi)
VALUES ('id_abaddon','id_bloodseeker','If Rupture is used on Abaddon, Abaddon can use Borrowed Time and easily heal for the duration of Borrowed Time by simply moving.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_chen','Aphotic Shield can nullify Chen combo, while clearing Chen creeps simultaneously.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_death_prophet','1. Exorcism will continuously heal Abaddon under Borrowed Time, and cannot be turned off.
2. Spirit Siphon will continuously heal Abaddon under Borrowed Time, for a percentage of his maximum health.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_void','Abaddon constant heals and protection can save himself and his allies from burst-focused heroes','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_huskar','Heroes that deal damage over time will also fail to bring down Abaddon and his allies','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_juggernaut','In general, heroes that deal burst in multiple instances will trigger Borrowed Time and heal Abaddon for a large amount','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_legion_commander','Abaddon constant heals and protection can save himself and his allies from burst-focused heroes','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_lina','Lina Laguna Blade have a 0.25 second delay before the damage is dealt.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_lion','1. By simply using Aphotic Shield, Abaddon can easily save his allies from heroes that apply long disables.
2. Lion Finger of Death have a 0.25 second delay before the damage is dealt.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_mirana','Sacred Arrow can be instantly dispelled by Aphotic Shield.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_necrophos','If he is not silenced, Abaddon can use Borrowed Time to escape Reaper Scythe during the delay.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_nyx_assassin','1. Impale and Spiked Carapace stun can be instantly dispelled by Aphotic Shield.
2. If he is  not break by Vendetta, Abaddon can use Borrowed Time and easily heal for the duration of Borrowed Time by his burst damage.','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_phoenix','Heroes that deal damage over time will also fail to bring down Abaddon and his allies','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_pudge','Abaddon constant heals and protection can save himself and his allies from burst-focused heroes','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_shadow_shaman','1. In general, heroes that deal burst in multiple instances will trigger Borrowed Time and heal Abaddon for a large amount
2. By simply using Aphotic Shield, Abaddon can easily save his allies from heroes that apply long disables','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_skywrath_mage','In general, heroes that deal burst in multiple instances will trigger Borrowed Time and heal Abaddon for a large amount','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_techies','1. Aphotic Shield makes it less likely for allies to be killed by mines.
2. Because they explode in very short intervals, a stack of Remote Mines will always trigger Borrowed Time, making them impossible to kill Abaddon unless his resistances are lowered enough or Abaddon is affected by Break.
3. Abaddon can easily dispel the long-lasting root of Stasis Trap on himself and on an ally, while at the same also providing protection against the other mines damage.
4. In general, heroes that deal burst in multiple instances will trigger Borrowed Time and heal Abaddon for a large amount','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_venomancer','Heroes that deal damage over time will also fail to bring down Abaddon and his allies','b','c');

INSERT INTO HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
VALUES ('id_abaddon','id_ogre_magi','In general, heroes that deal burst in multiple instances will trigger Borrowed Time and heal Abaddon for a large amount','b','c');
