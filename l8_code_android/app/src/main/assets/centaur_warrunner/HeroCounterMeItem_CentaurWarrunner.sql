SELECT id_item FROM ItemBaseInfo WHERE name in ('Radiance',
'Black King Bar',
'Rod of Atos')

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_centaur_warrunner','id_rod_of_atos','Radiance prevents Centaur Warrunner from blinking, forcing him to use Stampede.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_centaur_warrunner','id_radiance','Black King Bar, if used well, will save a victim from Centaur Warrunner onslaught.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_centaur_warrunner','id_black_king_bar','Rod of Atos can root a hero during Stampede.','b','c');
