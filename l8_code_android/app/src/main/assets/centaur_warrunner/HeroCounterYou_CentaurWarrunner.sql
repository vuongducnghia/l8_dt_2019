select id_hero from HeroBaseInfo where name_dota_2 in ('Arc Warden',
'Doom',
'Nyx Assassin',
'Phantom Assassin',
'Phantom Lancer',
'Sniper',
'Windranger',
'Troll Warlord',
'Night Stalker',
'Anti Mage',
'Chaos Knight',
'Phantom Lancer',
'Naga Siren')

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_antimage','- Anti-Mage Mana Void won not do a lot of damage because Centaur has a low mana pool. Mana Break is also relatively ineffective against him.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_chaos_knight','- Heroes who are reliant on illusions','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_doom_bringer','- Whenever Doom casts his ultimate, Centaur Warrunner can cast Stampede and allow the Doomed hero to run away, wasting Doom ultimate.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_naga_siren','- Heroes who are reliant on illusions','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_night_stalker','- Night Stalker is all about right-clicks. His nuke, Void is also not that threatning once you have Hood of Defiance, your core item.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_nyx_assassin','- They are both nukers, but Centaur Warrunner has superior health and strength gain.
- Centaur low Intelligence makes Mana Burn do low damage.
- Bar some creative skill builds, Centaur Hoof Stomp stun duration should always last longer than Spiked Carapace stun, so escaping unscathed is hard for Nyx.
- With Blink Dagger and detection, Centaur can devastate Nyx in the open.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_phantom_assassin','- Retaliate deals damage to Phantom Assassin for every attack and Stifling Dagger thrown at Centaur.
- Blur cannot evade Double Edge.
- Heroes who are reliant on illusions','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_phantom_lancer','- His illusions will kill themselves in a few hits because of Retaliate. The nukes from Hoof Stomp and Double-Edge should be enough to clear them for a large portion of the game
- Centaur Warrunner small mana pool makes Diffusal Blade damage much lower once Centaur doesn not have any more mana left to burn. Keep in mind that Double Edge does not cost mana.
- Be wary when chasing down Phantom Lancer. Hoof Stomp terrible cast time makes it quite easy to dodge with Doppelganger.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_sniper','- Headshot and Shrapnel slow is ineffective against a team with haste (which Stampede gives).
- Commonly picked up mobility items and hasted movement during his Stampede (as well as his high health pool) make him extremely hard to kill from a distance, damaging Sniper with Retaliate and Blade Mail if he has one.
- A low health pool and no escape mechanism makes him an easy target for Centaur Warrunner burst damage and initiation (even making his team possible initiators with Stampede).','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_troll_warlord','- Troll Warlord high speed attack makes Retaliate deal a lot of damage to him.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_windrunner','- Focus Fire grants Windranger a massive attack speed bonus, while her attack damage is generally quite low. This, combined with Windranger low health pool and armor, makes Retaliate do absolutely massive damage over a short time, making Centaur a hero she can never expect to kill or harass.
- Windrun grants evasion, but Centaur Warrunner does not rely on successful attacks in order to do damage. Hoof Stomp, Double Edge, and Stampede all do large amounts of magical damage and can completely demolish Windranger in a moment.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_arc_warden','- He can not take you very easily, and his nukes are not strong enough to deal with Centaur Warrunner.
- Centaur Warrunner deals huge AoE damage and applies an AoE disable through Hoof Stomp, affecting the double as well the main Arc Warden.
- Magnetic Field is not very effective against melee heroes.
- This ability also increases his allies attack speed, meaning Retaliate will deal more damage.
- Stampede catches him easily due to the lack of escape mechanisms.','b','c');