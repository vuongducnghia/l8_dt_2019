select id_hero from HeroBaseInfo where name_dota_2 in
('Disruptor',
'Lifestealer',
'Necrophos',
'Nyx Assassin',
'Templar Assassi')

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_disruptor','Kinetic Field and Glimpse can completely negate Stampede.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_life_stealer','- Feast is an effective tool against Centaur Warrunner, who generally has high health and low armor, and compensates the reflected damage of Retaliate.
- If activated, Rage protects Lifestealer from Hoof Stomp, Double Edge, and Stampede.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_necrophos','- With high health and rarely buying Linken Sphere or Black King Bar, Centaur Warrunner is a good target for Reaper Scythe. However, Centaur Warrunner can resolve this problem by buying Pipe of Insight or Hood of Defiance to diminish its damage.
- Heartstopper Aura will do well against high health heroes such as Centaur Warrunner.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_nyx_assassin','Once you  have taken a point in Retaliate, Nyx can easily initiate on you: he can use Spiked Carapace and hit you with Vendetta. The damage from Retaliate will be reflected back to you, stunning you with no chance to react after he revealed himself, and should guarantee a successful Impale.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_centaur_warrunner','id_templar_assassin','- Centaur has nothing to deal with Refraction that will negate all of his burst damage.
- Psionic Traps provide good vision, making for him harder to initiate.','b','c');