UPDATE HeroBuildItems
SET item_starting = 'id_tango,id_healing_salve,id_quelling_blade,id_enchanted_mango',
    item_early = 'id_bottle,id_phase_boots,id_ring_of_basilius,id_veil_of_discord,id_orchid_malevolence,id_maelstrom,id_kaya',
	item_code = 'id_abyssal_blade,id_black_king_bar,id_shiva_is_guard,id_bloodstone,id_linken_is_sphere',
    item_situational = 'id_eul_is_scepter_of_divinity,id_scythe_of_vyse,id_radiance,id_desolator,id_octarine_core'
WHERE
    id_hero like 'id_void_spirit';

UPDATE HeroBuildItems
SET item_starting = 'id_clarity,id_iron_branch,id_tango,id_healing_salve,id_observer_ward,id_enchanted_mango',
    item_early = 'id_blink_dagger,id_magic_wand,id_magic_stick,id_boots_of_speed,id_arcane_boots,id_eul_is_scepter_of_divinity,id_wind_lace,id_glimmer_cape',
	item_code = 'id_scythe_of_vyse,id_guardian_greaves',
    item_situational = 'id_bottle,id_phase_boots,id_boots_of_travel,id_force_staff,id_veil_of_discord,id_rod_of_atos,id_aghanim_is_scepter,id_linken_is_sphere,id_kaya,id_aeon_disk,id_spirit_vessel'
WHERE
    id_hero like 'id_dark_willow';