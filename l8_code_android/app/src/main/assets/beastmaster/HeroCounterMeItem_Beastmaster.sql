SELECT id_item FROM ItemBaseInfo WHERE name in ('Linken Sphere',
'Aeon Disk',
'Battle Fury')



INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_beastmaster','id_battle_fury','Battle Fury cleave clears out Beastmaster summons in no time.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_beastmaster','id_linken_is_sphere','Linken Sphere blocks Primal Roar, making Beastmaster much less useful.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_beastmaster','id_aeon_disk','Aeon Disk passive automatically dispels Primal Roar stun.','b','c');





