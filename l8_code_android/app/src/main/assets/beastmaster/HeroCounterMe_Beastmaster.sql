select id_hero from HeroBaseInfo where name_dota_2 in ('Abaddon',
'Enchantress',
'Earthshaker',
'Keeper of the Light',
'Oracle',
'Timbersaw',
'Winter Wyvern')

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_abaddon','1. Aphotic Shield effectively nullifies Primal Roar single-target initiation on allies.
2. Even being initiated by Primal Roar, Borrowed Time can make Abaddon extremely difficult to kill.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_earthshaker','Having many summons out will cause Echo Slam to deal extra damage.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_enchantress','1. Untouchable negates Inner Beast attack speed bonus, making her hard to kill even if stunned by Primal Roar.
2. Enchantress can also take control of Beastmaster summons.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_keeper_of_the_light','Illuminate clears Beastmaster summons and is effective against his push.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_timbersaw','1. Reactive Armor is very effective against Beastmaster summons during laning stage and entire game.
2. Timbersaw can quickly clear Beastmaster summons with his nukes.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_oracle','1. False Promise effectively nullifies Primal Roar single-target initiation on allies.
2. Purifying Flames kills Beastmaster Hawk instantly and Boar with two casts.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_winter_wyvern','1. Cold Embrace mitigates Primal Roar single-target initiation on allies.
2. Winter Curse can turn Beastmaster summons and Inner Beast against his teammate.','b','c');

