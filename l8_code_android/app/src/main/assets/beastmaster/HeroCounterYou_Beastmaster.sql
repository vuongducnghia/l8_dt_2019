select id_hero from HeroBaseInfo where name_dota_2 in ('Storm Spirit',
'Ember Spirit',
'Queen of Pain',
'Monkey King',
'Phoenix',
'Sven',
'Templar Assassin',
'Tinker',
'Tiny',
'Troll Warlord',
'Ursa',
'Wraith King')


insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_ember_spirit','Heroes who rely on high mobility are especially vulnerable to Primal Roar','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_phoenix','Inner Beast helps Beastmaster team destroy Supernova quickly.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_queenofpain','Heroes who rely on high mobility are especially vulnerable to Primal Roar','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_tinker','Beastmaster Hawk can find Tinker easily while he is pushing or blinking into trees.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_storm_spirit','Heroes who rely on high mobility are especially vulnerable to Primal Roar','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_sven','Primal Roar takes Sven out of the fight completely, even with Black King Bar.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_templar_assassin','1. Attacks from Boars and Inner Beast will quickly wear down Refraction.
2. True Sight provided by Level 3 Necronomicon can see Templar Assassin in Meld.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_tiny','Primal Roar and the slow from Boars makes kiting Tiny very easy.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_troll_warlord','Primal Roar and the slow from Boars makes kiting Troll Warlord very easy.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_ursa','Primal Roar and the slow from Boars makes kiting Ursa very easy.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_wraith_king','Mana Break can deplete Wraith King mana and prevent him from using Reincarnation.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_beastmaster','id_monkey_king','1. Beastmaster Hawk can find Monkey King easily while he is sitting on tree.
2. Beastmaster Wild Axes can knock out Monkey King from tree.
3. Beastmaster Boars drastically reduce Monkey King attack speed, making it harder for him to gain Jingu Mastery stacks.','b','c');
