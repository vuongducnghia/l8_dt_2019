SELECT id_item FROM ItemBaseInfo WHERE name in ('Magic Stick',
'Silver Edge',
'Solar Crest',
'Assault Cuirass',
'Town Portal Scroll',
'Spirit Vessel',
'Diffusal Blade',
'Ghost Scepter')

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_town_portal_scroll','- Town Portal Scroll provides an easy means of escape from Bristleback, as he has no disables.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_magic_stick','- Magic Stick gains many charges due to Bristleback constant usage of Quill Spray, giving its holder an advantage in lane.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_ghost_scepter','- Ghost Scepter and its upgrade Ethereal Blade provide protection against Quill Spray, and Bristleback only means of dealing damage are through Quill Spray and basic attacks. ','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_assault_cuirass','- Assault Cuirass armor aura reduces Quill Spray area damage significantly.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_diffusal_blade','- Diffusal Blade burns Bristleback mana, which lessens his ability to spam Quill Spray.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_solar_crest','- Solar Crest reduces Bristleback armor greatly, allowing teammates to beat him down quickly.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_silver_edge','- Silver Edge removes Bristleback passive, taking away both his tankiness as well as his damage output.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_spirit_vessel','- Spirit Vessel cancels health regen and wears Bristleback down with percentage-based damage.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_manta_style','- He does not like purchasing dispelling.','b','c');

INSERT INTO HeroCounterMeItem(id_hero,id_item,guide,guide_sp,guide_vi)
VALUES ('id_bristleback','id_nullifier','- He does not like purchasing dispelling.','b','c');








