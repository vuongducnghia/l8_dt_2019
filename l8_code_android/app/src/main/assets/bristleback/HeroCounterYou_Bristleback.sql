select id_hero from HeroBaseInfo where name_dota_2 in ('Arc Warden',
'Broodmother',
'Chaos Knight',
'Huskar',
'Jakiro',
'Luna',
'Lycan',
'Meepo',
'Phantom Lance',
'Pudge',
'Riki',
'Rubick',
'Sand King',
'Venomancer',
'Arc Warden')

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_broodmother','- Quill Spray destroys spiderlings, and hits Broodmother even when she regenerating in her webs. This makes Bristleback one of her hardest counters in lane.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_chaos_knight','- Quill Spray quickly destroys Chaos Knight Phantasms.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_huskar','- The damage from Burning Spear can be reduced by turning away from Huskar.
- Bristleback can bait Huskar into using Life Break on him, then turn around and receive very little damage.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_jakiro','- Macropyre frequent damage ticks causes Bristleback to activate repeatedly, resulting in free damage as long as Bristleback faces away from Jakiro.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_luna','- Multiple instances of damage from Eclipse will trigger Bristleback passive repeatedly in a short time, dealing tremendous damage to Luna and her teammates.
- Luna damage burst may be high, but it will not be enough for her to take down Bristleback.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_lycan','- Lycan wolves deal little damage to Bristleback, and are easily killed by Quill Spray, even when they are invisible.
- Lycan lack of burst damage makes it very hard for him to kill Bristleback, especially when his back is turned.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_meepo','- Quill Spray usually hits Meepo clones, although Bristleback cannot turn his back when rooted with Earthbind.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_phantom_lancer','- Quill Spray cleans up Phantom Lancer illusions in no time.
- Phantom Lancer complete lack of burst damage makes it nearly impossible for him to kill Bristleback during chases.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_pudge','- Bristleback is one of the hardest heroes to gank with a Meat Hook and Dismember combo, and attempts to do so sometimes backfire on Pudge.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_riki','- Quill Spray will hit Riki even when he is invisible.
- Cloak and Dagger bonus backstab damage is rendered irrelevant against Bristleback.
- Smoke Screen does not stop Bristleback passive from activating.
- Due to Bristleback natural tankiness, he is an ideal Gem of True Sight icon.png Gem of True Sight carrier, directly countering Riki invisibility.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_rubick','- Bristleback abilities are nearly useless for Rubick to Spell Steal, as he does not gain the bonuses from Warpath.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_sand_king','- Quill Spray will hit Sand King inside Sand Storm, damaging him and putting Sand King Blink Dagger icon.png Blink Dagger on cooldown, preventing him from escaping.
- Epicenter numerous damage ticks will trigger Bristleback passive repeatedly during teamfights.','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_venomancer','- Heroes with damage-over-time abilities will not only proc Bristleback passive continuously, they will also do little harm to him when he is facing away from the source','b','c');

insert into HeroCounterYou (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_arc_warden','- Bristleback need not worry about charging into Magnetic Field, and quickly clearing up Arc Warden summons and Tempest Double with Quill Spray.
- Heroes with damage-over-time abilities will not only proc Bristleback passive continuously, they will also do little harm to him when he is facing away from the source','b','c');