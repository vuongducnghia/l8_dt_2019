select id_hero from HeroBaseInfo where name_dota_2 in ('Anti Mage',
'Doom',
'Faceless Void',
'Grimstroke',
'Legion Commander',
'Lich',
'Lion',
'Mars',
'Necrophos',
'Outworld Devourer',
'Razor',
'Silencer',
'Slardar',
'Timbersaw',
'Viper',
'Weaver',
'Axe',
'Terrorblade',
'Batrider',
'Shadow Demon',
'Lina')

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_antimage','- Mana Break quickly depletes Bristleback low mana, denying him the ability to spam Quill Spray and Viscous Nasal Goo.
- Bristleback has no way to prevent Anti-Mage from using Blink.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_axe','- Axe forces Bristleback to turn around with Berserker Call, allowing allies to attack him without fear of his passive.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_batrider','- Batrider turn rate slow from Sticky Napalm makes it harder for Bristleback to keep his back towards the enemy.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_doom_bringer','- Infernal Blade deals scaling damage and is useful against high health targets such as Bristleback. With Aghanim Scepter, it also breaks Bristleback passive, rendering him more vulnerable.
- Doom disables Bristleback low cooldown abilities, preventing him from triggering Warpath. Its pure damage also penetrates Bristleback.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_void','- Time Dilation prevents Bristleback from spamming his low-cooldown abilities, greatly reducing his damage and effectiveness.
- Chronosphere allows allies to resposition and hit Bristleback from the front, bypassing his defensive bonus from behind.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_legion_commander','- Duel makes Bristleback fight Legion Commander face to face, forcing him to take full damage.
- Press the Attack dispels the slow from stacks of Viscous Nasal Goo.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_lich','- Sinister Gaze forces Bristleback to face Lich, exposing him to damage.
- Frost Shield reduces damage to a single target, counteracting Warpath and Viscous Nasal Goo.
- Lich plethora of slows make it easy to kite Bristleback.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_lina','- Heroes who deal single instance burst damage will only proc Bristleback once','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_lion','- Mana Drain is a headache for Bristleback to deal with during the laning stage, as it denies him the ability to get last hits with Quill Spray.
- Finger of Death deals high damage to Bristleback, and only triggers his passive once.
- Heroes who deal single instance burst damage will only proc Bristleback once','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_necrophos','- Reaper Scythe and Heartstopper Aura deal percentage-based damage, ignoring Bristleback high health.
- Heartstopper Aura HP removal does not trigger Bristleback passive.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_outworld_devourer','- Outworld Devourer heavy pure damage output penetrates Bristleback tanky items.
- Sanity Eclipse only triggers Bristleback passive once, and deals high damage against Bristleback low intelligence.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_razor','- Eye of the Storm melts Bristleback armor, and makes him much less tanky.
- Static Link can completely drain Bristleback bonus damage from Warpath.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_shadow_demon','- Shadow Demon Soul Catcher hinders Bristleback tanking capabilities. Demonic Purge with Aghanim Scepter disables Bristleback passive.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_timbersaw','- Whirling Death deals damage as well while reducing 15% of Bristleback strength, which reduces Bristleback tankiness.
- Reactive Armor can sustain through multiple Viscous Nasal Goo and Quill Spray stacks.
- Timber Chain makes it hard for Bristleback to slow down Timbersaw with Viscous Nasal Goo.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_silencer','- Arcane Curse stacks every time Bristleback uses an ability, forcing him to stop spamming his abilities, or take heavy health degeneration.
- Glaives of Wisdom pure damage cuts through Bristleback tanky items. also steals intelligence both permanently and temporarily and cause mana issues for Bristleback
- Last Word and Global Silence stops Bristleback from using his abilities altogether.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_slardar','- Guardian Sprint allows Slardar to outrun Bristleback, even when slowed by Viscous Nasal Goo.
- Corrosive Haze armor reduction makes Bristleback much less tanky.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_terrorblade','- Terrorblade Sunder is not affected by the Bristleback damage reduction.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_viper','- Nethertoxin disables Bristleback passive.
- Viper Strike and Poison Attack slow down Bristleback movement speed so he unable to get out from Nethertoxin.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_weaver','- Shukuchi gets Weaver in front of Bristleback, negating the protection from the Bristleback passive.
- Weaver mana break talent depletes Bristleback mana pool, preventing him from spamming abilities.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_grimstroke','- Phantom Embrace silences Viscous Nasal Goo and Quill Spray.
- Ink Swell stuns Bristleback and gets allies in position faster with increased movement speed to attack his front, countering his passive ability.','b','c');

insert into HeroCounterMe (id_hero,id_hero_counter,guide,guide_sp,guide_vi)
values ('id_bristleback','id_mars','- Bulwark completely negates Quill Spray physical damage.','b','c');