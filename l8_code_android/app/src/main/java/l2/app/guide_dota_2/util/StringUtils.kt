package l2.app.guide_dota_2.util

/**
 * Created by nghia.vuong on 25,February,2020
 */
object StringUtils {
    fun FindText(searchMe: String, findMe: String): Boolean {
        val searchMeLength = searchMe.length
        val findMeLength = findMe.length
        var foundIt = false
        for (i in 0..searchMeLength - findMeLength) {
            if (searchMe.regionMatches(i, findMe, 0, findMeLength)) {
                foundIt = true
                return foundIt
            }
        }
        return foundIt
    }

    fun convertListId2ArrayList(listId: String): ArrayList<String>? {
        val arrayList: ArrayList<String> = ArrayList()
        var idItem = ""
        for (i in 0 until listId.length) {
            val charAt = listId[i].toString()
            if (charAt != ",") {
                idItem += charAt
                if (i == listId.length - 1) {
                    arrayList.add(idItem.trim { it <= ' ' })
                }
            } else {
                arrayList.add(idItem.trim { it <= ' ' })
                idItem = ""
            }
        }
        return arrayList
    }
}