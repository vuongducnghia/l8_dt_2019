package l2.app.guide_dota_2.data.source.local.skill

import com.google.firebase.database.FirebaseDatabase
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.table_sqlite_hero.*
import l2.app.guide_dota_2.util.AppExecutors


class SkillRepository : SkillRepositoryContract {

    private var firebaseDatabase = FirebaseDatabase.getInstance()

    private var appExecutors: AppExecutors = AppExecutors()

    init {
        init()
    }

    private fun init() {

    }

    override fun getSkillTable(callBack: SkillRepositoryContract.ResultSkillTable) {
        appExecutors.diskIO.execute {
            val list = L8Application.instance.db.skillTableQuery().getSkillTable()
            appExecutors.mainThread.execute {
                callBack.result(list)
            }
        }
    }

    override fun getSkillOfHero(idHero: String, callBack: SkillRepositoryContract.ResultListSkill) {
        appExecutors.diskIO.execute {
            val list = L8Application.instance.db.skillJoinQuery().getSkillOfHero(idHero)
            appExecutors.mainThread.execute {
                callBack.result(list)
            }
        }
    }

    override fun getSkillDetail(idSkill: String, callBack: SkillRepositoryContract.ResultSkillDetail) {
        appExecutors.diskIO.execute {
            val skillDetail = SkillDetail()
            skillDetail.baseInfo = L8Application.instance.db.skillQuery().getBaseInfo(idSkill)
            skillDetail.description = L8Application.instance.db.skillQuery().getDescription(idSkill)
            skillDetail.moreInfo = L8Application.instance.db.skillQuery().getMoreInfo(idSkill)
            skillDetail.photo = L8Application.instance.db.skillQuery().getPhoto(idSkill)
            skillDetail.play = L8Application.instance.db.skillQuery().getPlay(idSkill)
            appExecutors.mainThread.execute {
                callBack.result(skillDetail)
            }
        }
    }

    class SkillDetail {
        var idSkill: String? = null
        var baseInfo: SkillBaseInfo? = null
        var description: SkillDescription? = null
        var moreInfo: SkillMoreInfo? = null
        var photo: SkillPhoto? = null
        var play: SkillPlay? = null
    }

}
