package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SkillMoreInfo")
class SkillMoreInfo {
    @PrimaryKey
    @ColumnInfo(name = "id_skill")
    @NonNull
    var idSkill: String? = null

    @ColumnInfo(name = "info_skill")
    var info: String? = null

    @ColumnInfo(name = "info_skill_sp")
    var infoSp: String? = null

    @ColumnInfo(name = "info_skill_vi")
    var infoVi: String? = null

    @ColumnInfo(name = "value")
    var value: String? = null

}