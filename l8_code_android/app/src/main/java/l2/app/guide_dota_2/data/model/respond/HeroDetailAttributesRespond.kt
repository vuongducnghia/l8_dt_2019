package l2.app.guide_dota_2.data.model.respond

import l2.app.guide_dota_2.data.model.table_sqlite_hero.*

/**
 * Created by nghia.vuong on 04,March,2020
 */
class HeroDetailAttributesRespond {
    var idHero: String? = null
    var attributes: HeroAttributesTable? = null
    var howPlay: HeroHowPlay? = null
    var heroTalent: HeroTalent? = null
}