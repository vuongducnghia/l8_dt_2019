package l2.app.guide_dota_2.data.source.firebase.item

import SecurityData
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.util.AppExecutors
import l2.app.guide_dota_2.util.ConstantTable.Item_Base_Info_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Description_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Note_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Photo_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Property_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Up_Table

/**
 * Created by nghia.vuong on 23,June,2020
 */
class PostItem {

    private val TAG = "PostItem"

    private var appExecutors: AppExecutors = AppExecutors()
    private var firebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
    private val gson = Gson()

    fun postListIDItem() {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()
            firebaseDatabase.getReference(Item_Table).setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.itemTableQuery().getItemTable()), securityData.stringFromJNI()))
        }
    }

    fun postItem(id: String) {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()

            // postItemBaseInfo
            firebaseDatabase.getReference(Item_Base_Info_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.itemQuery().getBaseInfo(id)), securityData.stringFromJNI()))
            // postItemDescription
            firebaseDatabase.getReference(Item_Description_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.itemQuery().getDescription(id)), securityData.stringFromJNI()))
            // postItemNote
            firebaseDatabase.getReference(Item_Note_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.itemQuery().getNote(id)), securityData.stringFromJNI()))
            // postItemPhoto
            firebaseDatabase.getReference(Item_Photo_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.itemQuery().getPhoto(id)), securityData.stringFromJNI()))
            // postItemProperty
            firebaseDatabase.getReference(Item_Property_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.itemQuery().getProperty(id)), securityData.stringFromJNI()))
            // postItemUp
            firebaseDatabase.getReference(Item_Up_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.itemQuery().getUp(id)), securityData.stringFromJNI()))

        }
    }

    fun postAll() {
        appExecutors.diskIO.execute {
            val securityData = SecurityData()

            // postItemBaseInfo
            L8Application.instance.db.itemTableQuery().getBaseInfo().forEach {
                firebaseDatabase.getReference(Item_Base_Info_Table).child(it.id_item.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }
            // postItemDescription
            L8Application.instance.db.itemTableQuery().getDescription().forEach {
                firebaseDatabase.getReference(Item_Description_Table).child(it.id_item.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }
            // postItemNote
            L8Application.instance.db.itemTableQuery().getNote().forEach {
                firebaseDatabase.getReference(Item_Note_Table).child(it.id_item.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }
            // postItemPhoto
            L8Application.instance.db.itemTableQuery().getPhotoAndGroup().forEach {
                firebaseDatabase.getReference(Item_Photo_Table).child(it.id_item.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }
            // postItemProperty
            L8Application.instance.db.itemTableQuery().getProperty().forEach {
                firebaseDatabase.getReference(Item_Property_Table).child(it.id_item.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }
            // postItemUp
            L8Application.instance.db.itemTableQuery().getUp().forEach {
                firebaseDatabase.getReference(Item_Up_Table).child(it.id_item.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }

        }
    }

}