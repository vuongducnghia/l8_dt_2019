package l2.app.guide_dota_2.util

object ConstantTable {

    const val Hero_Table = "Hero"
    const val Hero_Attributes_Table = "HeroAttributesTable"
    const val Hero_Base_Info_Table = "HeroBaseInfo"
    const val Hero_Build_Items_Table = "HeroBuildItems"
    const val Hero_Counter_Me_Table = "HeroCounterMe"
    const val Hero_Counter_Me_Item_Table = "HeroCounterMeItem"
    const val Hero_Counter_You_Table = "HeroCounterYou"
    const val Hero_How_Play_Table = "HeroHowPlay"
    const val Hero_Photos_Table = "HeroPhotos"
    const val Hero_Role_Table = "HeroRole"
    const val Hero_Talent_Table = "HeroTalent"

    const val Item_Table = "Item"
    const val Item_Base_Info_Table = "ItemBaseInfo"
    const val Item_Description_Table = "ItemDescription"
    const val Item_Note_Table = "ItemNote"
    const val Item_Photo_Table = "ItemPhoto"
    const val Item_Property_Table = "ItemProperty"
    const val Item_Up_Table = "ItemUp"

    const val Skill_Table = "Skill"
    const val Skill_BaseInfo_Table = "SkillBaseInfo"
    const val Skill_Description_Table = "SkillDescription"
    const val Skill_MoreInfo_Table = "SkillMoreInfo"
    const val Skill_Photo_Table = "SkillPhoto"
    const val Skill_Play_Table = "SkillPlay"

    const val New_Data = "NewData"

}