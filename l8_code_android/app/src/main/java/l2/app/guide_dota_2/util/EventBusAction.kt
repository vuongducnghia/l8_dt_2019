package l2.app.guide_dota_2.util


object EventBusAction {

    class SkillAdapterEventBus(val idEvent: String, val message: String)
    class CheckCreateSQLiteEncode()
    class RequestResult(var requestCode: Int, var resultCode: Int, var data: Any)

}