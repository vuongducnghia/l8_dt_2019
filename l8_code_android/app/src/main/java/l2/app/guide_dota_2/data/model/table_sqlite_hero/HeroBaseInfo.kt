package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "HeroBaseInfo")
class HeroBaseInfo {
    @PrimaryKey
    @ColumnInfo(name = "id_hero")
    @NonNull
    var idHero: String? = null // Not encode idHero
    @ColumnInfo(name = "name_dota_1")
    var nameDota1: String? = null
    @ColumnInfo(name = "name_dota_2")
    var nameDota2: String? = null
    @ColumnInfo(name = "group_hero")
    var groupHero: String? = null

}
