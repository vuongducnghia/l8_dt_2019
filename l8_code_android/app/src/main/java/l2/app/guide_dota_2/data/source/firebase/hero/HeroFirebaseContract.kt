package l2.app.guide_dota_2.data.source.firebase.hero

/**
 * Created by nghia.vuong on 23,June,2020
 */
interface HeroFirebaseContract {

    fun createHeroTable()
    fun postAll() // Post all table hero to firebase
    fun postListIDHero() // Post table id hero to firebase
    fun postHero(id: String) // Post one hero to firebase

    fun updateByFirebase(id: String) // Get all table whit a id_hero at firebase
    fun getListIDHero(callBack: ResultListID) // Get data table hero at firebase
    interface ResultListID : Result {
        fun result(data: List<String>)
    }

    interface Result {
        fun noData(message: String){}
    }

    fun insertByFirebase(id: String)


}