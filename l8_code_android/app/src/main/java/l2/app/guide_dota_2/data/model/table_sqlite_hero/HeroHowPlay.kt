package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.annotation.NonNull

@Entity(tableName = "HeroHowPlay")
class HeroHowPlay {
    @PrimaryKey
    @ColumnInfo(name = "id_hero")
    @NonNull
    var idHero: String? = null // Not encode idHero
    @ColumnInfo(name = "en")
    var en: String? = null
    @ColumnInfo(name = "sp")
    var sp: String? = null
    @ColumnInfo(name = "vi")
    var vi: String? = null
}