package l2.app.guide_dota_2.ui.fragment.heroes

import android.content.Context
import l2.app.guide_dota_2.data.model.respond.HeroesFragmentRespond
import l2.app.guide_dota_2.data.source.firebase.hero.HeroFirebaseHelper
import l2.app.guide_dota_2.data.source.local.hero.HeroRepository
import l2.app.guide_dota_2.data.source.local.hero.HeroRepositoryContract
import l2.app.guide_dota_2.ui.fragment.detail_hero.FilterHero
import l2.app.guide_dota_2.util.AppLog

/**
 * Created by nghia.vuong on 24,June,2020
 */
class HeroesPresenter(var context: Context?, var view: HeroesContract.View) : HeroesContract.Presenter {

    private var TAG = "HeroesPresenter"
    private var dbHeroGetRepository: HeroRepository
    private var heroFirebaseHelper: HeroFirebaseHelper

    init {
        view.presenter = this
        dbHeroGetRepository = HeroRepository()
        heroFirebaseHelper = HeroFirebaseHelper()
    }

    override fun start() {

    }

    override fun end() {

    }

    override fun getFilterHeroes(filter: FilterHero) {
        if (context != null && view.isActive) {
            dbHeroGetRepository.filterHeroesFragment(filter, object : HeroRepositoryContract.ResultFilter {
                override fun result(data: List<HeroesFragmentRespond>) {
                    AppLog.d(TAG, "result ${data.size}")
                    view.getFilterHeroesFinish(data)
                }

                override fun noData() {}

            })

        }
    }

    override fun postHero(id: String) {
        if (context != null && view.isActive) {
            heroFirebaseHelper.postHero(id)
        }
    }
}