package l2.app.guide_dota_2.data.source.firebase.skill

import SecurityData
import android.app.Activity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.table_sqlite_hero.*
import l2.app.guide_dota_2.ui.UPDATE_BY_FB_SUCCESS_REQUEST_CODE
import l2.app.guide_dota_2.ui.UPDATE_BY_FB_TRACKING_REQUEST_CODE
import l2.app.guide_dota_2.util.AppExecutors
import l2.app.guide_dota_2.util.AppLog
import l2.app.guide_dota_2.util.Constant
import l2.app.guide_dota_2.util.ConstantTable.Skill_BaseInfo_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_Description_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_MoreInfo_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_Photo_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_Play_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_Table
import l2.app.guide_dota_2.util.EventBusAction
import org.greenrobot.eventbus.EventBus

/**
 * Created by nghia.vuong on 25,June,2020
 */
class GetSkill {

    private val TAG = "GetSkill"

    private var appExecutors: AppExecutors = AppExecutors()
    private var firebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
    private val gson = Gson()


    private fun <T> getDataFromFirebase(id: String, table: String, classOfT: Class<T>, callback: ResultCallBack) {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()

            firebaseDatabase.getReference(table).child(id).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    AppLog.w(TAG, "$table Failed to read value.", error.toException())
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    try {
                        val value = snapshot.value as String
                        if (value.isNotEmpty()) {
                            val decryption = securityData.decrypt(value, securityData.stringFromJNI())
                            val hero = gson.fromJson(decryption, classOfT)
                            appExecutors.diskIO.execute {
                                callback.send2SQLite(hero)
                            }
                        }
                    } catch (error: Exception) {
                        error.printStackTrace()
                        AppLog.e(TAG, "$table can not update new data error: ${error.message}")
                    }
                }
            })
        }
    }

    interface ResultCallBack {
        fun send2SQLite(t: Any?) {
            L8Application.instance.sharedHelper.setBoolean(Constant.FIRE_BASE_CREATE_NEW_SKILL, true)
            EventBus.getDefault().post(EventBusAction.RequestResult(UPDATE_BY_FB_SUCCESS_REQUEST_CODE, Activity.RESULT_OK, true))
        }
    }

    fun getListIDSkill(callBack: SkillFirebaseContract.ResultListIDSkill) {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()
            val myRef = firebaseDatabase.getReference(Skill_Table)
            myRef.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    AppLog.w(TAG, "Failed to read value.", error.toException())

                    appExecutors.mainThread.execute {
                        callBack.noData("Failed to read value. ${error.toException()}")
                    }
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val value = dataSnapshot.value as String
                    val decryption = securityData.decrypt(value, securityData.stringFromJNI())
                    val listSkillIdGSon = Gson().fromJson(decryption, Array<String>::class.java)
                    AppLog.w(TAG, "Get Skill Table value. ${listSkillIdGSon[0]} , ${listSkillIdGSon[1]}")
                    appExecutors.mainThread.execute {
                        callBack.result(listSkillIdGSon.toList())
                    }
                }
            })
        }
    }

    /*
     * 1. Get list id first
     * 2. Get item by id
     * */
    fun getAll() {
        getListIDSkill(object : SkillFirebaseContract.ResultListIDSkill {
            override fun result(data: List<String>) {
                if (data.size > 0) {
                    data.forEachIndexed { index, s ->
                        AppLog.w(TAG, "index $index Insert 2 SQLite Skill. $s")

                        insertByFirebase(s)

                    }
                }
            }
        })
    }

    private var isUpdateData = false

    fun insertByFirebase(id: String) {
        isUpdateData = false
        insertSkillTable(id)
        getBase(id)
    }

    // Get data at firebase and save to sqlite
    fun updateByFirebase(id: String) {
        isUpdateData = true
        getBase(id)
    }

    private fun getBase(id: String) {
        getBaseInfo(id)
        getDescription(id)
        getMoreInfo(id)
        getPhoto(id)
        getPlay(id)
    }

    private fun insertSkillTable(id: String) {
        appExecutors.diskIO.execute {
            val skill = Skill()
            skill.idSkill = id
            L8Application.instance.db.skillQuery().insertSkillTable(skill)
        }
    }

    private fun getBaseInfo(id: String) {
        getDataFromFirebase(id, Skill_BaseInfo_Table, SkillBaseInfo::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.skillQuery().updateBaseInfo(t as SkillBaseInfo)
                    } else {
                        L8Application.instance.db.skillQuery().insertBaseInfo(t as SkillBaseInfo)
                    }
                }
            }
        })
    }

    private fun getDescription(id: String) {
        getDataFromFirebase(id, Skill_Description_Table, SkillDescription::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.skillQuery().updateDescription(t as SkillDescription)
                    } else {
                        L8Application.instance.db.skillQuery().insertDescription(t as SkillDescription)
                    }
                }
            }
        })
    }

    private fun getMoreInfo(id: String) {
        getDataFromFirebase(id, Skill_MoreInfo_Table, SkillMoreInfo::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.skillQuery().updateMoreInfo(t as SkillMoreInfo)
                    } else {
                        L8Application.instance.db.skillQuery().insertMoreInfo(t as SkillMoreInfo)
                    }
                }
            }
        })
    }

    private fun getPhoto(id: String) {
        getDataFromFirebase(id, Skill_Photo_Table, SkillPhoto::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.skillQuery().updatePhoto(t as SkillPhoto)
                    } else {
                        L8Application.instance.db.skillQuery().insertPhoto(t as SkillPhoto)
                    }
                }
            }
        })
    }

    private fun getPlay(id: String) {
        getDataFromFirebase(id, Skill_Play_Table, SkillPlay::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.skillQuery().updatePlay(t as SkillPlay)
                    } else {
                        L8Application.instance.db.skillQuery().insertPlay(t as SkillPlay)
                    }
                }

                if (id.contains("id_x_marks_the_spot")) {
                    appExecutors.mainThread.execute {
                        L8Application.instance.sharedHelper.setBoolean(Constant.FIRE_BASE_CREATE_NEW_SKILL, true)
                        EventBus.getDefault().post(EventBusAction.RequestResult(UPDATE_BY_FB_SUCCESS_REQUEST_CODE, Activity.RESULT_OK, true))
                    }
                }
            }
        })
    }
}