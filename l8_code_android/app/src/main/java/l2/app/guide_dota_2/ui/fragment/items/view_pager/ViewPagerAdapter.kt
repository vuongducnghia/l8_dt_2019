package l2.app.guide_dota_2.ui.fragment.items.view_pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ViewPagerAdapter(private val titles: Array<String>, fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(p0: Int): Fragment {
        return when (p0) {
            0 -> ItemFragmentBase()
            else -> ItemFragmentUpgrades()
        }
    }

    override fun getCount(): Int {
        return titles.size
    }

}