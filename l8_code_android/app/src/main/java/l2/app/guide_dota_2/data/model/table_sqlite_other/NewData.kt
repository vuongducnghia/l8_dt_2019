package l2.app.guide_dota_2.data.model.table_sqlite_other

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "NewData")
class NewData {
    @PrimaryKey
    @ColumnInfo(name = "id")
    @NonNull
    var id: Int  = 0
    @ColumnInfo(name = "new_hero")
    var new_hero: String? = null
    @ColumnInfo(name = "new_item")
    var new_item: String? = null
    @ColumnInfo(name = "new_skill")
    var new_skill: String? = null

}
