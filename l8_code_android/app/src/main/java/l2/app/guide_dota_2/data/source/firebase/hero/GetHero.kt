package l2.app.guide_dota_2.data.source.firebase.hero

import SecurityData
import android.app.Activity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.table_sqlite_hero.*
import l2.app.guide_dota_2.ui.UPDATE_BY_FB_SUCCESS_REQUEST_CODE
import l2.app.guide_dota_2.ui.UPDATE_BY_FB_TRACKING_REQUEST_CODE
import l2.app.guide_dota_2.util.AppExecutors
import l2.app.guide_dota_2.util.AppLog
import l2.app.guide_dota_2.util.Constant
import l2.app.guide_dota_2.util.ConstantTable.Hero_Attributes_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Base_Info_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Build_Items_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Counter_Me_Item_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Counter_Me_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Counter_You_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_How_Play_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Photos_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Role_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Talent_Table
import l2.app.guide_dota_2.util.EventBusAction
import org.greenrobot.eventbus.EventBus

/**
 * Created by nghia.vuong on 25,June,2020
 */
class GetHero {

    private val TAG = "GetHero"

    private var appExecutors: AppExecutors = AppExecutors()
    private var firebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
    private val gson = Gson()

    private fun <T> getDataFromFirebase(id: String, table: String, classOfT: Class<T>, callback: GetFirebaseCallBack) {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()

            firebaseDatabase.getReference(table).child(id).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    AppLog.w(TAG, "$table Failed to read value.", error.toException())
                    callback.getFinish(null)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    try {
                        val value = snapshot.value as String
                        if (value.isNotEmpty()) {
                            val decryption = securityData.decrypt(value, securityData.stringFromJNI())
                            val t = gson.fromJson(decryption, classOfT)
                            appExecutors.diskIO.execute {
                                callback.getFinish(t)
                            }
                        }
                    } catch (error: Exception) {
                        error.printStackTrace()
                        AppLog.e(TAG, "$table can not update new data error: ${error.message}")
                        callback.getFinish(null)
                    }

                }
            })
        }
    }

    interface GetFirebaseCallBack {
        fun getFinish(t: Any?)
    }

    fun getListID(callBack: HeroFirebaseContract.ResultListID) {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()
            val myRef = firebaseDatabase.getReference(Hero_Table)
            myRef.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    AppLog.w(TAG, "Failed to read value.", error.toException())

                    appExecutors.mainThread.execute {
                        callBack.noData("Failed to read value. ${error.toException()}")
                    }
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val value = dataSnapshot.value as String
                    val decryption = securityData.decrypt(value, securityData.stringFromJNI())
                    val listHeroIdGSon = Gson().fromJson(decryption, Array<String>::class.java)
                    appExecutors.mainThread.execute {
                        callBack.result(listHeroIdGSon.toList())
                    }
                }
            })
        }

    }


    /*
    * 1. Get list id first
    * 2. Get item by id
    * */
    fun getAll() {
        getListID(object : HeroFirebaseContract.ResultListID {
            override fun result(data: List<String>) {
                val sharedHelper = L8Application.instance.sharedHelper
                if (data.size > 0) {
                    data.forEachIndexed { index, s ->
                        AppLog.w(TAG, "index $index Insert 2 SQLite hero. $s")
                        insertByFirebase(s)
                    }
                }
            }
        })
    }

    private var isUpdateData = false

    fun insertByFirebase(id: String) {
        isUpdateData = false
        insertHero(id)
        insertUpdateBase(id)
    }

    // Get data at firebase and save to sqlite
    fun updateByFirebase(id: String) {
        isUpdateData = true
        insertUpdateBase(id)
    }

    private fun insertUpdateBase(id: String) {
        insertUpdateAttributes(id)
        insertUpdateBaseInfo(id)
        insertUpdateBuildItems(id)
        insertUpdateCounterMe(id)
        insertUpdateCounterMeItem(id)
        insertUpdateCounterYou(id)
        insertUpdateHowPlay(id)
        insertUpdatePhotos(id)
        insertUpdateRole(id)
        insertUpdateTalent(id)
    }

    private fun insertHero(id: String) {
        appExecutors.diskIO.execute {
            val hero = Hero()
            hero.idHero = id
            L8Application.instance.db.heroQuery().insertHeroTable(hero)
        }
    }

    private fun insertUpdateAttributes(id: String) {
        getDataFromFirebase(id, Hero_Attributes_Table, HeroAttributesTable::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.heroQuery().updateAttributes(t as HeroAttributesTable)
                    } else {
                        L8Application.instance.db.heroQuery().insertAttributes(t as HeroAttributesTable)
                    }
                }
            }
        })
    }

    private fun insertUpdateBaseInfo(id: String) {
        getDataFromFirebase(id, Hero_Base_Info_Table, HeroBaseInfo::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.heroQuery().updateBaseInfo(t as HeroBaseInfo)
                    } else {
                        L8Application.instance.db.heroQuery().insertBaseInfo(t as HeroBaseInfo)
                    }
                }
            }
        })
    }

    private fun insertUpdateBuildItems(id: String) {
        getDataFromFirebase(id, Hero_Build_Items_Table, HeroBuildItems::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.heroQuery().updateBuildItems(t as HeroBuildItems)
                    } else {
                        L8Application.instance.db.heroQuery().insertBuildItems(t as HeroBuildItems)
                    }
                }
            }
        })
    }

    private fun insertUpdateCounterMe(id: String) {
        getDataFromFirebase(id, Hero_Counter_Me_Table, HeroCounterMe::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.heroQuery().updateCounterMe(t as HeroCounterMe)
                    } else {
                        L8Application.instance.db.heroQuery().insertCounterMe(t as HeroCounterMe)
                    }
                }
            }
        })
    }

    private fun insertUpdateCounterMeItem(id: String) {
        getDataFromFirebase(id, Hero_Counter_Me_Item_Table, HeroCounterMeItem::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.heroQuery().updateCounterMeItem(t as HeroCounterMeItem)
                    } else {
                        L8Application.instance.db.heroQuery().insertCounterMeItem(t as HeroCounterMeItem)
                    }
                }
            }
        })
    }

    private fun insertUpdateCounterYou(id: String) {
        getDataFromFirebase(id, Hero_Counter_You_Table, HeroCounterYou::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.heroQuery().updateCounterYou(t as HeroCounterYou)
                    } else {
                        L8Application.instance.db.heroQuery().insertCounterYou(t as HeroCounterYou)
                    }
                }
            }
        })
    }

    private fun insertUpdateHowPlay(id: String) {
        getDataFromFirebase(id, Hero_How_Play_Table, HeroHowPlay::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.heroQuery().updateHowPlay(t as HeroHowPlay)
                    } else {
                        L8Application.instance.db.heroQuery().insertHowPlay(t as HeroHowPlay)
                    }
                }

            }
        })
    }

    private fun insertUpdatePhotos(id: String) {
        getDataFromFirebase(id, Hero_Photos_Table, HeroPhotos::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (!isUpdateData) {
                        L8Application.instance.db.heroQuery().insertPhotos(t as HeroPhotos)
                    } else {
                        L8Application.instance.db.heroQuery().updatePhotos(t as HeroPhotos)
                    }
                }
            }
        })
    }

    private fun insertUpdateRole(id: String) {
        getDataFromFirebase(id, Hero_Role_Table, HeroRole::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (!isUpdateData) {
                        L8Application.instance.db.heroQuery().insertRoles(t as HeroRole)
                    } else {
                        L8Application.instance.db.heroQuery().updateRoles(t as HeroRole)
                    }
                }
            }
        })
    }

    private fun insertUpdateTalent(id: String) {
        getDataFromFirebase(id, Hero_Talent_Table, HeroTalent::class.java, object : GetFirebaseCallBack {
            override fun getFinish(t: Any?) {
                if (t != null) {
                    if (!isUpdateData) {
                        L8Application.instance.db.heroQuery().insertTalent(t as HeroTalent)
                    } else {
                        L8Application.instance.db.heroQuery().updateTalent(t as HeroTalent)
                    }
                }

                if (id.contains("id_zeus")) {
                    appExecutors.mainThread.execute {
                        L8Application.instance.sharedHelper.setBoolean(Constant.FIRE_BASE_CREATE_NEW_HERO, true)
                        EventBus.getDefault().post(EventBusAction.RequestResult(UPDATE_BY_FB_SUCCESS_REQUEST_CODE, Activity.RESULT_OK, true))
                    }
                }
            }
        })
    }
}