package l2.app.guide_dota_2.data.source.local.hero_dao

import androidx.room.Dao
import androidx.room.Query
import l2.app.guide_dota_2.data.model.table_sqlite_hero.*

@Dao
interface HeroTableDao {

    /*Insert*/

    /*Get*/
    @Query("SELECT * FROM Hero")
    fun getHeroTable(): List<String>

    @Query("SELECT * FROM HeroAttributesTable")
    fun getAttributesAll(): List<HeroAttributesTable>

    @Query("SELECT * FROM HeroBaseInfo")
    fun getBaseInfoAll(): List<HeroBaseInfo>

    @Query("SELECT * FROM HeroBuildItems")
    fun getBuildItemsAll(): List<HeroBuildItems>

    @Query("SELECT * FROM HeroCounterMe")
    fun getCounterMeAll(): List<HeroCounterMe>

    @Query("SELECT * FROM HeroCounterMeItem")
    fun getCounterMeItemAll(): List<HeroCounterMeItem>

    @Query("SELECT * FROM HeroCounterYou")
    fun getCounterYouAll(): List<HeroCounterYou>

    @Query("SELECT * FROM HeroHowPlay")
    fun getHowPlayAll(): List<HeroHowPlay>

    @Query("SELECT * FROM HeroPhotos")
    fun getPhotosAll(): List<HeroPhotos>

    @Query("SELECT * FROM HeroRole")
    fun getRolesAll(): List<HeroRole>

    @Query("SELECT * FROM HeroTalent")
    fun getTalentAll(): List<HeroTalent>

    /*Delete*/
    @Query("DELETE FROM Hero")
    fun deleteHeroTable()

    @Query("DELETE FROM HeroAttributesTable")
    fun deleteAttributesTable()

    @Query("DELETE FROM HeroBaseInfo")
    fun deleteBaseInfo()

    @Query("DELETE FROM HeroBuildItems")
    fun deleteBuildItems()

    @Query("DELETE FROM HeroCounterMe")
    fun deleteCounterMe()

    @Query("DELETE FROM HeroCounterMeItem")
    fun deleteCounterMeItem()

    @Query("DELETE FROM HeroCounterYou")
    fun deleteCounterYou()

    @Query("DELETE FROM HeroHowPlay")
    fun deleteHowPlay()

    @Query("DELETE FROM HeroPhotos")
    fun deletePhotos()

    @Query("DELETE FROM HeroRole")
    fun deleteRoles()

    @Query("DELETE FROM HeroTalent")
    fun deleteTalent()


}