package l2.app.guide_dota_2

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import l2.app.guide_dota_2.data.source.local.L8Database
import l2.app.guide_dota_2.helper.SharedPreferencesHelper
import l2.app.guide_dota_2.util.AppExecutors
import l2.app.guide_dota_2.util.CommonUtils
import l2.app.guide_dota_2.util.Constant
import l2.app.guide_dota_2.util.Constant.DEV
import l2.app.guide_dota_2.util.Constant.PRO


@SuppressLint("Registered")
class L8Application : Application() {

    lateinit var sharedHelper: SharedPreferencesHelper
    lateinit var db: L8Database
    var activeBuilder: String = ""

    companion object {
        var appExecutors: AppExecutors = AppExecutors()
        lateinit var instance: L8Application
        lateinit var context: Context
        private var androidId: String = ""
        var openSecond = false
        var isEncode = false
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext
        activeBuilder = when {
            applicationInfo.packageName.contains(DEV) -> DEV
            else -> PRO
        }
        sharedHelper = SharedPreferencesHelper(this)
        db = L8Database.getInstance(this)
        onFirstApp()
        encodeControl()
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }


    /*AndroidID*/
    private fun getAndroidID() {
        try {
            androidId = CommonUtils.getDeviceId(L8Application.instance)
        } catch (e: Exception) {
            var androidIdNotFound = sharedHelper.getString(Constant.ANDROID_ID_NOT_FOUND, "")
            if (androidIdNotFound.isNullOrEmpty()) {
                androidIdNotFound = System.currentTimeMillis().toString() + "L"
                sharedHelper.getString(Constant.ANDROID_ID_NOT_FOUND, androidIdNotFound)
            }
            androidId = androidIdNotFound + ""
        }
    }

    private fun onFirstApp() {
        openSecond = if (sharedHelper.getBoolean(Constant.IS_FIRST_APP, false)) {
            true
        } else {
            instance.sharedHelper.setBoolean(Constant.IS_FIRST_APP, true)
            false
        }
    }

    private fun encodeControl() {
        when (activeBuilder) {
            DEV -> isEncode = false
            PRO -> isEncode = true
        }
    }

}