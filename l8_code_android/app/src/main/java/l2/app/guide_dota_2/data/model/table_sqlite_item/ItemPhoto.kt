package l2.app.guide_dota_2.data.model.table_sqlite_item

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ItemPhoto")
class ItemPhoto {
    @PrimaryKey
    @ColumnInfo(name = "id_item")
    @NonNull
    var id_item: String? = null
    @ColumnInfo(name = "dota1")
    var dota1: ByteArray? = null
    @ColumnInfo(name = "dota2")
    var dota2: ByteArray? = null
    @ColumnInfo(name = "group_item")
    var group_item: String? = null
}
