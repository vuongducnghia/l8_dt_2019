package l2.app.guide_dota_2.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.ByteArrayOutputStream
import java.io.InputStream

object BitmapUtils {

    fun ByteToBitmap(bytes: ByteArray): Bitmap {
        val opt = BitmapFactory.Options()
        opt.inJustDecodeBounds = false
        return BitmapFactory.decodeByteArray(bytes, 0, bytes.size, opt)
    }

    fun BitmapToByte(bmp: Bitmap): ByteArray {
        val stream = ByteArrayOutputStream()
        bmp.compress(Bitmap.CompressFormat.PNG, 100, stream)
        return stream.toByteArray()
    }

    /*var inputStream = App.instance.applicationContext.contentResolver.openInputStream(uri)*/
    fun StreamToBitmap(inputStream: InputStream): Bitmap? {
        val opt = BitmapFactory.Options()
        opt.inJustDecodeBounds = false
        val bmp = BitmapFactory.decodeStream(inputStream, null, opt)
        inputStream.close()
        return bmp
    }

}