package l2.app.guide_dota_2.data.source.firebase.skill

import com.google.firebase.database.FirebaseDatabase
import l2.app.guide_dota_2.data.source.firebase.item.GetItem
import l2.app.guide_dota_2.data.source.firebase.item.ItemFirebaseContract
import l2.app.guide_dota_2.util.AppExecutors

/**
 * Created by nghia.vuong on 23,June,2020
 */
class SkillFirebaseHelper : SkillFirebaseContract {

    override fun createSkillTable() {
        GetSkill().getAll()
    }

    override fun postAll() {
        PostSkill().postAll()
    }

    override fun postListIDSkill() {
        PostSkill().postListIDSkill()
    }

    override fun postSkill(id: String) {
        PostSkill().postSkill(id)
    }

    override fun updateByFirebase(id: String) {
        GetSkill().updateByFirebase(id)
    }

    override fun insertByFirebase(id: String) {
        GetSkill().insertByFirebase(id)
    }

    override fun getListIDSkill(callBack: SkillFirebaseContract.ResultListIDSkill) {
        GetSkill().getListIDSkill(callBack)
    }


}