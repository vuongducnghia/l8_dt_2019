package l2.app.guide_dota_2.util

import android.view.View
import android.view.ViewGroup
import android.widget.TextView

object ViewUtils {

    fun findTextViews(v: View?, callBack: Listener)   {
        try {
            if (v is ViewGroup) {
                for (i in 0 until v.childCount) {
                    val child = v.getChildAt(i)
                    findTextViews(child, callBack) // recursively call this method
                }
            } else if (v is TextView) {
                callBack.doSomething(v)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    //region LISTENER
    interface Listener {
        fun doSomething(v:TextView)
    }
    //endregion


    fun enableDisableView(view: View, enabled: Boolean) {
        view.isEnabled = enabled
        if (view is ViewGroup) {
            for (idx in 0 until view.childCount) {
                enableDisableView(view.getChildAt(idx), enabled)
            }
        }
    }

}