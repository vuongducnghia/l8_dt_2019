package l2.app.guide_dota_2.ui.fragment.heroes.spinner

data class SpinnerRoleObj(val image: Int? = null, val description: String)