package l2.app.guide_dota_2.data.source.local.item_dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Update
import l2.app.guide_dota_2.data.model.table_sqlite_item.*

@Dao
interface ItemJoinDao {
    /*Insert*/
    /*Get*/
    @Query("SELECT count(*) FROM Item")
    fun getItemCount(): Int
    /*Update*/
    /*Delete*/
}