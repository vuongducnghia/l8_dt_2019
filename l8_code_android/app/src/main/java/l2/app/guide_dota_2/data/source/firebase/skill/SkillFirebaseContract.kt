package l2.app.guide_dota_2.data.source.firebase.skill

/**
 * Created by nghia.vuong on 23,June,2020
 */
interface SkillFirebaseContract {

    fun createSkillTable()
    fun postAll() // Post all table skill to firebase
    fun postListIDSkill() // Post table id skill to firebase
    fun postSkill(id: String) // Post one skill to firebase

    fun updateByFirebase(id: String) // Get all table whit a id_skill at firebase
    fun insertByFirebase(id: String)
    fun getListIDSkill(callBack: ResultListIDSkill) // Get data table skill at firebase
    interface ResultListIDSkill : Result {
        fun result(data: List<String>)
    }

    interface Result {
        fun noData(message: String){}
    }

}