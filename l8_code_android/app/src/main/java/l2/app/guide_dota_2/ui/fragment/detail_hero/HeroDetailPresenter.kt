package l2.app.guide_dota_2.ui.fragment.detail_hero

import android.content.Context
import l2.app.guide_dota_2.data.model.respond.HeroDetailFragmentRespond
import l2.app.guide_dota_2.data.model.respond.HeroDetailSkillRespond
import l2.app.guide_dota_2.data.source.local.hero.HeroRepository
import l2.app.guide_dota_2.data.source.local.hero.HeroRepositoryContract
import l2.app.guide_dota_2.data.source.local.skill.SkillRepository
import l2.app.guide_dota_2.data.source.local.skill.SkillRepositoryContract
import l2.app.guide_dota_2.util.AppLog

/**
 * Created by nghia.vuong on 24,June,2020
 */
class HeroDetailPresenter(var context: Context?, var view: HeroDetailContract.View) : HeroDetailContract.Presenter {
    private var TAG = "HeroDetailPresenter"
    private var dbHeroGetRepository: HeroRepository
    private var dbSkillRepository: SkillRepository

    init {
        view.presenter = this
        dbHeroGetRepository = HeroRepository()
        dbSkillRepository = SkillRepository()
    }

    override fun start() {

    }

    override fun end() {

    }

    override fun getHeroDetail(id: String) {
        if (context != null && view.isActive) {
            dbHeroGetRepository.getHeroDetailFragmentRespond(id, object : HeroRepositoryContract.ResultHeroFrg {
                override fun result(data: HeroDetailFragmentRespond) {
                    view.getHeroDetailFinish(data)
                }

                override fun noData() {}

            })
        }
    }

    override fun getListSkill(id: String) {
        if (context != null && view.isActive) {
            dbSkillRepository.getSkillOfHero(id, object : SkillRepositoryContract.ResultListSkill {
                override fun result(data: List<HeroDetailSkillRespond>) {
                    AppLog.d(TAG, "id $id data ${data.size} photoDota2 ${data[0].photoDota2!!.size}")
                    view.getListSkillFinish(data)
                }

                override fun noData() {}

            })
        }
    }

}