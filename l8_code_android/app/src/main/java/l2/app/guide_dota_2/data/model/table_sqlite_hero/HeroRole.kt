package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "HeroRole")
class HeroRole {
    @PrimaryKey
    @ColumnInfo(name = "id_hero")
    @NonNull
    var idHero: String? = null // Not encode idHero
    @ColumnInfo(name = "en")
    var forteEN: String? = null
    @ColumnInfo(name = "sp")
    var forteSP: String? = null
    @ColumnInfo(name = "vi")
    var forteVI: String? = null
}
