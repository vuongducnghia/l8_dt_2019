package l2.app.guide_dota_2.ui.fragment.detail_hero.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.ui.base.BaseDialog
import l2.app.guide_dota_2.util.ScreenUtils

const val PLAY: String = "PLAY"

class DialogHeroDetail : BaseDialog() {

    private val TAG = DialogHeroDetail::class.java.simpleName

    companion object {
        fun instance(id_hero: String): DialogHeroDetail {
            val dialogStyle = DialogHeroDetail()
            val bundle = Bundle()
            bundle.putString("id_hero", id_hero)
            dialogStyle.arguments = bundle
            return dialogStyle
        }
    }

    fun show(context: Context) {
        super.show(context, TAG)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity!!, R.style.fullScreenDialog)

        //region Edit Dialog's body

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        if (dialog.window != null) {
            val screenWidth = ScreenUtils.getScreenWidth(context!!)
            val screenHeight = ScreenUtils.getScreenHeight(context!!)
            val size = if (screenWidth < screenHeight) screenWidth else screenHeight
            dialog.window!!.setLayout(
                (size * 0.855).toInt(),
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
        }

        //endregion

        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_hero_detail_layout, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val idHero = arguments?.getString("id_hero")


    }

}