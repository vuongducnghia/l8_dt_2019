package l2.app.guide_dota_2.ui.customs

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.*
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.ui.base.BaseDialog

class DialogCustomView : BaseDialog() {

    private val TAG = DialogCustomView::class.java.simpleName

    companion object {
        fun instance(): DialogCustomView {
            val dialogStyle = DialogCustomView()
            val bundle = Bundle()
            dialogStyle.arguments = bundle
            return dialogStyle
        }
    }

    fun show(context: Context) {
        super.show(context, TAG)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(activity!!, R.style.fullScreenDialog)

        //region Edit Dialog's body

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        if (dialog.window != null) {
            dialog.window!!.attributes.dimAmount = 0.5f
            dialog.window!!.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND)
            dialog.window!!.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        }

        //endregion

        return dialog
    }



}