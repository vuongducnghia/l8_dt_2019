package l2.app.guide_dota_2.data.source.firebase.hero

import SecurityData
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.util.AppExecutors
import l2.app.guide_dota_2.util.ConstantTable.Hero_Attributes_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Base_Info_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Build_Items_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Counter_Me_Item_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Counter_Me_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Counter_You_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_How_Play_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Photos_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Role_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Table
import l2.app.guide_dota_2.util.ConstantTable.Hero_Talent_Table

/**
 * Created by nghia.vuong on 23,June,2020
 */
class PostHero {

    private val TAG = "PostHero"

    private var appExecutors: AppExecutors = AppExecutors()
    private var firebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
    private val gson = Gson()

    fun postListIDHero() {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()
            firebaseDatabase.getReference(Hero_Table).setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.heroTableQuery().getHeroTable()), securityData.stringFromJNI()))
        }
    }

    private fun sendToFirebase(table: String, id: String, it: Any) {
        firebaseDatabase
            .getReference(table)
            .child(id)
            .setValue(Gson().toJson(it))
    }

    fun postHero(id: String) {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()
            val db = L8Application.instance.db

            // 1. Get data from sqlite normal
            val heroAttributesTable = db.heroQuery().getAttributes(id)
            heroAttributesTable?.let {
                it.health = securityData.encrypt(it.health ?: "", securityData.stringFromJNI())
                it.mana = securityData.encrypt(it.mana ?: "", securityData.stringFromJNI())
                it.armor = securityData.encrypt(it.armor ?: "", securityData.stringFromJNI())
                it.damage = securityData.encrypt(it.damage ?: "", securityData.stringFromJNI())
                it.movementSpeed = securityData.encrypt(it.movementSpeed ?: "", securityData.stringFromJNI())
                sendToFirebase(Hero_Attributes_Table, id, it)
            }

            val heroBaseInfo = db.heroQuery().getBaseInfo(id)
            heroBaseInfo?.let {
                it.nameDota1 = securityData.encrypt(it.nameDota1 ?: "", securityData.stringFromJNI())
                it.nameDota2 = securityData.encrypt(it.nameDota2 ?: "", securityData.stringFromJNI())
                it.groupHero = securityData.encrypt(it.groupHero ?: "", securityData.stringFromJNI())
                sendToFirebase(Hero_Base_Info_Table, id, it)
            }

            val heroBuildItems = db.heroQuery().getBuildItems(id)
            heroBuildItems?.let {
                it.itemStarting = securityData.encrypt(it.itemStarting ?: "", securityData.stringFromJNI())
                it.itemEarly = securityData.encrypt(it.itemEarly ?: "", securityData.stringFromJNI())
                it.itemCode = securityData.encrypt(it.itemCode ?: "", securityData.stringFromJNI())
                it.itemSituational = securityData.encrypt(it.itemSituational ?: "", securityData.stringFromJNI())
                sendToFirebase(Hero_Build_Items_Table, id, it)
            }

            val heroCounterMe = db.heroQuery().getCounterMe(id)
            heroCounterMe?.let {
                it.idHeroCounter = securityData.encrypt(it.idHeroCounter ?: "", securityData.stringFromJNI())
                it.guide = securityData.encrypt(it.guide ?: "", securityData.stringFromJNI())
                it.guideSP = securityData.encrypt(it.guideSP ?: "", securityData.stringFromJNI())
                it.guideVI = securityData.encrypt(it.guideVI ?: "", securityData.stringFromJNI())
                sendToFirebase(Hero_Counter_Me_Table, id, it)
            }

            val heroCounterMeItem = db.heroQuery().getCounterMeItem(id)
            heroCounterMeItem?.let {
                it.idItem = securityData.encrypt(it.idItem ?: "", securityData.stringFromJNI())
                it.guide = securityData.encrypt(it.guide ?: "", securityData.stringFromJNI())
                it.guideSP = securityData.encrypt(it.guideSP ?: "", securityData.stringFromJNI())
                it.guideVI = securityData.encrypt(it.guideVI ?: "", securityData.stringFromJNI())
                sendToFirebase(Hero_Counter_Me_Item_Table, id, it)
            }

            val heroCounterYou = db.heroQuery().getCounterYou(id)
            heroCounterYou?.let {
                it.idHeroCounter = securityData.encrypt(it.idHeroCounter ?: "", securityData.stringFromJNI())
                it.guide = securityData.encrypt(it.guide ?: "", securityData.stringFromJNI())
                it.guideSP = securityData.encrypt(it.guideSP ?: "", securityData.stringFromJNI())
                it.guideVI = securityData.encrypt(it.guideVI ?: "", securityData.stringFromJNI())
                sendToFirebase(Hero_Counter_You_Table, id, it)
            }

            val heroHowPlay = db.heroQuery().getHowPlay(id)
            heroHowPlay?.let {
                it.en = securityData.encrypt(it.en ?: "", securityData.stringFromJNI())
                it.sp = securityData.encrypt(it.sp ?: "", securityData.stringFromJNI())
                it.vi = securityData.encrypt(it.vi ?: "", securityData.stringFromJNI())
                sendToFirebase(Hero_How_Play_Table, id, it)
            }

            // postHeroPhotos
            // Not encode photos
            firebaseDatabase.getReference(Hero_Photos_Table)
                .child(id)
                .setValue(Gson().toJson(L8Application.instance.db.heroQuery().getPhotos(id)))

            val heroRole = db.heroQuery().getRoles(id)
            heroRole?.let {
                it.forteEN = securityData.encrypt(it.forteEN ?: "", securityData.stringFromJNI())
                it.forteSP = securityData.encrypt(it.forteSP ?: "", securityData.stringFromJNI())
                it.forteVI = securityData.encrypt(it.forteVI ?: "", securityData.stringFromJNI())
                sendToFirebase(Hero_Role_Table, id, it)
            }

            val heroTalent = db.heroQuery().getTalent(id)
            heroTalent?.let {
                it.lv10a = securityData.encrypt(it.lv10a ?: "", securityData.stringFromJNI())
                it.lv10b = securityData.encrypt(it.lv10b ?: "", securityData.stringFromJNI())
                it.lv15a = securityData.encrypt(it.lv15a ?: "", securityData.stringFromJNI())
                it.lv15b = securityData.encrypt(it.lv15b ?: "", securityData.stringFromJNI())
                it.lv20a = securityData.encrypt(it.lv20a ?: "", securityData.stringFromJNI())
                it.lv20b = securityData.encrypt(it.lv20b ?: "", securityData.stringFromJNI())
                it.lv25a = securityData.encrypt(it.lv25a ?: "", securityData.stringFromJNI())
                it.lv25b = securityData.encrypt(it.lv25b ?: "", securityData.stringFromJNI())
                sendToFirebase(Hero_Talent_Table, id, it)
            }

        }
    }

    fun postAll() {
        appExecutors.diskIO.execute {
            val securityData = SecurityData()
            val db = L8Application.instance.db

            // postHeroAttributes
            L8Application.instance.db.heroTableQuery().getAttributesAll().forEach {
                val id = it.idHero.toString()
                val heroAttributesTable = db.heroQuery().getAttributes(id)
                heroAttributesTable?.let { it_ ->
                    it_.health = securityData.encrypt(it_.health ?: "", securityData.stringFromJNI())
                    it_.mana = securityData.encrypt(it_.mana ?: "", securityData.stringFromJNI())
                    it_.armor = securityData.encrypt(it_.armor ?: "", securityData.stringFromJNI())
                    it_.damage = securityData.encrypt(it_.damage ?: "", securityData.stringFromJNI())
                    it_.movementSpeed = securityData.encrypt(it_.movementSpeed ?: "", securityData.stringFromJNI())
                    sendToFirebase(Hero_Attributes_Table, id, it_)
                }
            }

            // postHeroBaseInfo
            L8Application.instance.db.heroTableQuery().getBaseInfoAll().forEach {
                val id = it.idHero.toString()
                val heroBaseInfo = db.heroQuery().getBaseInfo(id)
                heroBaseInfo?.let { it_ ->
                    it_.nameDota1 = securityData.encrypt(it_.nameDota1 ?: "", securityData.stringFromJNI())
                    it_.nameDota2 = securityData.encrypt(it_.nameDota2 ?: "", securityData.stringFromJNI())
                    it_.groupHero = securityData.encrypt(it_.groupHero ?: "", securityData.stringFromJNI())
                    sendToFirebase(Hero_Base_Info_Table, id, it_)
                }
            }

            // postHeroBuildItems
            L8Application.instance.db.heroTableQuery().getBuildItemsAll().forEach {
                val id = it.idHero.toString()
                val heroBuildItems = db.heroQuery().getBuildItems(id)
                heroBuildItems?.let { it_ ->
                    it_.itemStarting = securityData.encrypt(it_.itemStarting ?: "", securityData.stringFromJNI())
                    it_.itemEarly = securityData.encrypt(it_.itemEarly ?: "", securityData.stringFromJNI())
                    it_.itemCode = securityData.encrypt(it_.itemCode ?: "", securityData.stringFromJNI())
                    it_.itemSituational = securityData.encrypt(it_.itemSituational ?: "", securityData.stringFromJNI())
                    sendToFirebase(Hero_Build_Items_Table, id, it_)
                }
            }

            // postHeroCounterMe
            L8Application.instance.db.heroTableQuery().getCounterMeAll().forEach {
                val id = it.idHero.toString()
                val heroCounterMe = db.heroQuery().getCounterMe(id)
                heroCounterMe?.let { it_ ->
                    it_.idHeroCounter = securityData.encrypt(it_.idHeroCounter ?: "", securityData.stringFromJNI())
                    it_.guide = securityData.encrypt(it_.guide ?: "", securityData.stringFromJNI())
                    it_.guideSP = securityData.encrypt(it_.guideSP ?: "", securityData.stringFromJNI())
                    it_.guideVI = securityData.encrypt(it_.guideVI ?: "", securityData.stringFromJNI())
                    sendToFirebase(Hero_Counter_Me_Table, id, it_)
                }
            }

            // postHeroCounterMeItem
            L8Application.instance.db.heroTableQuery().getCounterMeItemAll().forEach {
                val id = it.idHero.toString()
                val heroCounterMeItem = db.heroQuery().getCounterMeItem(id)
                heroCounterMeItem?.let { it_ ->
                    it_.idItem = securityData.encrypt(it_.idItem ?: "", securityData.stringFromJNI())
                    it_.guide = securityData.encrypt(it_.guide ?: "", securityData.stringFromJNI())
                    it_.guideSP = securityData.encrypt(it_.guideSP ?: "", securityData.stringFromJNI())
                    it_.guideVI = securityData.encrypt(it_.guideVI ?: "", securityData.stringFromJNI())
                    sendToFirebase(Hero_Counter_Me_Item_Table, id, it_)
                }
            }

            // postHeroCounterYou
            L8Application.instance.db.heroTableQuery().getCounterYouAll().forEach {
                val id = it.idHero.toString()
                val heroCounterYou = db.heroQuery().getCounterYou(id)
                heroCounterYou?.let { it_ ->
                    it_.idHeroCounter = securityData.encrypt(it_.idHeroCounter ?: "", securityData.stringFromJNI())
                    it_.guide = securityData.encrypt(it_.guide ?: "", securityData.stringFromJNI())
                    it_.guideSP = securityData.encrypt(it_.guideSP ?: "", securityData.stringFromJNI())
                    it_.guideVI = securityData.encrypt(it_.guideVI ?: "", securityData.stringFromJNI())
                    sendToFirebase(Hero_Counter_You_Table, id, it_)
                }
            }

            // postHeroHowPlay
            L8Application.instance.db.heroTableQuery().getHowPlayAll().forEach {
                val id = it.idHero.toString()
                val heroHowPlay = db.heroQuery().getHowPlay(id)
                heroHowPlay?.let { it_ ->
                    it_.en = securityData.encrypt(it_.en ?: "", securityData.stringFromJNI())
                    it_.sp = securityData.encrypt(it_.sp ?: "", securityData.stringFromJNI())
                    it_.vi = securityData.encrypt(it_.vi ?: "", securityData.stringFromJNI())
                    sendToFirebase(Hero_How_Play_Table, id, it_)
                }
            }

            // postHeroPhotos
            // Not encode photos
            L8Application.instance.db.heroTableQuery().getPhotosAll().forEach {
                val id = it.idHero.toString()
                firebaseDatabase.getReference(Hero_Photos_Table)
                    .child(id)
                    .setValue(Gson().toJson(L8Application.instance.db.heroQuery().getPhotos(id)))
            }

            // postHeroRole
            L8Application.instance.db.heroTableQuery().getRolesAll().forEach {
                val id = it.idHero.toString()
                val heroRole = db.heroQuery().getRoles(id)
                heroRole?.let { it_ ->
                    it_.forteEN = securityData.encrypt(it_.forteEN ?: "", securityData.stringFromJNI())
                    it_.forteSP = securityData.encrypt(it_.forteSP ?: "", securityData.stringFromJNI())
                    it_.forteVI = securityData.encrypt(it_.forteVI ?: "", securityData.stringFromJNI())
                    sendToFirebase(Hero_Role_Table, id, it_)
                }
            }

            // postHeroTalent
            L8Application.instance.db.heroTableQuery().getTalentAll().forEach {
                val id = it.idHero.toString()
                val heroTalent = db.heroQuery().getTalent(id)
                heroTalent?.let { it_ ->
                    it_.lv10a = securityData.encrypt(it_.lv10a ?: "", securityData.stringFromJNI())
                    it_.lv10b = securityData.encrypt(it_.lv10b ?: "", securityData.stringFromJNI())
                    it_.lv15a = securityData.encrypt(it_.lv15a ?: "", securityData.stringFromJNI())
                    it_.lv15b = securityData.encrypt(it_.lv15b ?: "", securityData.stringFromJNI())
                    it_.lv20a = securityData.encrypt(it_.lv20a ?: "", securityData.stringFromJNI())
                    it_.lv20b = securityData.encrypt(it_.lv20b ?: "", securityData.stringFromJNI())
                    it_.lv25a = securityData.encrypt(it_.lv25a ?: "", securityData.stringFromJNI())
                    it_.lv25b = securityData.encrypt(it_.lv25b ?: "", securityData.stringFromJNI())
                    sendToFirebase(Hero_Talent_Table, id, it_)
                }
            }

        }
    }


}