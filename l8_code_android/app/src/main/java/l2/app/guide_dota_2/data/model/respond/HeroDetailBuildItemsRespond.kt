package l2.app.guide_dota_2.data.model.respond

import l2.app.guide_dota_2.data.model.table_sqlite_item.ItemPhoto

/**
 * Created by nghia.vuong on 05,March,2020
 */
class HeroDetailBuildItemsRespond {
    var listStarting: List<ByteArray>? = null
    var listEarly: List<ByteArray>? = null
    var listCode: List<ByteArray>? = null
    var listSituational: List<ByteArray>? = null
}