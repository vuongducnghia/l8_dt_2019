package l2.app.guide_dota_2.ui.fragment.heroes


import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Parcelable
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.AdapterView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView.OnFlingListener
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.fragment_heroes.*
import kotlinx.android.synthetic.main.fragment_heroes_layout.*
import kotlinx.android.synthetic.main.layout_group_language.*
import kotlinx.android.synthetic.main.spinner_role_view.view.*
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.respond.HeroesFragmentRespond
import l2.app.guide_dota_2.data.source.local.hero.heroCount
import l2.app.guide_dota_2.ui.base.BaseFragment
import l2.app.guide_dota_2.ui.customs.DialogAlert
import l2.app.guide_dota_2.ui.fragment.detail_hero.FilterHero
import l2.app.guide_dota_2.ui.fragment.heroes.recyclerview_heroes.AnimationItem
import l2.app.guide_dota_2.ui.fragment.heroes.recyclerview_heroes.RecyclerViewHeroesAdapter
import l2.app.guide_dota_2.ui.fragment.heroes.recyclerview_heroes.RecyclerViewHeroesItemDecoration
import l2.app.guide_dota_2.ui.fragment.heroes.spinner.SpinnerRoleAdapter
import l2.app.guide_dota_2.ui.fragment.heroes.spinner.SpinnerRoleObj
import l2.app.guide_dota_2.util.AppLog
import l2.app.guide_dota_2.util.Constant.LANGUAGE
import l2.app.guide_dota_2.util.RecyclerViewUtils.smoothScroll
import l2.app.guide_dota_2.util.ScreenUtils
import l2.app.guide_dota_2.util.ViewUtils
import kotlin.math.abs
import kotlin.math.sign


/**
 * h_main
 */
class HeroesFragment : BaseFragment(), HeroesContract.View, AdapterView.OnItemSelectedListener {
    private val TAG = "HeroesFragment"
    override lateinit var presenter: HeroesContract.Presenter
    override var isActive: Boolean = false
        get() = isAdded

    private var dataHeroes = ArrayList<HeroesFragmentRespond>()
    private var recyclerViewHeroesAdapter: RecyclerViewHeroesAdapter? = null

    private var scrollDelay = 200L
    private var isFirst = true
    private val filter = FilterHero()

    private var colorDefault = -1
    private var colorSelected = -1

    private var saveList: Parcelable? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        presenter = HeroesPresenter(requireContext(), this)
        presenter.start()
        colorDefault = ContextCompat.getColor(requireContext(), R.color.text_base_color)
        colorSelected = ContextCompat.getColor(requireContext(), R.color.text_selected)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_heroes, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adRequest: AdRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)

        appBarHeroesLayout.outlineProvider = null

        recyclerViewLayout()
        recyclerViewAdapter()
        recyclerViewEffect()

        // filterHeroes(filter)
        if (dataHeroes.size == 0) {
            rvHeroes.post {
                // Filter
                filter.attributes = "1"
                filterHeroes(filter)
                // Change UI
                groupAttributesSelected(txtFilterStrength)
            }
        }

        txtClick()
        filter()
        language()
        spinnerRole()

    }

    override fun onStop() {
        super.onStop()
        saveList = rvHeroes.layoutManager?.onSaveInstanceState()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.end()
    }

    private fun showHeroes(heroes: List<HeroesFragmentRespond>) {
        dataHeroes.clear()
        dataHeroes.addAll(heroes)
        notifyData()
        if (dataHeroes.size != 0) {
            rvHeroes.postDelayed({
                smoothScroll(rvHeroes, 0, 1000)
            }, scrollDelay)
        }
    }

    private fun filterHeroes(filter: FilterHero) {
        presenter.getFilterHeroes(filter)
    }

    override fun getFilterHeroesFinish(data: List<HeroesFragmentRespond>) {
        AppLog.d(TAG, "heroes.size ${data.size}")
        showHeroes(data)
        showHeroCount()
    }

    private fun recyclerViewAdapter() {
        recyclerViewHeroesAdapter = RecyclerViewHeroesAdapter(dataHeroes)
        rvHeroes.adapter = recyclerViewHeroesAdapter
        recyclerViewHeroesAdapter?.setOnClickListener(object :
            RecyclerViewHeroesAdapter.OnClickListener {
            override fun onItemClick(view: View, position: Int, id_hero: String?) {
                val bundle = Bundle()
                bundle.putString("id_hero", id_hero)
                Navigation.findNavController(view).navigate(R.id.action_heroesFragment_to_heroDetailFragment, bundle)
            }

            override fun onPostHero(id: String) {
                presenter.postHero(id)
            }
        })
    }

    private fun recyclerViewLayout() {
        val mNoOfColumns = ScreenUtils.calculateNoOfColumns(requireContext(), 90f)
        rvHeroes.layoutManager = GridLayoutManager(requireContext(), mNoOfColumns)
        rvHeroes.onFlingListener = object : OnFlingListener() {
            val MAX_VELOCITY_Y = 7000
            override fun onFling(velocityX: Int, velocityY: Int): Boolean {

                val absY = abs(velocityY)
                return when {
                    absY in 1000..2000 -> {
                        autoScroll(velocityY)
                        true
                    }
                    absY > MAX_VELOCITY_Y -> { // Hold speed fling, i dont want fling so fast.
                        val y = MAX_VELOCITY_Y * sign(velocityY.toDouble()).toInt()
                        rvHeroes.fling(velocityX, y)
                        true
                    }
                    else -> {
                        false
                    }
                }
            }
        }
    }


    private fun recyclerViewEffect() {
        /*Animation: jp.wasabeef:recyclerview-animators:2.2.3 not work with notifyDataSetChanged()*/
        // rvHeroes.itemAnimator = SlideInUpAnimator()
        // rvHeroes.adapter = AlphaInAnimationAdapter(recyclerViewHeroesAdapter)
        recyclerViewAnimation()

        /*RecyclerView Snap Item*/
//        val snapHelper =  LinearSnapHelper()
//        snapHelper.attachToRecyclerView(rvHeroes)

        /*RecyclerView ItemDecoration*/
        val space = resources.getDimensionPixelOffset(R.dimen.space_top_hero_items)
        rvHeroes.addItemDecoration(RecyclerViewHeroesItemDecoration(space))
        // rvHeroes.addItemDecoration(RecyclerViewHeroesItemDecoration2(Color.TRANSPARENT, 0f))
    }

    private fun recyclerViewAnimation() {
        var anim = R.anim.layout_animation_fall_down
        if (isFirst) anim = R.anim.layout_animation_fall_down_fast
        recyclerViewAnimation(anim)
    }

    private fun recyclerViewAnimation(anim: Int) {
        val animationItem = AnimationItem("Fall down", anim)
        val controller =
            AnimationUtils.loadLayoutAnimation(requireContext(), animationItem.resourceId)
        rvHeroes.layoutAnimation = controller
        rvHeroes.scheduleLayoutAnimation()
    }

    private fun notifyData() {
        rvHeroes.adapter?.notifyDataSetChanged()
        recyclerViewAnimation()
        if (isFirst) rvHeroes.post { isFirst = false }

    }

    private fun txtClick() {
        txtItems.setOnClickListener {
            Navigation.findNavController(it).navigate(R.id.action_heroesFragment_to_itemsFragment)
        }

        txtCopyRight.setOnClickListener {
            DialogAlert().setMessage(resources.getString(R.string.copy_right_english) + "\n\n" + resources.getString(R.string.copy_right_viet_name)).show(requireContext())
        }

        // Gold
        tvGold.setOnClickListener { notifyGold() }
        vGold.setOnClickListener { notifyGold() }
    }

    private fun notifyGold() {
        val toast = Toast.makeText(requireContext(), "7K gold for remove ads", Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }

    @SuppressLint("SetTextI18n")
    private fun filter() {
        txtFilterStrength.setOnClickListener {
            // Filter
            filter.attributes = "1"
            filterHeroes(filter)

            // Change UI
            groupAttributesSelected(txtFilterStrength)
        }

        txtFilterAgility.setOnClickListener {
            // Filter
            filter.attributes = "2"
            filterHeroes(filter)

            // Change UI
            groupAttributesSelected(txtFilterAgility)
        }

        txtFilterIntelligence.setOnClickListener {
            // Filter
            filter.attributes = "3"
            filterHeroes(filter)

            // Change UI
            groupAttributesSelected(txtFilterIntelligence)
        }

        txtMyLike.setOnClickListener {
            txtMyLikeClick = !txtMyLikeClick
            if (txtMyLikeClick) {
                // Filter
                filter.liked = "notEmpty"
                filterHeroes(filter)

                // Change UI
                txtMyLike.setTextColor(colorSelected)
            } else {
                // Filter
                filter.liked = ""
                filterHeroes(filter)

                // Change UI
                txtMyLike.setTextColor(colorDefault)
            }
        }

        viewClose.setOnClickListener {
            // spinner role
            filter.role = ""
            spinner.setSelection(0)

            // Txt liked
            filter.liked = ""
            txtMyLikeClick = false
            txtMyLike.setTextColor(colorDefault)

            // Filter
            filterHeroes(filter)
        }

        showHeroCount()
    }

    private fun showHeroCount() {
        Handler().postDelayed({
            if (heroCount > 0) {
                txtHeroCount.visibility = View.VISIBLE
                txtHeroCount.text = "[ ${recyclerViewHeroesAdapter?.itemCount} / $heroCount Heroes ]"
            } else {
                txtHeroCount.visibility = View.GONE
            }
        }, 100)
    }

    private var txtMyLikeClick = false

    private fun groupAttributesSelected(textView: TextView) {
        ViewUtils.findTextViews(groupAttributes, object : ViewUtils.Listener {
            override fun doSomething(v: TextView) {
                v.setTextColor(colorDefault)
            }
        })

        textView.setTextColor(colorSelected)

    }

    private fun language() {
        setIcLang()
        ivLanguage.setOnClickListener {
            if (groupLanguage.visibility == View.VISIBLE) groupLanguage.visibility = View.GONE else groupLanguage.visibility = View.VISIBLE
        }

        ivLangKingdom.setOnClickListener { changeLanguage(0) }
        ivLangSpain.setOnClickListener { changeLanguage(1) }
        ivLangVietName.setOnClickListener { changeLanguage(2) }
    }

    private fun changeLanguage(keyLang: Int) {
        groupLanguage.visibility = View.GONE
        L8Application.instance.sharedHelper.setInt(LANGUAGE, keyLang)
        setIcLang()
    }

    private fun setIcLang() {
        when (L8Application.instance.sharedHelper.getInt(LANGUAGE, -1)) {
            0 -> ivLanguage.setBackgroundResource(R.drawable.ic_lang_kingdom)
            1 -> ivLanguage.setBackgroundResource(R.drawable.ic_lang_spain)
            2 -> ivLanguage.setBackgroundResource(R.drawable.ic_lang_vietnam)
        }
    }

    private val roleArray = ArrayList<SpinnerRoleObj>()
    private fun spinnerRole() {
        if (roleArray.size == 0) {
            val roles = resources.getStringArray(R.array.role_arrays)
            roles.forEach {
                roleArray.add(SpinnerRoleObj(null, it))
            }
        }
        spinner.adapter = SpinnerRoleAdapter(requireContext(), roleArray)
        spinner.onItemSelectedListener = this
    }

    private fun autoScroll(velocityY: Int) {
        rvHeroes.post {
            val duration = 5 * 1000
            val toTop = 0
            val toBottom = recyclerViewHeroesAdapter?.itemCount!! - 1
            if (velocityY > 0) {
                smoothScroll(rvHeroes, toBottom, duration)
            } else {
                smoothScroll(rvHeroes, toTop, duration)
            }
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    private var spinnerSelected = 0

    @SuppressLint("DefaultLocale", "SetTextI18n")
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        if (position == spinnerSelected) return
        spinnerSelected = position
        AppLog.d(TAG, "position $position spinnerSelected $spinnerSelected")
        if (view != null) {
            // Format data
            val role = view.txtRole?.text.toString()
            (spinner.adapter as SpinnerRoleAdapter).setPositionSelected(position)

            if (position == 0) {
                // Filter
                filter.role = ""

                // Change UI
                view.txtRole.setTextColor(colorDefault)
                viewClose.visibility = View.GONE

            } else {
                // Filter
                filter.role = role

                // Change UI
                view.txtRole.setTextColor(colorSelected)
                viewClose.visibility = View.VISIBLE
            }

            filterHeroes(filter)
        }
    }
}
