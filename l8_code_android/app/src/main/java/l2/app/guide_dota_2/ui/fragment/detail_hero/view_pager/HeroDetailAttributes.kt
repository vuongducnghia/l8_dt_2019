package l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_hero_detail_attributes.*
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.respond.HeroDetailAttributesRespond
import l2.app.guide_dota_2.data.source.local.hero.HeroRepository
import l2.app.guide_dota_2.data.source.local.hero.HeroRepositoryContract
import l2.app.guide_dota_2.ui.base.BaseFragment
import l2.app.guide_dota_2.ui.fragment.detail_hero.HeroDetailFragment

class HeroDetailAttributes : BaseFragment() {

    private lateinit var parent: HeroDetailFragment
    private lateinit var dbHeroGetRepository: HeroRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parent = (parentFragment as HeroDetailFragment)
        dbHeroGetRepository = HeroRepository()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_hero_detail_attributes, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent.idHeroDetail?.let {
            dbHeroGetRepository.getHeroAttributesFrg(parent.idHeroDetail!!, object : HeroRepositoryContract.ResultAttributesFrg {
                override fun result(data: HeroDetailAttributesRespond) {

                    txtHealth.text = data.attributes?.health ?: ""
                    txtMana.text = data.attributes?.mana ?: ""
                    txtArmor.text = data.attributes?.armor ?: ""
                    txtDamage.text = data.attributes?.damage ?: ""
                    txtMovementSpeed.text = data.attributes?.movementSpeed ?: ""
                    txtHowPlay.text = data.howPlay?.en ?: ""

                    // Talent
                    talent10Left.text = data.heroTalent?.lv10a ?: ""
                    talent10Right.text = data.heroTalent?.lv10b ?: ""
                    talent15Left.text = data.heroTalent?.lv15a ?: ""
                    talent15Right.text = data.heroTalent?.lv15b ?: ""
                    talent20Left.text = data.heroTalent?.lv20a ?: ""
                    talent20Right.text = data.heroTalent?.lv20b ?: ""
                    talent25Left.text = data.heroTalent?.lv25a ?: ""
                    talent25Right.text = data.heroTalent?.lv25b ?: ""
                }

                override fun noData() {

                }

            })
        }
    }
}

