package l2.app.guide_dota_2.util

object Constant {

    const val DEV = "dev"
    const val STG = "stg"
    const val PRO = "pro"

    const val IS_FIRST_APP = "is_first_app"

    const val DATA_BASE_NAME = "database_normal"
    const val DATA_BASE_ENCODE_NAME = "database_encode"

    const val ANDROID_ID_NOT_FOUND = "ANDROID_ID_NOT_FOUND"

    // Data base item base
    const val CONSUMABLES = "consumables"
    const val ATTRIBUTES = "attributes"
    const val EQUIPMENT = "armaments"
    const val MISCELLANEOUS = "arcane"
    const val SECRET_SHOP = "secret_shop"


    // Data base item upgrades
    const val ACCESSORIES = "common"
    const val SUPPORT = "support"
    const val MAGICAL = "caster"
    const val WEAPONS = "weapons"
    const val ARMOR = "armor"
    const val ARTIFACETS = "artifacts"

    const val LANGUAGE = "language_app"
    const val LIKED = "bLiked"

    const val FIRST_OPEN_APP = "FIRST_OPEN_APP"

    // Firebase

    const val INSERT_NEW_HERO_DONE = "INSERT_NEW_HERO_DONE"
    const val INSERT_NEW_ITEM_DONE = "INSERT_NEW_ITEM_DONE"
    const val INSERT_NEW_SKILL_DONE = "INSERT_NEW_SKILL_DONE"

    const val FIRE_BASE_CREATE_NEW_HERO = "FIRE_BASE_CREATE_NEW_HERO"
    const val FIRE_BASE_CREATE_NEW_ITEM = "FIRE_BASE_CREATE_NEW_ITEM"
    const val FIRE_BASE_CREATE_NEW_SKILL = "FIRE_BASE_CREATE_NEW_SKILL"
}