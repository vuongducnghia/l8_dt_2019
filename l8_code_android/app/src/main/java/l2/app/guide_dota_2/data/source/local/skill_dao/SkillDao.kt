package l2.app.guide_dota_2.data.source.local.skill_dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import l2.app.guide_dota_2.data.model.table_sqlite_hero.*

@Dao
interface SkillDao {

    /*Insert*/
    @Insert(onConflict = REPLACE)
    fun insertSkillTable(data: Skill)

    @Insert(onConflict = REPLACE)
    fun insertBaseInfo(data: SkillBaseInfo)

    @Insert(onConflict = REPLACE)
    fun insertDescription(data: SkillDescription)

    @Insert(onConflict = REPLACE)
    fun insertMoreInfo(data: SkillMoreInfo)

    @Insert(onConflict = REPLACE)
    fun insertPhoto(data: SkillPhoto)

    @Insert(onConflict = REPLACE)
    fun insertPlay(data: SkillPlay)

    /*Get*/
    @Query("SELECT * FROM SkillBaseInfo WHERE id_skill like :id")
    fun getBaseInfo(id: String): SkillBaseInfo

    @Query("SELECT * FROM SkillDescription WHERE id_skill like :id")
    fun getDescription(id: String): SkillDescription

    @Query("SELECT * FROM SkillMoreInfo WHERE id_skill like :id")
    fun getMoreInfo(id: String): SkillMoreInfo

    @Query("SELECT * FROM SkillPhoto WHERE id_skill like :id")
    fun getPhoto(id: String): SkillPhoto

    @Query("SELECT * FROM SkillPlay WHERE id_skill like :id")
    fun getPlay(id: String): SkillPlay

    /*Update*/
    @Update
    fun updateBaseInfo(data: SkillBaseInfo)

    @Update
    fun updateDescription(data: SkillDescription)

    @Update
    fun updateMoreInfo(data: SkillMoreInfo)

    @Update
    fun updatePhoto(data: SkillPhoto)

    @Update
    fun updatePlay(data: SkillPlay)

    /*Delete*/
    @Delete
    fun deleteBaseInfo(data: SkillBaseInfo)

    @Delete
    fun deleteDescription(data: SkillDescription)

    @Delete
    fun deleteMoreInfo(data: SkillMoreInfo)

    @Delete
    fun deletePhoto(data: SkillPhoto)

    @Delete
    fun deletePlay(data: SkillPlay)

}