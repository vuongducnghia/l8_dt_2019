package l2.app.guide_dota_2.data.source.local.hero

import l2.app.guide_dota_2.data.model.respond.*
import l2.app.guide_dota_2.data.model.table_sqlite_hero.Skill
import l2.app.guide_dota_2.ui.fragment.detail_hero.FilterHero

interface HeroRepositoryContract {

    interface BaseResult {
        fun noData(){}
    }

    fun getHeroTable(callBack: ResultHeroTable)
    interface ResultHeroTable : BaseResult {
        fun result(data: List<String>)
    }

    fun getHeroAttributesFrg(idHero: String, callBack: ResultAttributesFrg)
    interface ResultAttributesFrg : BaseResult {
        fun result(data: HeroDetailAttributesRespond)
    }

    fun getHeroDetailFragmentRespond(idHero: String, callBack: ResultHeroFrg)
    interface ResultHeroFrg : BaseResult {
        fun result(data: HeroDetailFragmentRespond)
    }

    fun filterHeroesFragment(filter: FilterHero, callBack: ResultFilter)
    interface ResultFilter : BaseResult {
        fun result(data: List<HeroesFragmentRespond>)
    }

    fun getHeroCounterYou(idHero: String, callBack: ResultHeroCounterYou)
    interface ResultHeroCounterYou : BaseResult {
        fun result(data: List<HeroDetailCounterRespond>)
    }

    fun getHeroCounterMeItem(idHero: String, callBack: ResultHeroCounterMeItem)
    interface ResultHeroCounterMeItem : BaseResult {
        fun result(data: List<HeroDetailCounterRespond>)
    }

    fun getHeroCounterMe(idHero: String, callBack: ResultHeroCounterMe)
    interface ResultHeroCounterMe : BaseResult {
        fun result(data: List<HeroDetailCounterRespond>)
    }

    fun getHeroBuildItems(idHero: String, callBack: ResultHeroBuildItems)
    interface ResultHeroBuildItems : BaseResult {
        fun result(data: HeroDetailBuildItemsRespond)
    }


    /* fun loadHeroFriendAndCounter(idHero: String, callBack: ResultHeroFriendAndCounter)
     interface ResultHeroFriendAndCounter {
         fun resultCounterMe(counterMe: List<CounterFrg>)
         fun resultCounterYou(counterYou: List<CounterFrg>)
         fun noData()
     }*/


}