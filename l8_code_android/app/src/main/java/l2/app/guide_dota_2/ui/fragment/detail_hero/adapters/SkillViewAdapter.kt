package l2.app.guide_dota_2.ui.fragment.detail_hero.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.FrameLayout
import android.widget.ImageView
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.respond.HeroDetailSkillRespond
import l2.app.guide_dota_2.util.BitmapUtils


class SkillViewAdapter(private val list: List<HeroDetailSkillRespond>) : BaseAdapter() {

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any? {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: View?
        val gridItemViewHolder: GridItemViewHolder
        if (convertView == null) {
            view = LayoutInflater.from(parent!!.context).inflate(R.layout.fragment_item_view_holder, parent, false)
            gridItemViewHolder = GridItemViewHolder(view)
            view.tag = gridItemViewHolder
        } else {
            view = convertView
            gridItemViewHolder = view.tag as GridItemViewHolder
        }


        // Show data
        val image = list[position].photoDota2
        gridItemViewHolder.imgItem.setImageBitmap(image?.let { BitmapUtils.ByteToBitmap(it) })
        gridItemViewHolder.bodyItem.setOnClickListener {
            mOnClickListener.onItemClick(position)
        }

        return view
    }


    class GridItemViewHolder(itemView: View) {
        var imgItem: ImageView = itemView.findViewById(R.id.imgView)
        var bodyItem: FrameLayout = itemView.findViewById(R.id.bodyItem)
    }

    interface OnClickListener {
        fun onItemClick(position:Int)
    }

    private lateinit var mOnClickListener: OnClickListener
    fun setOnClickListener(onClickListener: OnClickListener) {
        mOnClickListener = onClickListener
    }

}

