package l2.app.guide_dota_2.util

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SmoothScroller
import kotlin.math.abs


object RecyclerViewUtils {

    /** Smoothly scroll to specified position allowing for interval specification. <br></br>
     * Note crude deceleration towards end of scroll
     * @param rv        Your RecyclerView
     * @param toPos     Position to scroll to
     * @param duration  Approximate desired duration of scroll (ms)
     * @throws IllegalArgumentException
     */
    // See androidx.recyclerview.widget.LinearSmoothScroller
    @Throws(IllegalArgumentException::class)
    fun smoothScroll(rv: RecyclerView, toPos: Int, duration: Int) {
        if (rv.getChildAt(0) == null) return
        val TARGET_SEEK_SCROLL_DISTANCE_PX = 10000
        var itemHeight =
            rv.getChildAt(0).height   // Height of first visible view! NB: ViewGroup method!
        itemHeight += 33 // Example pixel Adjustment for decoration?
        val fvPos =
            (rv.layoutManager as LinearLayoutManager?)!!.findFirstCompletelyVisibleItemPosition()
        var i = abs((fvPos - toPos) * itemHeight)
        if (i == 0) {
            i = abs(rv.getChildAt(0).y).toInt()
        }
        val totalPix = i // Best guess: Total number of pixels to scroll

        val smoothScroller: SmoothScroller = object : LinearSmoothScroller(rv.context) {

            override fun getVerticalSnapPreference(): Int {
                return SNAP_TO_ANY
            }

            override fun calculateTimeForScrolling(dx: Int): Int {
                var ms = (duration * dx / (totalPix.toFloat())).toInt()

                // Now double the interval for the last fling.
                if (dx < TARGET_SEEK_SCROLL_DISTANCE_PX) {
                    ms *= 2
                } // Crude deceleration!

                println("RecyclerViewUtils.calculateTimeForScrolling ms2 $ms")
                //lg(format("For dx=%d we allot %dms", dx, ms));
                return ms
            }

            /*override fun calculateTimeForDeceleration(dx: Int): Int {
                var ms = (duration * dx / (totalPix.toFloat())).toInt()
                if (dx < TARGET_SEEK_SCROLL_DISTANCE_PX) {
                    ms *= 2
                }
                return ms
            }*/
        }

        //lg(format("Total pixels from = %d to %d = %d [ itemHeight=%dpix ]", fvPos, toPos, totalPix, itemHeight));
        smoothScroller.targetPosition = toPos
        rv.layoutManager!!.startSmoothScroll(smoothScroller)
    }


}