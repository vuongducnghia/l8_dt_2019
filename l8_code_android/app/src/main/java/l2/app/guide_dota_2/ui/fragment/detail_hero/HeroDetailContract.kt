package l2.app.guide_dota_2.ui.fragment.detail_hero

import l2.app.guide_dota_2.data.model.respond.HeroDetailFragmentRespond
import l2.app.guide_dota_2.data.model.respond.HeroDetailSkillRespond
import l2.app.guide_dota_2.ui.base.BasePresenter
import l2.app.guide_dota_2.ui.base.BaseView

/**
 * Created by nghia.vuong on 24,June,2020
 */
interface HeroDetailContract  {

    interface Presenter : BasePresenter {
        fun getHeroDetail(id:String)
        fun getListSkill(id:String)
    }

    interface View : BaseView<Presenter> {
        fun getHeroDetailFinish(heroDetailFrg: HeroDetailFragmentRespond)
        fun getListSkillFinish(result: List<HeroDetailSkillRespond>)
    }
}