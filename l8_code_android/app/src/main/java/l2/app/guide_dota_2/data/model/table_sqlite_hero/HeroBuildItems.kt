package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "HeroBuildItems")
class HeroBuildItems {
    @PrimaryKey
    @ColumnInfo(name = "id_hero")
    @NonNull
    var idHero: String? = null // Not encode idHero
    @ColumnInfo(name = "item_starting")
    var itemStarting: String? = null
    @ColumnInfo(name = "item_early")
    var itemEarly: String? = null
    @ColumnInfo(name = "item_code")
    var itemCode: String? = null
    @ColumnInfo(name = "item_situational")
    var itemSituational: String? = null

}
