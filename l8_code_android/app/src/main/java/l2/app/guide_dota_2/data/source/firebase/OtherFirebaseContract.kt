package l2.app.guide_dota_2.data.source.firebase

import l2.app.guide_dota_2.data.model.table_sqlite_other.NewData

/**
 * Created by nghia.vuong on 23,June,2020
 */
interface OtherFirebaseContract {

    fun createNewData()
    fun postNewData()
    fun getNewData(callBack: ResultData) // Get and insert new data
    interface ResultData : Result {
        fun result(data: List<NewData>)
    }

    interface Result {
        fun noData(message: String){}
    }

}