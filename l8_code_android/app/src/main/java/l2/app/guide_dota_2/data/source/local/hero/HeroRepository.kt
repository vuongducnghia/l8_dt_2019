package l2.app.guide_dota_2.data.source.local.hero

import android.annotation.SuppressLint
import com.google.firebase.database.FirebaseDatabase
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.respond.HeroDetailAttributesRespond
import l2.app.guide_dota_2.data.model.respond.HeroDetailBuildItemsRespond
import l2.app.guide_dota_2.data.model.respond.HeroDetailCounterRespond
import l2.app.guide_dota_2.data.model.respond.HeroesFragmentRespond
import l2.app.guide_dota_2.ui.fragment.detail_hero.FilterHero
import l2.app.guide_dota_2.util.AppExecutors
import l2.app.guide_dota_2.util.AppLog
import l2.app.guide_dota_2.util.Constant.LIKED
import l2.app.guide_dota_2.util.StringUtils
import java.util.*
import kotlin.collections.ArrayList

var heroCount = -1

class HeroRepository : HeroRepositoryContract {

    private var TAG = "HeroRepository"

    private var firebaseDatabase = FirebaseDatabase.getInstance()

    private var appExecutors: AppExecutors = AppExecutors()
    private val listMyLikeHero = ArrayList<String>()

    private var dataHeroes = ArrayList<HeroesFragmentRespond>()


    init {
        initDataHeroes()
    }

    private fun initDataHeroes() {
        if (dataHeroes.size == 0) {
            appExecutors.diskIO.execute {
                dataHeroes = L8Application.instance.db.heroJoinQuery().getHeroesFragment() as ArrayList<HeroesFragmentRespond>
                heroCount = L8Application.instance.db.heroJoinQuery().getHeroCount()
            }
        }
    }

    override fun getHeroTable(callBack: HeroRepositoryContract.ResultHeroTable) {
        appExecutors.diskIO.execute {
            val hero = L8Application.instance.db.heroTableQuery().getHeroTable()
            appExecutors.mainThread.execute {
                callBack.result(hero)
            }
        }
    }

    @SuppressLint("DefaultLocale")
    override fun filterHeroesFragment(filter: FilterHero, callBack: HeroRepositoryContract.ResultFilter) {
        AppLog.d(TAG, "filter group ${filter.attributes} role ${filter.role.trim().toLowerCase(Locale.getDefault())} liked ${filter.liked} ")
        AppLog.d(TAG, "dataHeroes ${dataHeroes.size}")

        appExecutors.diskIO.execute {
            val group = filter.attributes
            val role = filter.role.trim().toLowerCase()
            val liked = filter.liked
            val last = ArrayList<HeroesFragmentRespond>()

            val heroes = dataHeroes.filter { it.groupHero!!.contains(group) }

            last.clear()
            last.addAll(heroes)

            if (role.isNotEmpty()) {
                val heroesRole = last.filter {
                    it.role!!.trim().toLowerCase().contains(role)
                }
                last.clear()
                last.addAll(heroesRole)
            }

            if (liked.isNotEmpty()) {
                // Clear data
                listMyLikeHero.clear()

                // Get and store idHero
                L8Application.instance.sharedHelper.getAll()?.forEach { it: Map.Entry<String, Any?> ->
                    if (it.key.contains(LIKED)) {
                        listMyLikeHero.add(it.key.substring(LIKED.length, it.key.length - 1).trim())
                    }
                }

                // Filter by idHero
                val listHeroesFragmentRespond = ArrayList<HeroesFragmentRespond>()
                for (i in listMyLikeHero.indices) {
                    for (j in last.indices) {
                        if (last[j].idHero.contains(listMyLikeHero[i])) {
                            listHeroesFragmentRespond.add(last[j])
                            break
                        }
                    }
                }

                last.clear()
                last.addAll(listHeroesFragmentRespond)

            }

            appExecutors.mainThread.execute {
                callBack.result(last)
            }
        }
    }

    // 2. HeroDetail
    override fun getHeroDetailFragmentRespond(idHero: String, callBack: HeroRepositoryContract.ResultHeroFrg) {
        appExecutors.diskIO.execute {
            val heroBaseInfo = L8Application.instance.db.heroJoinQuery().getHeroDetailFragmentRespond(idHero)
            appExecutors.mainThread.execute {
                callBack.result(heroBaseInfo)
            }
        }
    }


    override fun getHeroAttributesFrg(idHero: String, callBack: HeroRepositoryContract.ResultAttributesFrg) {
        appExecutors.diskIO.execute {
            val heroAttributeFrg = HeroDetailAttributesRespond()
            heroAttributeFrg.attributes = L8Application.instance.db.heroQuery().getAttributes(idHero)
            heroAttributeFrg.heroTalent = L8Application.instance.db.heroQuery().getTalent(idHero)
            heroAttributeFrg.howPlay = L8Application.instance.db.heroQuery().getHowPlay(idHero)
            appExecutors.mainThread.execute {
                callBack.result(heroAttributeFrg)
            }
        }
    }

    override fun getHeroCounterMe(idHero: String, callBack: HeroRepositoryContract.ResultHeroCounterMe) {
        appExecutors.diskIO.execute {
            val list = ArrayList<HeroDetailCounterRespond>()
            list.addAll(L8Application.instance.db.heroJoinQuery().getHeroCounterMe(idHero))
            list.addAll(L8Application.instance.db.heroJoinQuery().getHeroCounterMeItem(idHero))
            appExecutors.mainThread.execute {
                callBack.result(list)
            }
        }
    }

    override fun getHeroCounterMeItem(idHero: String, callBack: HeroRepositoryContract.ResultHeroCounterMeItem) {
        appExecutors.diskIO.execute {
            val list = L8Application.instance.db.heroJoinQuery().getHeroCounterMeItem(idHero)
            appExecutors.mainThread.execute {
                callBack.result(list)
            }
        }
    }

    override fun getHeroCounterYou(idHero: String, callBack: HeroRepositoryContract.ResultHeroCounterYou) {
        appExecutors.diskIO.execute {
            val list = L8Application.instance.db.heroJoinQuery().getHeroCounterYou(idHero)
            appExecutors.mainThread.execute {
                callBack.result(list)
            }
        }
    }


    override fun getHeroBuildItems(idHero: String, callBack: HeroRepositoryContract.ResultHeroBuildItems) {
        appExecutors.diskIO.execute {
            val heroBuildItems = L8Application.instance.db.heroQuery().getBuildItems(idHero)
            val heroDetailBuildItemsRespond = HeroDetailBuildItemsRespond()
            val itemStarting = heroBuildItems?.itemStarting?.let { StringUtils.convertListId2ArrayList(it) }
            val itemEarly = heroBuildItems?.itemEarly?.let { StringUtils.convertListId2ArrayList(it) }
            val itemCode = heroBuildItems?.itemCode?.let { StringUtils.convertListId2ArrayList(it) }
            val itemSituational = heroBuildItems?.itemSituational?.let { StringUtils.convertListId2ArrayList(it) }
            heroDetailBuildItemsRespond.listStarting = getItemPhotoDota2(itemStarting)
            heroDetailBuildItemsRespond.listEarly = getItemPhotoDota2(itemEarly)
            heroDetailBuildItemsRespond.listCode = getItemPhotoDota2(itemCode)
            heroDetailBuildItemsRespond.listSituational = getItemPhotoDota2(itemSituational)
            appExecutors.mainThread.execute {
                callBack.result(heroDetailBuildItemsRespond)
            }
        }
    }

    private fun getItemPhotoDota2(items: ArrayList<String>?): ArrayList<ByteArray> {
        val list = ArrayList<ByteArray>()
        items?.forEach { id ->
            L8Application.instance.db.itemQuery().getPhoto(id.trim()).dota2?.let { list.add(it) }
        }
        return list
    }

}
