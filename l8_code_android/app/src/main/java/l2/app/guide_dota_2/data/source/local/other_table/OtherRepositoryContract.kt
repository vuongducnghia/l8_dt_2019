package l2.app.guide_dota_2.data.source.local.other_table

import l2.app.guide_dota_2.data.model.table_sqlite_other.NewData

interface OtherRepositoryContract {

    interface BaseResult {
        fun noData(){}
    }

    fun getNewData(callBack: ResultData)
    interface ResultData : BaseResult {
        fun result(data: List<NewData>)
    }

    fun insertNewData(newData: NewData)


}