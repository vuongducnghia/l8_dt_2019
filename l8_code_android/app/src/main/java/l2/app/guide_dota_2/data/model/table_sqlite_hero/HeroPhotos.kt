package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "HeroPhotos")
class HeroPhotos {
    @PrimaryKey
    @ColumnInfo(name = "id_hero")
    @NonNull
    var idHero: String? = null
    @ColumnInfo(name = "dota1")
    var dota1: ByteArray? = null
    @ColumnInfo(name = "dota2")
    var dota2: ByteArray? = null
    @ColumnInfo(name = "avatar")
    var avatar: ByteArray? = null
}
