package l2.app.guide_dota_2.ui.fragment.items

import l2.app.guide_dota_2.data.source.local.item.GroupItemObject
import l2.app.guide_dota_2.data.source.local.item.ItemDetail
import l2.app.guide_dota_2.ui.base.BasePresenter
import l2.app.guide_dota_2.ui.base.BaseView

/**
 * Created by nghia.vuong on 24,June,2020
 */
interface ItemsContract {

    interface Presenter : BasePresenter {
        fun getItemBase()
        fun getItemDetail(id: String)
        fun getItemDetailLeft(id: String)
        fun getItemUpgrades()
    }

    interface View : BaseView<Presenter> {
        fun getItemBaseFinish(data: GroupItemObject)
        fun getItemDetailFinish(data: ItemDetail)
        fun getItemDetailLeftFinish(data: ItemDetail)
        fun getItemUpgradesFinish(data: GroupItemObject)
    }
}