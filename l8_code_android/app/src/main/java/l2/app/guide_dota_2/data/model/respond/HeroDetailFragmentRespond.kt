package l2.app.guide_dota_2.data.model.respond

import androidx.room.ColumnInfo

/**
 * Created by nghia.vuong on 04,March,2020
 */
class HeroDetailFragmentRespond(
    @ColumnInfo(name = "name_dota_1") var name_dota_1: String? = null,
    @ColumnInfo(name = "name_dota_2") var name: String? = null,
    @ColumnInfo(name = "avatar") var avatar: ByteArray? = null,
    @ColumnInfo(name = "en") var role: String? = null,
    @ColumnInfo(name = "sp") var roleSP: String? = null,
    @ColumnInfo(name = "vi") var roleVI: String? = null
)