package l2.app.guide_dota_2.ui

import l2.app.guide_dota_2.ui.base.BasePresenter
import l2.app.guide_dota_2.ui.base.BaseView

/**
 * Created by nghia.vuong on 22,June,2020
 */
interface SplashContract {

    interface Presenter : BasePresenter {
        fun proInitSQLiteFromFirebase() // Init sqlite pro to upload google play.
        fun proCheckNewDataFromFirebase()
        fun proUpdateNewDataFromFirebase() // New Hero, New Item, New Skill.

        fun devPostListIDHeroToFirebase()
        fun devPostListIDItemToFirebase()
        fun devPostListIDSkillToFirebase()

        fun devPostAllHeroTableToFirebase()
        fun devPostAllItemTableToFirebase()
        fun devPostAllSkillTableToFirebase()
    }

    interface View : BaseView<Presenter> {
        fun proUpdateDataFromFirebaseSuccess()
    }
}