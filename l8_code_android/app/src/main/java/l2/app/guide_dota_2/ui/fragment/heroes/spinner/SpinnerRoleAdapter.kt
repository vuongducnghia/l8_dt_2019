package l2.app.guide_dota_2.ui.fragment.heroes.spinner

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.spinner_role_view.view.*
import l2.app.guide_dota_2.R

class SpinnerRoleAdapter(private val ctx: Context, val list: List<SpinnerRoleObj>) :
        ArrayAdapter<SpinnerRoleObj>(ctx, 0, list) {

    private var positionSelected = -1

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.spinner_role_view, parent, false)
        return createView(position, view)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val view = convertView ?: LayoutInflater.from(context).inflate(R.layout.spinner_role_drop_down_view, parent, false)
        return createView(position, view)
    }


    private fun createView(position: Int, view: View): View {
        val spinnerRoleObj = getItem(position)
        view.txtRole.text = spinnerRoleObj!!.description
        if (position == positionSelected) {
            view.txtRole.setTextColor(ContextCompat.getColor(ctx, R.color.text_selected))
        }
        return view
    }

    fun setPositionSelected(position: Int) {
        positionSelected = position
    }

}