package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.room.Embedded
import androidx.room.Relation

data class DemoHeroBase(
    @Embedded
    val a: HeroBaseInfo,
    @Relation(
        parentColumn = "id_hero",
        entityColumn = "id_hero"
    )
    val b: HeroPhotos

)