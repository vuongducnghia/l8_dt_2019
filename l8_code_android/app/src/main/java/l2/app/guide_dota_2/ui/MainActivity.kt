package l2.app.guide_dota_2.ui

import android.os.Bundle
import com.google.android.gms.ads.MobileAds
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.ui.base.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MobileAds.initialize(this) {}
    }
}
