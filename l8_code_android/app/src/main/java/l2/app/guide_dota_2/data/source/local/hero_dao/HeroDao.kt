package l2.app.guide_dota_2.data.source.local.hero_dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import l2.app.guide_dota_2.data.model.table_sqlite_hero.*

@Dao
interface HeroDao {

    /*Insert*/
    @Insert(onConflict = REPLACE)
    fun insertHeroTable(data: Hero)

    @Insert(onConflict = REPLACE)
    fun insertAttributes(data: HeroAttributesTable)

    @Insert(onConflict = REPLACE)
    fun insertBaseInfo(data: HeroBaseInfo)

    @Insert(onConflict = REPLACE)
    fun insertBuildItems(data: HeroBuildItems)

    @Insert(onConflict = REPLACE)
    fun insertCounterMe(data: HeroCounterMe)

    @Insert(onConflict = REPLACE)
    fun insertCounterMeItem(data: HeroCounterMeItem)

    @Insert(onConflict = REPLACE)
    fun insertCounterYou(data: HeroCounterYou)

    @Insert(onConflict = REPLACE)
    fun insertHowPlay(data: HeroHowPlay)

    @Insert(onConflict = REPLACE)
    fun insertPhotos(data: HeroPhotos)

    @Insert(onConflict = REPLACE)
    fun insertRoles(data: HeroRole)

    @Insert(onConflict = REPLACE)
    fun insertTalent(data: HeroTalent)

    @Insert(onConflict = REPLACE)
    fun insertSkill(data: Skill)

    /*Get*/
    @Query("SELECT * FROM HeroAttributesTable WHERE id_hero like :idHero")
    fun getAttributes(idHero: String): HeroAttributesTable?

    @Query("SELECT * FROM HeroBaseInfo WHERE id_hero like :idHero")
    fun getBaseInfo(idHero: String): HeroBaseInfo?

    @Query("SELECT * FROM HeroBuildItems WHERE id_hero like :idHero")
    fun getBuildItems(idHero: String): HeroBuildItems?

    @Query("SELECT * FROM HeroCounterMe WHERE id_hero like :idHero")
    fun getCounterMe(idHero: String): HeroCounterMe?

    @Query("SELECT * FROM HeroCounterMeItem WHERE id_hero like :idHero")
    fun getCounterMeItem(idHero: String): HeroCounterMeItem?

    @Query("SELECT * FROM HeroCounterYou WHERE id_hero like :idHero")
    fun getCounterYou(idHero: String): HeroCounterYou?

    @Query("SELECT * FROM HeroHowPlay WHERE id_hero like :idHero")
    fun getHowPlay(idHero: String): HeroHowPlay?

    @Query("SELECT * FROM HeroPhotos WHERE id_hero like :idHero")
    fun getPhotos(idHero: String): HeroPhotos?

    @Query("SELECT * FROM HeroRole WHERE id_hero like :idHero")
    fun getRoles(idHero: String): HeroRole?

    @Query("SELECT * FROM HeroTalent WHERE id_hero like :idHero")
    fun getTalent(idHero: String): HeroTalent?

    @Query("SELECT * FROM Skill WHERE id_skill like :idSkill")
    fun getSkill(idSkill: String): Skill?

    /*Update*/
    @Query("UPDATE Hero SET id_hero = :data WHERE id_hero = :id")
    fun updateHeroTable(data: String, id: String)

    @Update
    fun updateAttributes(data: HeroAttributesTable)

    @Update
    fun updateBaseInfo(data: HeroBaseInfo)

    @Update
    fun updateBuildItems(data: HeroBuildItems)

    @Update
    fun updateCounterMe(data: HeroCounterMe)

    @Update
    fun updateCounterMeItem(data: HeroCounterMeItem)

    @Update
    fun updateCounterYou(data: HeroCounterYou)

    @Update
    fun updateHowPlay(data: HeroHowPlay)

    @Update
    fun updatePhotos(data: HeroPhotos)

    @Update
    fun updateRoles(data: HeroRole)

    @Update
    fun updateTalent(data: HeroTalent)

    @Update
    fun updateSkill(data: Skill)

    /*Delete*/
    @Delete
    fun deleteAttributes(data: HeroAttributesTable)

    @Delete
    fun deleteBaseInfo(data: HeroBaseInfo)

    @Delete
    fun deleteBuildItems(data: HeroBuildItems)

    @Delete
    fun deleteCounterMe(data: HeroCounterMe)

    @Delete
    fun deleteCounterMeItem(data: HeroCounterMeItem)

    @Delete
    fun deleteCounterYou(data: HeroCounterYou)

    @Delete
    fun deleteHowPlay(data: HeroHowPlay)

    @Delete
    fun deletePhotos(data: HeroPhotos)

    @Delete
    fun deleteRoles(data: HeroRole)

    @Delete
    fun deleteTalent(data: HeroTalent)

    @Delete
    fun deleteSkill(data: Skill)


}