package l2.app.guide_dota_2.data.source.local

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteException
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.table_sqlite_hero.*
import l2.app.guide_dota_2.data.model.table_sqlite_item.*
import l2.app.guide_dota_2.data.model.table_sqlite_other.NewData
import l2.app.guide_dota_2.data.source.local.hero_dao.HeroDao
import l2.app.guide_dota_2.data.source.local.hero_dao.HeroJoinDao
import l2.app.guide_dota_2.data.source.local.hero_dao.HeroTableDao
import l2.app.guide_dota_2.data.source.local.item_dao.ItemDao
import l2.app.guide_dota_2.data.source.local.item_dao.ItemJoinDao
import l2.app.guide_dota_2.data.source.local.item_dao.ItemTableDao
import l2.app.guide_dota_2.data.source.local.other_table.OtherTableDao
import l2.app.guide_dota_2.data.source.local.skill_dao.SkillDao
import l2.app.guide_dota_2.data.source.local.skill_dao.SkillJoinDao
import l2.app.guide_dota_2.data.source.local.skill_dao.SkillTableDao
import l2.app.guide_dota_2.util.AppLog
import l2.app.guide_dota_2.util.Constant
import l2.app.guide_dota_2.util.Constant.DATA_BASE_ENCODE_NAME
import l2.app.guide_dota_2.util.Constant.DATA_BASE_NAME

/*
* 0. Add a new hero.
* 1. Delete a hero.
* 2. Update a hero.
* 3. Update id group of a hero.
* - At screen splash:
* -- Get list id hero in heroTable firebase compare heroTable sqlite
* -- Download new hero and insert to sqlite
* --> Multiple heroes.
* 0. Add heroes.
* 1. Delete heroes.
* 2. Update heroes.
* hero version at firebase have new version.
* - At screen splash:
* -- Remove all hero table.
* -- Follow hero id table, get and insert hero to sqlite.
* */
const val VERSION_DATABASE = 1
const val VERSION_DATABASE_KEY = "VERSION_DATABASE"

@Database(
    entities = [
        Hero::class,
        HeroAttributesTable::class,
        HeroBaseInfo::class,
        HeroBuildItems::class,
        HeroCounterMe::class,
        HeroCounterMeItem::class,
        HeroCounterYou::class,
        HeroHowPlay::class,
        HeroPhotos::class,
        HeroRole::class,
        HeroTalent::class,

        Item::class,
        ItemBaseInfo::class,
        ItemDescription::class,
        ItemNote::class,
        ItemPhoto::class,
        ItemProperty::class,
        ItemUp::class,

        NewData::class,

        Skill::class,
        SkillBaseInfo::class,
        SkillDescription::class,
        SkillMoreInfo::class,
        SkillPhoto::class,
        SkillPlay::class
    ], version = VERSION_DATABASE
)
abstract class L8Database : RoomDatabase() {

    abstract fun heroQuery(): HeroDao
    abstract fun heroTableQuery(): HeroTableDao
    abstract fun heroJoinQuery(): HeroJoinDao

    abstract fun itemQuery(): ItemDao
    abstract fun itemTableQuery(): ItemTableDao
    abstract fun itemJoinQuery(): ItemJoinDao

    abstract fun skillQuery(): SkillDao
    abstract fun skillTableQuery(): SkillTableDao
    abstract fun skillJoinQuery(): SkillJoinDao

    abstract fun otherTableQuery(): OtherTableDao

    private val TAG = "L8Database"

    companion object {
        private val TAG = L8Database::class.java.simpleName
        private var INSTANCE: L8Database? = null
        private val lock = Any()

        fun getInstance(context: Context): L8Database {
            synchronized(lock) {

                /*if (L8Application.instance.sharedHelper.getInt(VERSION_DATABASE_KEY, -1) != VERSION_DATABASE) {
                    // Delete old database
                    val dbName = getDataBaseName(context)
                    val dbPath: String = context.getDatabasePath(dbName).path
                    if (checkDataBase(dbPath)) {
                        context.deleteDatabase(dbName)
                        L8Application.instance.sharedHelper.setInt(VERSION_DATABASE_KEY, VERSION_DATABASE)
                    }
                }*/

                if (INSTANCE == null) {
                    // Build room SQLite
                    INSTANCE = if (L8Application.instance.activeBuilder == Constant.DEV) {
                        Room.databaseBuilder(context.applicationContext, L8Database::class.java, DATA_BASE_NAME)
                            .addMigrations(MIGRATION_UPDATE_DATA)
                            .createFromAsset(DATA_BASE_NAME)
                            .build()
                    } else {
                        Room.databaseBuilder(context.applicationContext, L8Database::class.java, DATA_BASE_ENCODE_NAME)
                            .addMigrations(MIGRATION_UPDATE_DATA)
                            .createFromAsset(DATA_BASE_ENCODE_NAME)
                            .build()
                    }
                }
                return INSTANCE!!
            }
        }

        /*Bắt buộc phải có khi update data*/
        @JvmField
        val MIGRATION_UPDATE_DATA: Migration = object : Migration(1, VERSION_DATABASE) {
            override fun migrate(database: SupportSQLiteDatabase) {}
        }

        @JvmField
        val MIGRATION_UPDATE_TABLE: Migration = object : Migration(1, VERSION_DATABASE) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // 1. Create the new table.
                // 2.Copy the data.
                // 3.Remove the old table.
                // 4.Change the table name to the correct one
            }
        }


        private fun checkDataBase(DB_PATH: String): Boolean {
            AppLog.d(TAG, "db path $DB_PATH")

            val checkDB: SQLiteDatabase? = try {
                SQLiteDatabase.openDatabase(DB_PATH, null, SQLiteDatabase.OPEN_READONLY)
            } catch (e: SQLiteException) {   //database does't exist yet.
                return false
            }

            return if (checkDB != null) {
                checkDB.close()
                true
            } else {
                false
            }
        }
    }
}


