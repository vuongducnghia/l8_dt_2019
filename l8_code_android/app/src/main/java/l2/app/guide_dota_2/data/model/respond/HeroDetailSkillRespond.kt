package l2.app.guide_dota_2.data.model.respond

import androidx.room.ColumnInfo

/**
 * Created by nghia.vuong on 04,March,2020
 */
class HeroDetailSkillRespond() {
    @ColumnInfo(name = "id_skill")
    var idSkill: String? = null
    @ColumnInfo(name = "dota1")
    var photoDota: ByteArray? = null
    @ColumnInfo(name = "dota2")
    var photoDota2: ByteArray? = null
}