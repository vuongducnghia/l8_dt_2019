package l2.app.guide_dota_2.data.source.local.item

import l2.app.guide_dota_2.data.source.local.skill.SkillRepositoryContract


interface ItemRepositoryContract {

    interface BaseResult {
        fun noData(){}
    }

    fun getItemTable(callBack: ResultItemTable)
    interface ResultItemTable : BaseResult {
        fun result(data: List<String>)
    }

    fun getItemBase(callBack: ResultItemBase)
    interface ResultItemBase : BaseResult {
        fun result(data:GroupItemObject)
    }

    fun getItemUpgrades(callBack: ResultItemUpgrades)
    interface ResultItemUpgrades : BaseResult {
        fun result(data: GroupItemObject)
    }

    fun getItemDetail(idItem: String, callBack: ResultItemDetail)
    interface ResultItemDetail : BaseResult {
        fun result(data:ItemDetail)
    }
}
