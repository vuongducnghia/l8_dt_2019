package l2.app.guide_dota_2.ui

import android.app.Activity
import android.content.Context
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.table_sqlite_other.NewData
import l2.app.guide_dota_2.data.source.firebase.OtherFirebaseContract
import l2.app.guide_dota_2.data.source.firebase.OtherFirebaseHelper
import l2.app.guide_dota_2.data.source.firebase.hero.HeroFirebaseContract
import l2.app.guide_dota_2.data.source.firebase.hero.HeroFirebaseHelper
import l2.app.guide_dota_2.data.source.firebase.item.ItemFirebaseContract
import l2.app.guide_dota_2.data.source.firebase.item.ItemFirebaseHelper
import l2.app.guide_dota_2.data.source.firebase.skill.SkillFirebaseContract
import l2.app.guide_dota_2.data.source.firebase.skill.SkillFirebaseHelper
import l2.app.guide_dota_2.data.source.local.hero.HeroRepository
import l2.app.guide_dota_2.data.source.local.hero.HeroRepositoryContract
import l2.app.guide_dota_2.data.source.local.item.ItemRepository
import l2.app.guide_dota_2.data.source.local.item.ItemRepositoryContract
import l2.app.guide_dota_2.data.source.local.other_table.OtherRepository
import l2.app.guide_dota_2.data.source.local.skill.SkillRepository
import l2.app.guide_dota_2.data.source.local.skill.SkillRepositoryContract
import l2.app.guide_dota_2.util.Constant
import l2.app.guide_dota_2.util.EventBusAction
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

/**
 * Created by nghia.vuong on 22,June,2020
 */
const val UPDATE_BY_FB_SUCCESS_REQUEST_CODE = 101
const val UPDATE_BY_FB_TRACKING_REQUEST_CODE = 102

class SplashPresenter(val context: Context?, val view: SplashContract.View) : SplashContract.Presenter {

    private var heroFirebase: HeroFirebaseHelper
    private var itemFirebase: ItemFirebaseHelper
    private var skillFirebase: SkillFirebaseHelper
    private var otherFirebase: OtherFirebaseHelper

    private var heroRepository: HeroRepository
    private var itemRepository: ItemRepository
    private var skillRepository: SkillRepository
    private var dbOtherTable: OtherRepository


    init {
        view.presenter = this
        heroFirebase = HeroFirebaseHelper()
        itemFirebase = ItemFirebaseHelper()
        skillFirebase = SkillFirebaseHelper()
        otherFirebase = OtherFirebaseHelper()
        dbOtherTable = OtherRepository()

        heroRepository = HeroRepository()
        skillRepository = SkillRepository()
        itemRepository = ItemRepository()
    }

    override fun start() {
        view.isActive = true
        EventBus.getDefault().register(this)
    }

    override fun end() {
        view.isActive = false
        EventBus.getDefault().unregister(this)
    }


    override fun proInitSQLiteFromFirebase() {
        if (context != null && view.isActive) {
            heroFirebase.createHeroTable()
            itemFirebase.createItemTable()
            skillFirebase.createSkillTable()
            otherFirebase.createNewData()
        }
    }

    override fun devPostAllHeroTableToFirebase() {
        if (context != null && view.isActive) heroFirebase.postAll()
    }

    override fun devPostListIDHeroToFirebase() {
        if (context != null && view.isActive) heroFirebase.postListIDHero()
    }

    override fun devPostListIDItemToFirebase() {
        if (context != null && view.isActive) itemFirebase.postListIDItem()
    }

    override fun devPostAllItemTableToFirebase() {
        if (context != null && view.isActive) itemFirebase.postAll()
    }

    override fun devPostListIDSkillToFirebase() {
        if (context != null && view.isActive) skillFirebase.postListIDSkill()
    }

    override fun devPostAllSkillTableToFirebase() {
        if (context != null && view.isActive) skillFirebase.postAll()
    }

    override fun proCheckNewDataFromFirebase() {
        if (context != null && view.isActive) {
            otherFirebase.getNewData(object : OtherFirebaseContract.ResultData {
                override fun result(data: List<NewData>) {
                    proUpdateNewDataFromFirebase()
                }
            })
        }
    }

    /***
     * 1. Get list id hero Firebase
     * 2. Get list id item Sqlite
     * 3. Compare
     * 4. Insert new data firebase to sqlite by id
     */
    override fun proUpdateNewDataFromFirebase() {
        if (context != null && view.isActive) {
            // Update từ nhỏ đến lớn. Trong updateNewSkill đã bao gồm updateNewItem, updateNewHero
            updateNewSkill()
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onEvent(event: EventBusAction.RequestResult) {
        if (event.resultCode == Activity.RESULT_OK) {
            if (event.requestCode == UPDATE_BY_FB_SUCCESS_REQUEST_CODE) {
                if (L8Application.instance.sharedHelper.getBoolean(Constant.FIRE_BASE_CREATE_NEW_HERO, false) &&
                    L8Application.instance.sharedHelper.getBoolean(Constant.FIRE_BASE_CREATE_NEW_ITEM, false) &&
                    L8Application.instance.sharedHelper.getBoolean(Constant.FIRE_BASE_CREATE_NEW_SKILL, false)
                ) {
                    view.proUpdateDataFromFirebaseSuccess()
                }
            }
        }
    }


    private fun updateNewHero() {
        val IDHero_Firebase = ArrayList<String>()
        val IDHero_SQLite = ArrayList<String>()
        val listIdHeroNew = ArrayList<String>()

        // Get list id hero firebase
        heroFirebase.getListIDHero(object : HeroFirebaseContract.ResultListID {
            override fun result(data: List<String>) {
                IDHero_Firebase.addAll(data)

                println("SplashPresenter.result updateNewHero listIdHeroFirebase ${IDHero_Firebase.size}")
                if (IDHero_Firebase.size > 0) {
                    // Get list id hero sqlite encode
                    heroRepository.getHeroTable(object : HeroRepositoryContract.ResultHeroTable {
                        override fun result(data: List<String>) {
                            IDHero_SQLite.addAll(data)
                            println("SplashPresenter.result updateNewHero listIdHeroSqliteEncode ${IDHero_SQLite.size}")
                            if (IDHero_Firebase.size != IDHero_SQLite.size) {
                                // Check new hero
                                for (i in 0 until IDHero_Firebase.size) {
                                    var isContain = false
                                    for (j in 0 until IDHero_SQLite.size) {
                                        if (IDHero_SQLite[j].contains(IDHero_Firebase[i])) {
                                            isContain = true
                                            break
                                        }
                                    }
                                    if (!isContain) {
                                        listIdHeroNew.add(IDHero_Firebase[i])
                                    }
                                }
                            }


                            listIdHeroNew.forEach {
                                println("SplashPresenter.result updateNewHero listIdHeroNew: ${it}")
                                heroFirebase.insertByFirebase(it)
                            }

                            // Update UI
                            view.proUpdateDataFromFirebaseSuccess()
                        }
                    })
                }
            }
        })
    }

    private fun updateNewItem() {
        val listIdItemFirebase = ArrayList<String>()
        val listIdItemSqliteEncode = ArrayList<String>()
        val listIdItemNew = ArrayList<String>()

        // Get list id Item firebase
        itemFirebase.getListIDItem(object : ItemFirebaseContract.ResultListIDItem {
            override fun result(data: List<String>) {
                listIdItemFirebase.addAll(data)
                println("SplashPresenter.result updateNewItem listIdItemFirebase ${listIdItemFirebase.size}")
                if (listIdItemFirebase.size > 0) {
                    // Get list id Item sqlite encode
                    itemRepository.getItemTable(object : ItemRepositoryContract.ResultItemTable {
                        override fun result(data: List<String>) {
                            listIdItemSqliteEncode.addAll(data)
                            println("SplashPresenter.result updateNewItem listIdItemSqliteEncode ${listIdItemSqliteEncode.size}")
                            if (listIdItemSqliteEncode.size > 0) {
                                if (listIdItemFirebase.size != listIdItemSqliteEncode.size) {
                                    // Check new Item
                                    for (i in 0 until listIdItemFirebase.size) {
                                        var isContain = false
                                        for (j in 0 until listIdItemSqliteEncode.size) {
                                            if (listIdItemSqliteEncode[j].contains(listIdItemFirebase[i])) {
                                                isContain = true
                                                break
                                            }
                                        }
                                        if (!isContain) {
                                            listIdItemNew.add(listIdItemFirebase[i])
                                        }
                                    }
                                }
                            }

                            listIdItemNew.forEach {
                                println("SplashPresenter.result updateNewItem listIdItemNew: ${it}")
                                itemFirebase.insertByFirebase(it)
                            }

                            // Insert new
                            updateNewHero()
                        }
                    })
                }
            }
        })
    }

    private fun updateNewSkill() {
        val listIdSkillFirebase = ArrayList<String>()
        val listIdSkillSqliteEncode = ArrayList<String>()
        val listIdSkillNew = ArrayList<String>()

        // Get list id Skill firebase
        skillFirebase.getListIDSkill(object : SkillFirebaseContract.ResultListIDSkill {
            override fun result(data: List<String>) {
                listIdSkillFirebase.addAll(data)
                println("SplashPresenter.result updateNewSkill listIdSkillFirebase ${listIdSkillFirebase.size}")
                if (listIdSkillFirebase.size > 0) {
                    // Get list id Skill sqlite encode
                    skillRepository.getSkillTable(object : SkillRepositoryContract.ResultSkillTable {
                        override fun result(data: List<String>) {
                            listIdSkillSqliteEncode.addAll(data)
                            println("SplashPresenter.result updateNewSkill listIdSkillSqliteEncode ${listIdSkillSqliteEncode.size}")
                            if (listIdSkillSqliteEncode.size > 0) {
                                if (listIdSkillFirebase.size != listIdSkillSqliteEncode.size) {
                                    // Check new Skill
                                    for (i in 0 until listIdSkillFirebase.size) {
                                        var isContain = false
                                        for (j in 0 until listIdSkillSqliteEncode.size) {
                                            if (listIdSkillSqliteEncode[j].contains(listIdSkillFirebase[i])) {
                                                isContain = true
                                                break
                                            }
                                        }
                                        if (!isContain) {
                                            listIdSkillNew.add(listIdSkillFirebase[i])
                                        }
                                    }
                                }
                            }

                            listIdSkillNew.forEach {
                                println("SplashPresenter.result updateNewSkill listIdSkillNew: ${it}")
                                skillFirebase.insertByFirebase(it)
                            }

                            // Insert new
                            updateNewItem()
                        }
                    })
                }
            }
        })
    }

}