package l2.app.guide_dota_2.ui.fragment.heroes.recyclerview_heroes

import android.content.Context
import android.graphics.PointF
import android.util.DisplayMetrics
import android.view.View
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView

class MySmoothScroller(context: Context, vectorY: Float) : LinearSmoothScroller(context) {

    private val MILLISECONDS_PER_INCH = 500f
    private val vectorY = vectorY

    override fun computeScrollVectorForPosition(targetPosition: Int): PointF? {
        println("HeroesFragment.0 computeScrollVectorForPosition") // Len hay xuong
        return PointF(0f, vectorY)
    }

    override fun calculateTimeForScrolling(dx: Int): Int {
        val value = super.calculateTimeForScrolling(dx)
        println("HeroesFragment.1 calculateTimeForScrolling $value")
        return value
    }

    //This returns the milliseconds it takes to scroll one pixel.
    override fun calculateSpeedPerPixel(displayMetrics: DisplayMetrics): Float {
        println("HeroesFragment.2 calculateSpeedPerPixel")
        return MILLISECONDS_PER_INCH / displayMetrics.densityDpi
    }

    override fun calculateDxToMakeVisible(view: View?, snapPreference: Int): Int {
        println("HeroesFragment.3 calculateDxToMakeVisible")
        return super.calculateDxToMakeVisible(view, snapPreference)
    }

    override fun getVerticalSnapPreference(): Int {
        println("HeroesFragment.4 getVerticalSnapPreference")
        return super.getVerticalSnapPreference()
    }

    override fun calculateDyToMakeVisible(view: View?, snapPreference: Int): Int {
        println("HeroesFragment.5 calculateDyToMakeVisible")
        return super.calculateDyToMakeVisible(view, snapPreference)
    }

    override fun calculateDtToFit(
        viewStart: Int, viewEnd: Int, boxStart: Int, boxEnd: Int, snapPreference: Int
    ): Int {
        println("HeroesFragment.6 calculateDtToFit")
        return super.calculateDtToFit(viewStart, viewEnd, boxStart, boxEnd, snapPreference)
    }

    override fun calculateTimeForDeceleration(dx: Int): Int {
        val value = super.calculateTimeForDeceleration(dx)
        println("HeroesFragment.1 calculateTimeForDeceleration $value")
        return value

    }

    // computeScrollVectorForPosition

    override fun onSeekTargetStep(
        dx: Int, dy: Int, state: RecyclerView.State, action: Action
    ) {
        println("MySmoothScroller.onSeekTargetStep")
        super.onSeekTargetStep(dx, dy, state, action)
    }

    override fun onStart() {
        super.onStart()
        println("HeroesFragment.onStart")
    }

    override fun onStop() {
        super.onStop()
        println("HeroesFragment.onStop")
    }

    override fun onTargetFound(
        targetView: View, state: RecyclerView.State, action: Action
    ) {
        super.onTargetFound(targetView, state, action)
        println("HeroesFragment.onTargetFound")
    }

    override fun updateActionForInterimTarget(action: Action?) {
        super.updateActionForInterimTarget(action)
        println("HeroesFragment.updateActionForInterimTarget")
    }

}