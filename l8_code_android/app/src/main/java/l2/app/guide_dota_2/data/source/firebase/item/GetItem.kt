package l2.app.guide_dota_2.data.source.firebase.item

import SecurityData
import android.app.Activity
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.gson.Gson
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.table_sqlite_item.*
import l2.app.guide_dota_2.ui.UPDATE_BY_FB_SUCCESS_REQUEST_CODE
import l2.app.guide_dota_2.util.*
import l2.app.guide_dota_2.util.ConstantTable.Item_Base_Info_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Description_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Note_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Photo_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Property_Table
import l2.app.guide_dota_2.util.ConstantTable.Item_Up_Table
import org.greenrobot.eventbus.EventBus

/**
 * Created by nghia.vuong on 25,June,2020
 */
class GetItem {

    private val TAG = "GetItem"

    private var appExecutors: AppExecutors = AppExecutors()
    private var firebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
    private val gson = Gson()

    private fun <T> getDataFromFirebase(id: String, table: String, classOfT: Class<T>, callback: ResultCallBack) {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()

            firebaseDatabase.getReference(table).child(id).addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    AppLog.w(TAG, "$table Failed to read value.", error.toException())
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    try {
                        val value = snapshot.value as String
                        if (value.isNotEmpty()) {
                            val decryption = securityData.decrypt(value, securityData.stringFromJNI())
                            val hero = gson.fromJson(decryption, classOfT)
                            appExecutors.diskIO.execute {
                                callback.send2SQLite(hero)
                            }
                        }
                    } catch (error: Exception) {
                        error.printStackTrace()
                        AppLog.e(TAG, "$table can not update new data error: ${error.message}")
                    }

                }
            })
        }
    }

    interface ResultCallBack {
        fun send2SQLite(t: Any?)
    }

    fun getListIDItem(callBack: ItemFirebaseContract.ResultListIDItem) {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()
            val myRef = firebaseDatabase.getReference(ConstantTable.Item_Table)
            myRef.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    AppLog.w(TAG, "Failed to read value.", error.toException())

                    appExecutors.mainThread.execute {
                        callBack.noData("Failed to read value. ${error.toException()}")
                    }
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val value = dataSnapshot.value as String
                    val decryption = securityData.decrypt(value, securityData.stringFromJNI())
                    val listItemIdGSon = Gson().fromJson(decryption, Array<String>::class.java)
                    AppLog.w(TAG, "Get Item Table value. ${listItemIdGSon[0]} , ${listItemIdGSon[1]}")
                    appExecutors.mainThread.execute {
                        callBack.result(listItemIdGSon.toList())
                    }
                }
            })
        }
    }

    /*
        * 1. Get list id first
        * 2. Get item by id
        * */
    fun getAll() {
        getListIDItem(object : ItemFirebaseContract.ResultListIDItem {
            override fun result(data: List<String>) {
                if (data.size > 0) {
                    data.forEachIndexed { index, s ->
                        AppLog.w(TAG, "index $index Insert 2 SQLite item. $s")
                        insertByFirebase(s)
                    }
                }
            }
        })
    }

    private var isUpdateData = false

    fun insertByFirebase(id: String) {
        isUpdateData = false
        insertItem(id)
        getBase(id)
    }

    // Get data at firebase and save to sqlite
    fun updateByFirebase(id: String) {
        isUpdateData = true
        getBase(id)
    }

    private fun getBase(id: String) {
        getBaseInfo(id)
        getDescription(id)
        getNote(id)
        getPhoto(id)
        getProperty(id)
        getUp(id)
    }

    private fun insertItem(id: String) {
        appExecutors.diskIO.execute {
            val item = Item()
            item.idItem = id
            L8Application.instance.db.itemQuery().insertItemTable(item)
        }
    }

    private fun getBaseInfo(id: String) {
        getDataFromFirebase(id, Item_Base_Info_Table, ItemBaseInfo::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.itemQuery().updateBaseInfo(t as ItemBaseInfo)
                    } else {
                        L8Application.instance.db.itemQuery().insertBaseInfo(t as ItemBaseInfo)
                    }
                }
            }
        })
    }

    private fun getDescription(id: String) {
        getDataFromFirebase(id, Item_Description_Table, ItemDescription::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.itemQuery().updateDescription(t as ItemDescription)
                    } else {
                        L8Application.instance.db.itemQuery().insertDescription(t as ItemDescription)
                    }
                }
            }
        })
    }

    private fun getNote(id: String) {
        getDataFromFirebase(id, Item_Note_Table, ItemNote::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.itemQuery().updateNote(t as ItemNote)
                    } else {
                        L8Application.instance.db.itemQuery().insertNote(t as ItemNote)
                    }
                }
            }
        })
    }

    private fun getPhoto(id: String) {
        getDataFromFirebase(id, Item_Photo_Table, ItemPhoto::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.itemQuery().updatePhoto(t as ItemPhoto)
                    } else {
                        L8Application.instance.db.itemQuery().insertPhoto(t as ItemPhoto)
                    }
                }
            }
        })
    }

    private fun getProperty(id: String) {
        getDataFromFirebase(id, Item_Property_Table, ItemProperty::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.itemQuery().updateProperty(t as ItemProperty)
                    } else {
                        L8Application.instance.db.itemQuery().insertProperty(t as ItemProperty)
                    }
                }
            }
        })
    }

    private fun getUp(id: String) {
        getDataFromFirebase(id, Item_Up_Table, ItemUp::class.java, object : ResultCallBack {
            override fun send2SQLite(t: Any?) {
                if (t != null) {
                    if (isUpdateData) {
                        L8Application.instance.db.itemQuery().updateUp(t as ItemUp)
                    } else {
                        L8Application.instance.db.itemQuery().insertUp(t as ItemUp)
                    }

                    if (id.contains("id_yasha")) {
                        appExecutors.mainThread.execute {
                            L8Application.instance.sharedHelper.setBoolean(Constant.FIRE_BASE_CREATE_NEW_ITEM, true)
                            EventBus.getDefault().post(EventBusAction.RequestResult(UPDATE_BY_FB_SUCCESS_REQUEST_CODE, Activity.RESULT_OK, true))
                        }
                    }
                }
            }
        })
    }

}