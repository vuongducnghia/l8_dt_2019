package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SkillBaseInfo")
class SkillBaseInfo {
    @PrimaryKey
    @ColumnInfo(name = "id_skill")
    @NonNull
    var idSkill: String? = null

    @ColumnInfo(name = "id_hero")
    var idHero: String? = null

    @ColumnInfo(name = "name_skill")
    var name: String? = null

    @ColumnInfo(name = "note")
    var note: String? = null
}