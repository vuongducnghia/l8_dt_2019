package l2.app.guide_dota_2.ui.fragment.detail_hero.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager.HeroDetailAttributes
import l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager.HeroDetailBuildItem
import l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager.HeroDetailCounter
import l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager.HeroDetailSkill

class HeroDetailViewPagerAdapter(private val idHeroDetail:String, private val titles: Array<String>, fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(p0: Int): Fragment {
        return when (p0) {
            0 -> HeroDetailAttributes()
            1 -> HeroDetailSkill()
            2 -> HeroDetailCounter()
            else -> HeroDetailBuildItem()
        }
    }

    override fun getCount(): Int {
        return titles.size
    }

}