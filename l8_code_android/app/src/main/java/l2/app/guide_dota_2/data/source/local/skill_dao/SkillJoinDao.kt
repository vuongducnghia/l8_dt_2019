package l2.app.guide_dota_2.data.source.local.skill_dao

import androidx.room.Dao
import androidx.room.Query
import l2.app.guide_dota_2.data.model.respond.HeroDetailSkillRespond

@Dao
interface SkillJoinDao {

    @Query("SELECT SkillBaseInfo.id_skill , SkillPhoto.dota1, SkillPhoto.dota2 FROM SkillBaseInfo INNER JOIN SkillPhoto ON SkillBaseInfo.id_skill = SkillPhoto.id_skill WHERE id_hero like :idHero")
    fun getSkillOfHero(idHero: String): List<HeroDetailSkillRespond>

}