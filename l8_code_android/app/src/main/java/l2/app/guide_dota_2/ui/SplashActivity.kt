package l2.app.guide_dota_2.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import kotlinx.android.synthetic.main.activity_splash.*
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.ui.base.BaseActivity
import l2.app.guide_dota_2.util.Constant.DEV
import l2.app.guide_dota_2.util.Constant.PRO
import l2.app.guide_dota_2.util.NetworkUtils.isNetworkConnected


/*
* dev:
* - Web -> Browser sqlite -> db normal -> post one when test.
* - Post one or button post all to firebase.
* - Close aaptOptions ignoreAssetsPattern "!database_encode" and "!database_normal"
* pro:
* - Check table NewData to update multiple item
* - Open item to update item
* - Open aaptOptions ignoreAssetsPattern "!database_normal"
* */

class SplashActivity : BaseActivity(), SplashContract.View {

    private val TAG = "SplashActivity"

    override lateinit var presenter: SplashContract.Presenter
    override var isActive: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Init presenter
        presenter = SplashPresenter(this, this)
        presenter.start()

        // Init View
        when (L8Application.instance.activeBuilder) {
            DEV -> {
                setContentView(R.layout.activity_splash)
                initViewDEV()
            }
            PRO -> {
                setContentView(R.layout.activity_splash_pro)

                // 1. Check network and update data base.
                if (isNetworkConnected(this)) {
                    // Check isFirst app
//                    presenter.proCheckNewDataFromFirebase()
//                    progressBarMessage.text = "Checking new data..."

                    // Test
                    presenter.proInitSQLiteFromFirebase()
                } else {
                    goMainActivity()
                }
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.end()
    }

    private fun initViewDEV() {
        gDEV.visibility = View.VISIBLE
        progressBar_cyclic.visibility = View.INVISIBLE
        progressBarMessage.visibility = View.INVISIBLE

        // Post all
        btnPostAll.setOnClickListener {
            presenter.devPostListIDHeroToFirebase()
            presenter.devPostListIDItemToFirebase()
            presenter.devPostListIDSkillToFirebase()
            presenter.devPostAllHeroTableToFirebase()
            presenter.devPostAllItemTableToFirebase()
            presenter.devPostAllSkillTableToFirebase()
        }

        // Post one
        btnGoMainActivity.setOnClickListener {
            goMainActivity()
        }
    }

    override fun proUpdateDataFromFirebaseSuccess() {
        progressBarMessage.text = "Download finish."
        progressBarMessage.invalidate()
        // goMainActivity()
    }

    private fun goMainActivity() {
          Handler().postDelayed({
              startActivity(Intent(this, MainActivity::class.java))
              overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
              finish()
          }, 2000)
    }
}
