package l2.app.guide_dota_2.ui.customs

import android.content.Context
import android.util.AttributeSet
import android.widget.GridView
import l2.app.guide_dota_2.R


class CustomGridView : GridView {

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    init {
        autoNumColumns()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val heightSpec: Int = if (layoutParams.height == LayoutParams.WRAP_CONTENT) {
            // The two leftmost bits in the height measure spec have
            // a special meaning, hence we can't use them to describe height.
            MeasureSpec.makeMeasureSpec(
                Int.MAX_VALUE shr 2, MeasureSpec.AT_MOST
            )
        } else {
            // Any other height should be respected as is.
            heightMeasureSpec
        }
        super.onMeasure(widthMeasureSpec, heightSpec)


    }

    private fun autoNumColumns() {
        this.post {
            calculatorNumColumns()
        }
    }

    fun resetNumColumns() {
        calculatorNumColumns()
    }

    private fun calculatorNumColumns() {
        val sizeImageItem = resources.getDimension(R.dimen.size_image_item)
        val sizeImageItemSpace = resources.getDimension(R.dimen.size_image_item_space)
        val numColumns = (width / (sizeImageItem + sizeImageItemSpace))
        this.numColumns = numColumns.toInt()

    }

}