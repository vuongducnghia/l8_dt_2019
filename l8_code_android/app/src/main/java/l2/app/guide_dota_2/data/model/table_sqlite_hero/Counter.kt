package l2.app.guide_dota_2.data.model.table_sqlite_hero

/**
 * Created by nghia.vuong on 02,March,2020
 */
class Counter {
    var idHero: String? = null
    var nameDota1: String? = null
    var nameDota2: String? = null
    var imageDota1: String? = null
    var imageDota2: String? = null
    var guide: String? = null
    var guideSP: String? = null
    var guideVI: String? = null
}