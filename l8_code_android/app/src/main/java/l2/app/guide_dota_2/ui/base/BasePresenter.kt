package l2.app.guide_dota_2.ui.base

interface BasePresenter {
    /*var compositeDisposable = CompositeDisposable() // io.reactivex.disposables

    fun addDisposable(d: Disposable?) {
        if (d != null) {
            compositeDisposable.add(d)
        }
    }

    fun dispose() {
        compositeDisposable.dispose()
    }*/

    fun start()
    fun end()
}