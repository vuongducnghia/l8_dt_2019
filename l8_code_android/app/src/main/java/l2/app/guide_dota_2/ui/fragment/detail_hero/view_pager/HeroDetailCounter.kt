package l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_hero_detail_counter.*
import kotlinx.android.synthetic.main.fragment_hero_detail_counter_guide.*
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.respond.HeroDetailCounterRespond
import l2.app.guide_dota_2.data.source.local.hero.HeroRepository
import l2.app.guide_dota_2.data.source.local.hero.HeroRepositoryContract
import l2.app.guide_dota_2.ui.base.BaseFragment
import l2.app.guide_dota_2.ui.fragment.detail_hero.HeroDetailFragment
import l2.app.guide_dota_2.ui.fragment.detail_hero.adapters.CounterViewAdapter

class HeroDetailCounter : BaseFragment() {

    private lateinit var parent: HeroDetailFragment
    private lateinit var dbHeroGetRepository: HeroRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parent = (parentFragment as HeroDetailFragment)
        dbHeroGetRepository = HeroRepository()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_hero_detail_counter, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        close_guide.setOnClickListener { show_guide.visibility = View.GONE }

        parent.idHeroDetail?.let {
            dbHeroGetRepository.getHeroCounterMe(parent.idHeroDetail!!, object : HeroRepositoryContract.ResultHeroCounterMe {
                override fun result(data: List<HeroDetailCounterRespond>) {
                    println("HeroDetailCounter.data ${data.size}")
                    if (data.size > 0) {
                        val adapter = CounterViewAdapter(data)
                        gvCounterMe.adapter = adapter
                        gvCounterMe.resetNumColumns()
                        adapter.setOnClickListener(object : CounterViewAdapter.OnClickListener {
                            override fun onItemClick(position: Int) {
                                println("HeroDetailCounter.onItemClick ${data[position].guide}")
                                show_guide.visibility = View.VISIBLE
                                tvGuide.text = data[position].guide
                            }
                        })
                    }
                }

                override fun noData() {}

            })

            dbHeroGetRepository.getHeroCounterYou(parent.idHeroDetail!!, object : HeroRepositoryContract.ResultHeroCounterYou {
                override fun result(data: List<HeroDetailCounterRespond>) {
                    if (data.size > 0) {
                        val adapter = CounterViewAdapter(data)
                        gvCounterYou.adapter = adapter
                        gvCounterYou.resetNumColumns()
                        adapter.setOnClickListener(object : CounterViewAdapter.OnClickListener {
                            override fun onItemClick(position: Int) {
                                println("HeroDetailCounter.onItemClick ${data[position].guide}")
                                show_guide.visibility = View.VISIBLE
                                tvGuide.text = data[position].guide
                            }
                        })
                    }
                }

                override fun noData() {}

            })

        }
    }
}



