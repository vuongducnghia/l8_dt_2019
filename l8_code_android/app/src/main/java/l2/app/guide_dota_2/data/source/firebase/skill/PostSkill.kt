package l2.app.guide_dota_2.data.source.firebase.skill

import SecurityData
import com.google.firebase.database.FirebaseDatabase
import com.google.gson.Gson
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.util.AppExecutors
import l2.app.guide_dota_2.util.ConstantTable.Skill_BaseInfo_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_Description_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_MoreInfo_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_Photo_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_Play_Table
import l2.app.guide_dota_2.util.ConstantTable.Skill_Table

/**
 * Created by nghia.vuong on 23,June,2020
 */
class PostSkill {

    private val TAG = "PostSkill"

    private var appExecutors: AppExecutors = AppExecutors()
    private var firebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()
    private val gson = Gson()

    fun postListIDSkill() {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()
            firebaseDatabase.getReference(Skill_Table).setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.skillTableQuery().getSkillTable()), securityData.stringFromJNI()))
        }
    }

    fun postSkill(id: String) {
        appExecutors.networkIO.execute {
            val securityData = SecurityData()

            // postSkillBaseInfo
            firebaseDatabase.getReference(Skill_BaseInfo_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.skillQuery().getBaseInfo(id)), securityData.stringFromJNI()))
            // postSkillDescription
            firebaseDatabase.getReference(Skill_Description_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.skillQuery().getDescription(id)), securityData.stringFromJNI()))
            // postSkillMoreInfo
            firebaseDatabase.getReference(Skill_MoreInfo_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.skillQuery().getMoreInfo(id)), securityData.stringFromJNI()))
            // postSkillPhoto
            firebaseDatabase.getReference(Skill_Photo_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.skillQuery().getPhoto(id)), securityData.stringFromJNI()))
            // postSkillPlay
            firebaseDatabase.getReference(Skill_Play_Table).child(id)
                .setValue(securityData.encrypt(Gson().toJson(L8Application.instance.db.skillQuery().getPlay(id)), securityData.stringFromJNI()))

        }
    }

    fun postAll() {
        appExecutors.diskIO.execute {
            val securityData = SecurityData()

            // postSkillBaseInfo
            L8Application.instance.db.skillTableQuery().getBaseInfoAll().forEach {
                firebaseDatabase.getReference(Skill_BaseInfo_Table).child(it.idSkill.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }
            // postSkillDescription
            L8Application.instance.db.skillTableQuery().getDescriptionAll().forEach {
                firebaseDatabase.getReference(Skill_Description_Table).child(it.idSkill.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }
            // postSkillMoreInfo
            L8Application.instance.db.skillTableQuery().getMoreInfoAll().forEach {
                firebaseDatabase.getReference(Skill_MoreInfo_Table).child(it.idSkill.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }
            // postSkillPhoto
            L8Application.instance.db.skillTableQuery().getPhotoAll().forEach {
                firebaseDatabase.getReference(Skill_Photo_Table).child(it.idSkill.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }
            // postSkillPlay
            L8Application.instance.db.skillTableQuery().getPlayAll().forEach {
                firebaseDatabase.getReference(Skill_Play_Table).child(it.idSkill.toString()).setValue(securityData.encrypt(gson.toJson(it), securityData.stringFromJNI()))
            }

        }
    }



}