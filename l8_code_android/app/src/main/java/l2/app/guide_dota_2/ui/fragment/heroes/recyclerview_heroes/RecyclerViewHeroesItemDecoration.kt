package l2.app.guide_dota_2.ui.fragment.heroes.recyclerview_heroes

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewHeroesItemDecoration(private var space: Int = -1) : RecyclerView.ItemDecoration() {


    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val params = view.layoutParams as RecyclerView.LayoutParams
        val position = params.viewAdapterPosition

        if (position < state.itemCount) {
            val spanCount = (parent.layoutManager as GridLayoutManager).spanCount
            if (position < spanCount) {
                outRect.set(0, space, 0, 0)
                // outRect.set(0, ScreenUtils.dpToPx(spaceDp), 0, 0)
            } else {
                if (position == state.itemCount - 1) {
                    outRect.set(0, 0, 0, space)
                } else {
                    outRect.set(0, 0, 0, 0)
                }
            }
        } else {
            outRect.setEmpty()
        }

    }

}

