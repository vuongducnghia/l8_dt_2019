package l2.app.guide_dota_2.ui.fragment.items.view_pager

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.FrameLayout
import android.widget.ImageView
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.table_sqlite_item.ItemPhoto
import l2.app.guide_dota_2.ui.fragment.items.Child
import l2.app.guide_dota_2.ui.fragment.items.ITEM_DETAIL_LOAD_DATA_LEFT
import l2.app.guide_dota_2.ui.fragment.items.ITEM_DETAIL_LOAD_DATA_RIGHT
import l2.app.guide_dota_2.util.BitmapUtils
import org.greenrobot.eventbus.EventBus

private var bodyItemTemp: FrameLayout? = null

class GridViewAdapter(private val list: List<ItemPhoto>) : BaseAdapter() {

    var isUp2Item: Boolean = false
    var isLayoutLeft: Boolean = true

    override fun getCount(): Int {
        return list.size
    }

    override fun getItem(position: Int): Any? {
        return list[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view: View?
        val gridItemViewHolder: GridItemViewHolder
        if (convertView == null) {
            view = LayoutInflater.from(parent!!.context).inflate(R.layout.fragment_item_view_holder, parent, false)
            gridItemViewHolder = GridItemViewHolder(view)
            view.tag = gridItemViewHolder
        } else {
            view = convertView
            gridItemViewHolder = view.tag as GridItemViewHolder
        }

        val itemPhoto = list[position]
        if (itemPhoto.dota2 != null) {
            gridItemViewHolder.imgItem.setImageBitmap(BitmapUtils.ByteToBitmap(itemPhoto.dota2!!))
            gridItemViewHolder.bodyItem.setOnClickListener {
                if (isUp2Item) {
                    if (isLayoutLeft) {
                        EventBus.getDefault().post(Child(ITEM_DETAIL_LOAD_DATA_RIGHT, null, itemPhoto.id_item!!))
                    } else {
                        EventBus.getDefault().post(Child(ITEM_DETAIL_LOAD_DATA_LEFT, null, itemPhoto.id_item!!))
                    }
                } else {
                    EventBus.getDefault().post(Child(ITEM_DETAIL_LOAD_DATA_LEFT, null, itemPhoto.id_item!!))

                    // Reset color default
                    bodyItemTemp?.isSelected = false

                    // Color selected
                    gridItemViewHolder.bodyItem.isSelected = true
                    bodyItemTemp = gridItemViewHolder.bodyItem
                }
            }
        }

        return view
    }


    class GridItemViewHolder(itemView: View) {
        var imgItem: ImageView = itemView.findViewById(R.id.imgView)
        var bodyItem: FrameLayout = itemView.findViewById(R.id.bodyItem)
    }
}