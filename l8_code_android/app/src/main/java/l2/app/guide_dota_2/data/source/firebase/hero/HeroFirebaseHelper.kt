package l2.app.guide_dota_2.data.source.firebase.hero

/**
 * Created by nghia.vuong on 23,June,2020
 */
class HeroFirebaseHelper : HeroFirebaseContract {


    override fun createHeroTable() {
        GetHero().getAll()
    }

    /*
    * Update or new create table in firebase.
    * */
    override fun postAll() {
        PostHero().postAll()
    }

    /*
   * Update or new insert a hero to firebase
   * */
    override fun postHero(id: String) {
        PostHero().postHero(id)
    }

    override fun postListIDHero() {
        PostHero().postListIDHero()
    }

    override fun getListIDHero(callBack: HeroFirebaseContract.ResultListID) {
        GetHero().getListID(callBack)
    }

    /*
   * Get the hero from firebase to update  a hero in sqlite
   * */
    override fun updateByFirebase(id: String) {
        GetHero().updateByFirebase(id)
    }

    override fun insertByFirebase(id: String) {
        GetHero().insertByFirebase(id)
    }


}