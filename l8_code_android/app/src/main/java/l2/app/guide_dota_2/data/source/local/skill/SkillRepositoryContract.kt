package l2.app.guide_dota_2.data.source.local.skill

import l2.app.guide_dota_2.data.model.respond.HeroDetailSkillRespond
import l2.app.guide_dota_2.data.source.local.hero.HeroRepositoryContract


interface SkillRepositoryContract {

    interface BaseResult {
        fun noData(){}
    }

    fun getSkillTable(callBack: ResultSkillTable)
    interface ResultSkillTable : BaseResult {
        fun result(data: List<String>)
    }

    fun getSkillOfHero(idHero: String, callBack: ResultListSkill)
    interface ResultListSkill : HeroRepositoryContract.BaseResult {
        fun result(data: List<HeroDetailSkillRespond>)
    }

    fun getSkillDetail(idSkill: String, callBack: ResultSkillDetail)
    interface ResultSkillDetail : HeroRepositoryContract.BaseResult {
        fun result(data: SkillRepository.SkillDetail)
    }

}
