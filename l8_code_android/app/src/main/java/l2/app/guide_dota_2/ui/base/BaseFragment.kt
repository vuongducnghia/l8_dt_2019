package l2.app.guide_dota_2.ui.base

import android.content.Context
import android.view.animation.Animation
import androidx.fragment.app.Fragment

abstract class BaseFragment : Fragment() {


    private val isDisableAnimation: Boolean = false

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    /*Fragment Disable animation*/
    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation? {
        if (isDisableAnimation) {
            val animation = object : Animation() {}
            animation.duration = 0
            return animation
        }
        return super.onCreateAnimation(transit, enter, nextAnim)
    }


}