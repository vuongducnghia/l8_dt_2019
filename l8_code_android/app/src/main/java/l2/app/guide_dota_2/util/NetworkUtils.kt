package l2.app.guide_dota_2.util

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.os.Build
import android.provider.Settings
import l2.app.guide_dota_2.BuildConfig
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.L8Application.Companion.appExecutors
import java.net.HttpURLConnection
import java.net.URL


object NetworkUtils {

    fun checkActiveInternetConnection(callBack: AppExecutors.CallBack) {
        appExecutors.networkIO.execute {
            val urlc = URL("https://www.google.com").openConnection() as HttpURLConnection
            urlc.setRequestProperty("User-Agent", "Test")
            urlc.setRequestProperty("Connection", "close")
            urlc.connectTimeout = 1500
            urlc.connect()
            val isSuccess = urlc.responseCode == 200
            appExecutors.mainThread.execute { callBack.result(isSuccess) }

        }
    }

    fun isNetworkConnected(context: Context): Boolean {
        // Checking internet connectivity
        val connectivityMgr = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityMgr.activeNetwork
        return activeNetwork != null
    }

    fun getDeviceInfo(): String { // android,OS8,ver4.3.2
        return "android,OS" + Build.VERSION.RELEASE + ",ver" + BuildConfig.VERSION_NAME
    }

    @SuppressLint("HardwareIds")
    fun getDeviceCode(): String { // androidId
        return Settings.Secure.getString(L8Application.instance.contentResolver, Settings.Secure.ANDROID_ID)
    }


}