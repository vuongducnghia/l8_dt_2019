package l2.app.guide_dota_2.util

import android.annotation.SuppressLint
import android.content.Context
import android.provider.Settings

object CommonUtils {

    const val TRANSITION_DELAY = 300L

    @SuppressLint("all")
    fun getDeviceId(context: Context): String =
            Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)

    fun getApplicationName(context: Context): String {
        val applicationInfo = context.applicationInfo
        val stringId = applicationInfo.labelRes
        return if (stringId == 0) {
            applicationInfo.nonLocalizedLabel.toString()
        } else {
            context.getString(stringId)
        }
    }
}