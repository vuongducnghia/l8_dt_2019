package l2.app.guide_dota_2.data.model.respond

import androidx.room.ColumnInfo
import l2.app.guide_dota_2.data.model.table_sqlite_hero.*

/**
 * Created by nghia.vuong on 04,March,2020
 */

class HeroDetailCounterRespond {
    @ColumnInfo(name = "id_hero_counter")
    var idHeroCounter: String? = null
    @ColumnInfo(name = "guide")
    var guide: String? = null
    @ColumnInfo(name = "guide_sp")
    var guideSP: String? = null
    @ColumnInfo(name = "guide_vi")
    var guideVI: String? = null
    @ColumnInfo(name = "dota2")
    var dota2: ByteArray? = null
    @ColumnInfo(name = "avatar")
    var avatar: ByteArray? = null
}
