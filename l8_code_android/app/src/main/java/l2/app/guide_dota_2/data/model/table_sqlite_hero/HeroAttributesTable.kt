package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "HeroAttributesTable")
class HeroAttributesTable {
    @PrimaryKey
    @ColumnInfo(name = "id_hero")
    @NonNull
    var idHero: String? = null // id not encode
    @ColumnInfo(name = "health")
    var health: String? = null
    @ColumnInfo(name = "mana")
    var mana: String? = null
    @ColumnInfo(name = "armor")
    var armor: String? = null
    @ColumnInfo(name = "damage")
    var damage: String? = null
    @ColumnInfo(name = "movement_speed")
    var movementSpeed: String? = null
}
