package l2.app.guide_dota_2.ui.fragment.detail_hero


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd
import kotlinx.android.synthetic.main.fragment_hero_detail_layout_content.*
import kotlinx.android.synthetic.main.fragment_hero_detail_layout_toolbar.*
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.respond.HeroDetailFragmentRespond
import l2.app.guide_dota_2.data.model.respond.HeroDetailSkillRespond
import l2.app.guide_dota_2.ui.base.BaseFragment
import l2.app.guide_dota_2.ui.fragment.detail_hero.adapters.SkillViewAdapter
import l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager.HeroDetailAttributes
import l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager.HeroDetailBuildItem
import l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager.HeroDetailCounter
import l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager.HeroDetailSkill
import l2.app.guide_dota_2.util.BitmapUtils
import l2.app.guide_dota_2.util.EventBusAction
import org.greenrobot.eventbus.EventBus


/**
 * A simple [Fragment] subclass.
 */
const val SKILL_DETAIL = "SKILL_DETAIL"


class HeroDetailFragment : BaseFragment(), HeroDetailContract.View {

    override lateinit var presenter: HeroDetailContract.Presenter
    override var isActive: Boolean = false
        get() = isAdded

    private lateinit var mInterstitialAd: InterstitialAd
    private var idSkillDetail: String? = null
    var idHeroDetail: String? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        presenter = HeroDetailPresenter(requireContext(), this)
        presenter.start()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        idHeroDetail = arguments?.getString("id_hero") ?: ""
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_hero_detail, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mInterstitialAd = InterstitialAd(requireContext())
        mInterstitialAd.adUnitId = "ca-app-pub-3940256099942544/1033173712"
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.adListener = object : AdListener() {
            override fun onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                // Code to be executed when an ad request fails.
            }

            override fun onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            override fun onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            override fun onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            override fun onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                findNavController().popBackStack()
            }
        }

        iconBack.setOnClickListener {
            /*if (mInterstitialAd.isLoaded) {
                rootLayoutHeroDetail.visibility = View.GONE
                mInterstitialAd.show()
            } else {
                findNavController().popBackStack()
            }*/
            findNavController().popBackStack()
        }

        val listViewPager = resources.getStringArray(R.array.view_pager_hero_detail)
        // viewPager.adapter = HeroDetailViewPagerAdapter(idHeroDetail!!, listViewPager, childFragmentManager)

        viewPager.adapter = object : FragmentStateAdapter(this) {
            override fun getItemCount(): Int {
                return listViewPager.size
            }

            override fun createFragment(position: Int): Fragment {
                return when (position) {
                    0 -> HeroDetailAttributes()
                    1 -> HeroDetailSkill()
                    2 -> HeroDetailCounter()
                    else -> HeroDetailBuildItem()
                }
            }

        }

        val imageCounter = counter.findViewById<ImageView>(R.id.imgView)
        imageCounter.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_counter))
        counter.setOnClickListener { viewPager.currentItem = 2 }
        val imageBuildItem = build_item.findViewById<ImageView>(R.id.imgView)
        imageBuildItem.setImageDrawable(ContextCompat.getDrawable(requireContext(), R.drawable.ic_build_item))
        build_item.setOnClickListener { viewPager.currentItem = 3 }

        idHeroDetail?.let {
            presenter.getHeroDetail(idHeroDetail!!)
            presenter.getListSkill(idHeroDetail!!)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.end()
    }

    override fun getHeroDetailFinish(heroDetailFrg: HeroDetailFragmentRespond) {
        txtNameHeroDetail.text = heroDetailFrg.name
        txtRole.text = heroDetailFrg.role
        imgAvatarHero.setImageBitmap(heroDetailFrg.avatar?.let { BitmapUtils.ByteToBitmap(it) })
        imgAvatarHero.setOnClickListener { viewPager.currentItem = 0 }
    }

    override fun getListSkillFinish(result: List<HeroDetailSkillRespond>) {
        if (result.size > 0) {
            idSkillDetail = result[0].idSkill ?: ""
            val adapter = SkillViewAdapter(result)
            gvSkill.adapter = adapter
            gvSkill.resetNumColumns()
            adapter.setOnClickListener(object : SkillViewAdapter.OnClickListener {
                override fun onItemClick(position: Int) {
                    idSkillDetail = result[position].idSkill ?: ""
                    viewPager.currentItem = 1
                    EventBus.getDefault().post(EventBusAction.SkillAdapterEventBus(SKILL_DETAIL, idSkillDetail ?: ""))
                }
            })
            EventBus.getDefault().post(EventBusAction.SkillAdapterEventBus(SKILL_DETAIL, idSkillDetail ?: ""))
        }
    }

}

class FilterHero {
    var attributes: String = ""
    var role: String = ""
    var liked: String = ""
}

