package l2.app.guide_dota_2.ui.fragment.heroes

import l2.app.guide_dota_2.data.model.respond.HeroesFragmentRespond
import l2.app.guide_dota_2.ui.base.BasePresenter
import l2.app.guide_dota_2.ui.base.BaseView
import l2.app.guide_dota_2.ui.fragment.detail_hero.FilterHero

/**
 * Created by nghia.vuong on 24,June,2020
 */
interface HeroesContract {

    interface Presenter : BasePresenter {
        fun postHero(id:String)
        fun getFilterHeroes(filter: FilterHero)
    }

    interface View : BaseView<Presenter> {
        fun getFilterHeroesFinish(data: List<HeroesFragmentRespond>)

    }
}