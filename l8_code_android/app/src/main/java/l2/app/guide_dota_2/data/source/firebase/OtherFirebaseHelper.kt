package l2.app.guide_dota_2.data.source.firebase

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.table_sqlite_other.NewData
import l2.app.guide_dota_2.util.AppExecutors
import l2.app.guide_dota_2.util.AppLog
import l2.app.guide_dota_2.util.ConstantTable
import java.lang.Exception

/**
 * Created by nghia.vuong on 23,June,2020
 */
class OtherFirebaseHelper : OtherFirebaseContract {

    private val TAG = "SkillFirebase"

    private var appExecutors: AppExecutors = AppExecutors()
    private var firebaseDatabase: FirebaseDatabase = FirebaseDatabase.getInstance()

    override fun createNewData() {
        // Get data
        appExecutors.networkIO.execute {
            val myRef = firebaseDatabase.getReference(ConstantTable.New_Data)
            myRef.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    AppLog.w(TAG, "Failed to read value.", error.toException())
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val listChildren = dataSnapshot.children
                    listChildren.forEach {
                        try {
                            val newData = it.getValue(NewData::class.java)
                            newData?.let { it1 -> L8Application.instance.db.otherTableQuery().insertNewData(it1) }
                        } catch (e: Exception) {
                        }
                    }
                }
            })
        }
    }

    override fun postNewData() {
        appExecutors.diskIO.execute {
            // Get new data
            val newData = L8Application.instance.db.otherTableQuery().getNewData()
            newData.forEach {
                firebaseDatabase.getReference(ConstantTable.New_Data)
                    .child(it.id.toString())
                    .setValue(it)
            }
        }
    }

    override fun getNewData(callBack: OtherFirebaseContract.ResultData) {
        // Get data
        appExecutors.networkIO.execute {
            val myRef = firebaseDatabase.getReference(ConstantTable.New_Data)
            myRef.addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    AppLog.w(TAG, "Failed to read value.", error.toException())

                    appExecutors.mainThread.execute {
                        callBack.noData("Failed to read value. ${error.toException()}")
                    }
                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val listNewData = ArrayList<NewData>()
                    val listChildren = dataSnapshot.children
                    listChildren.forEach {
                        val newdata = it.getValue(NewData::class.java)
                        if (newdata != null) {
                            listNewData.add(newdata)
                        }
                    }
                    appExecutors.mainThread.execute {
                        callBack.result(listNewData)
                    }
                }
            })
        }
    }
}