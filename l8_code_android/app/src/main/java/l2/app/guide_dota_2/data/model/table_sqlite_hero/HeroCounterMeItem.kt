package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "HeroCounterMeItem")
class HeroCounterMeItem {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "index")
    @NonNull
    var index: Long = 0 // Not encode index
    @ColumnInfo(name = "id_hero")
    var idHero: String? = null // Not encode idHero
    @ColumnInfo(name = "id_item")
    var idItem: String? = null
    @ColumnInfo(name = "guide")
    var guide: String? = null
    @ColumnInfo(name = "guide_sp")
    var guideSP: String? = null
    @ColumnInfo(name = "guide_vi")
    var guideVI: String? = null
}