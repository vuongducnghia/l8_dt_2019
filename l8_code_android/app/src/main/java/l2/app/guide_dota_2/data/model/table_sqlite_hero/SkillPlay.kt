package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SkillPlay")
class SkillPlay {
    @PrimaryKey
    @ColumnInfo(name = "id_skill")
    @NonNull
    var idSkill: String? = null

    @ColumnInfo(name = "play_skill")
    var play: String? = null

    @ColumnInfo(name = "play_skill_sp")
    var playSkillSp: String? = null

    @ColumnInfo(name = "play_skill_vi")
    var playSkillVi: String? = null

}