package l2.app.guide_dota_2.data.source.local.skill_dao

import androidx.room.Dao
import androidx.room.Query
import l2.app.guide_dota_2.data.model.table_sqlite_hero.*
 
@Dao
interface SkillTableDao {

    /*Insert*/
    /*Get*/
    @Query("SELECT * FROM Skill")
    fun getSkillTable(): List<String>

    @Query("SELECT * FROM SkillBaseInfo")
    fun getBaseInfoAll(): List<SkillBaseInfo>

    @Query("SELECT * FROM SkillDescription")
    fun getDescriptionAll(): List<SkillDescription>

    @Query("SELECT * FROM SkillMoreInfo")
    fun getMoreInfoAll(): List<SkillMoreInfo>

    @Query("SELECT * FROM SkillPhoto")
    fun getPhotoAll(): List<SkillPhoto>

    @Query("SELECT * FROM SkillPlay")
    fun getPlayAll(): List<SkillPlay>

    /*Update*/
    /*Delete*/
    @Query("DELETE FROM Skill")
    fun deleteSkillTable()

    @Query("DELETE FROM SkillBaseInfo")
    fun deleteBaseInfo()

    @Query("DELETE FROM SkillDescription")
    fun deleteDescription()

    @Query("DELETE FROM SkillMoreInfo")
    fun deleteMoreInfo()

    @Query("DELETE FROM SkillPhoto")
    fun deletePhoto()

    @Query("DELETE FROM SkillPlay")
    fun deletePlay()

}