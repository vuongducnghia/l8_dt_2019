package l2.app.guide_dota_2.ui.base

import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.os.PersistableBundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import l2.app.guide_dota_2.util.NetworkUtils

abstract class BaseActivity : AppCompatActivity() {

    /**
     * Variable
     */
    var isAlive = false
    var isTouchDisable: Boolean = false
    var isDisableDispatchTouchEvent: Boolean = false


    /**
     * Override
     */

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        fullScreen()
    }

    override fun onResume() {
        super.onResume()
        isAlive = true
    }

    override fun onPause() {
        super.onPause()
        isAlive = false
    }

    override fun dispatchTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN && !isDisableDispatchTouchEvent) {
            val view = currentFocus
            if (view != null && (view is AppCompatEditText || view is EditText)) {
                val outRect = Rect()
                view.getGlobalVisibleRect(outRect)
                if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                    view.clearFocus()
                    val imm = (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }
            }
        }
        return isTouchDisable || super.dispatchTouchEvent(event)
    }

    /**
     * DialogLoading
     */

    fun hideLoading() {

    }

    fun showLoading() {

    }

    /**
     * Keyboard
     */

    fun showKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(view, 2)
        }
    }

    fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    /**
     * Disposable
     */
    fun popBS() {
        val sfm = supportFragmentManager
        if (sfm.backStackEntryCount == 0) {
            return
        }
        sfm.popBackStackImmediate()
    }

    /**
     * Style Screen
     * */

    private fun fullScreen() {
        /*For Activity
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )*/

        /* For AppCompatActivity
        If you are using AppCompatActivity then you need to add new theme
        <style name="Theme.AppCompat.Light.NoActionBar.FullScreen" parent="@style/Theme.AppCompat.Light.NoActionBar">
            <item name="android:windowNoTitle">true</item>
            <item name="android:windowActionBar">false</item>
            <item name="android:windowFullscreen">true</item>
            <item name="android:windowContentOverlay">@null</item>
        </style>

        <activity android:name=".ActivityName"
            android:label="@string/app_name"
            android:theme="@style/Theme.AppCompat.Light.NoActionBar.Fullscreen"/>
        */
    }

}