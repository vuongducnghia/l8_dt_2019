package l2.app.guide_dota_2.ui.fragment.items.view_pager


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_item_upgrades.*
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.source.local.item.GroupItemObject
import l2.app.guide_dota_2.ui.fragment.items.Child
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe


/**
 * A simple [Fragment] subclass.
 */

const val ITEM_FRAG_UPGRADES_LOAD_DATA = "ITEM_FRAG_UPGRADES_DATA"
const val ITEM_FRAG_UPGRADES_VIEW_CREATED = "ITEM_FRAG_UPGRADES_VIEW_CREATED"

class ItemFragmentUpgrades : Fragment() {

    override fun onAttach(context: Context) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_item_upgrades, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        EventBus.getDefault().post(Child(ITEM_FRAG_UPGRADES_VIEW_CREATED, null))
    }


    private fun showItemUpgrades(items: GroupItemObject) {
        gvAccessories?.adapter = GridViewAdapter(items.accessories)
        gvSupport?.adapter = GridViewAdapter(items.support)
        gvMagical?.adapter = GridViewAdapter(items.magical)
        gvArmor?.adapter = GridViewAdapter(items.armor)
        gvWeapons?.adapter = GridViewAdapter(items.weapons)
        gvArtifacets?.adapter = GridViewAdapter(items.artifacets)
    }


    @Subscribe
    fun onEvent(event: Child) {
        if (event.message == ITEM_FRAG_UPGRADES_LOAD_DATA) {
            showItemUpgrades(event.items!!)
        }
    }


}
