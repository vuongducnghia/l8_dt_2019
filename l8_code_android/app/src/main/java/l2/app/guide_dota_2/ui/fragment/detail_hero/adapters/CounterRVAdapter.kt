package l2.app.guide_dota_2.ui.fragment.detail_hero.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.table_sqlite_hero.HeroBaseInfo
import l2.app.guide_dota_2.data.model.table_sqlite_hero.HeroCounterMe

/**
 * Created by nghia.vuong on 25,February,2020
 */
class CounterRVAdapter(private var data: List<HeroCounterMe>, private var dataImage: List<HeroBaseInfo>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.layout_rv_counter_me, parent, false))

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        //  holder.itemView.txtName.text = data[position].counter_m_id
        // holder.itemView.avatarHero.setImageBitmap(dataImage[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}