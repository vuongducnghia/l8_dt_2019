package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "Skill")
class Skill {
    @PrimaryKey
    @ColumnInfo(name = "id_skill")
    @NonNull
    var idSkill: String? = null
}