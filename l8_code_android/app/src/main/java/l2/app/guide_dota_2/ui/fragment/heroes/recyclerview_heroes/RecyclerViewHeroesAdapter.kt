package l2.app.guide_dota_2.ui.fragment.heroes.recyclerview_heroes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.layout_hero_item.view.*
import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.respond.HeroesFragmentRespond
import l2.app.guide_dota_2.util.BitmapUtils.ByteToBitmap
import l2.app.guide_dota_2.util.Constant.DEV
import l2.app.guide_dota_2.util.Constant.LIKED


class RecyclerViewHeroesAdapter(var data: List<HeroesFragmentRespond>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var firebaseDatabase = FirebaseDatabase.getInstance()

    override fun getItemCount(): Int = data.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = Holder(
        LayoutInflater.from(parent.context).inflate(R.layout.layout_hero_item, parent, false)
    )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val holder = holder.itemView
        val hero = data[position]

        // Name data
        holder.txtNameHero.text = hero.name

        if (hero.avatar != null) {
            val imgAvatarHero = holder.imgAvatarHero
            imgAvatarHero.setImageBitmap(ByteToBitmap(hero.avatar!!))
            imgAvatarHero.setOnClickListener {
                mOnClickListener.onItemClick(it, position, hero.idHero)
            }
        }

        // I like it.
        val keyLikedHero = "$LIKED${hero.idHero}"
        holder.vLiked.setOnClickListener {
            holder.vLiked.isSelected = !holder.vLiked.isSelected
            if (holder.vLiked.isSelected) {
                L8Application.instance.sharedHelper.setBoolean(keyLikedHero, true)
            } else {
                if (L8Application.instance.sharedHelper.contains(keyLikedHero)) L8Application.instance.sharedHelper.remove(keyLikedHero)
            }
        }

        holder.vLiked.isSelected = L8Application.instance.sharedHelper.contains(keyLikedHero)

        // Post hero
        if (L8Application.instance.activeBuilder == DEV) {
            holder.btnPostHero.visibility = View.VISIBLE
            holder.btnPostHero.setOnClickListener {
                mOnClickListener.onPostHero(hero.idHero)
            }
        } else {
            holder.btnPostHero.visibility = View.GONE
        }

    }

    /**
     * On click listener
     */
    interface OnClickListener {
        fun onItemClick(view: View, position: Int, idHero: String?)
        fun onPostHero(id: String)

    }

    /*Set onClickListener*/
    private lateinit var mOnClickListener: OnClickListener

    fun setOnClickListener(clickListener: OnClickListener) {
        mOnClickListener = clickListener
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView)

}