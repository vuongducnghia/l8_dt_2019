package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.*

@Entity(tableName = "Hero")
class Hero {
    @PrimaryKey
    @ColumnInfo(name = "id_hero")
    @NonNull
    var idHero: String? = null
}

