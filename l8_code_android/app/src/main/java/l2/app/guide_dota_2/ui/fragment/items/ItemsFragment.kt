package l2.app.guide_dota_2.ui.fragment.items


import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.fragment_item_layout.*
import kotlinx.android.synthetic.main.fragment_items.*
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.table_sqlite_item.ItemPhoto
import l2.app.guide_dota_2.data.source.local.item.GroupItemObject
import l2.app.guide_dota_2.data.source.local.item.ItemDetail
import l2.app.guide_dota_2.data.source.local.item.itemCount
import l2.app.guide_dota_2.ui.base.BaseFragment
import l2.app.guide_dota_2.ui.customs.CustomGridView
import l2.app.guide_dota_2.ui.fragment.items.view_pager.*
import l2.app.guide_dota_2.util.BitmapUtils
import l2.app.guide_dota_2.util.ScreenUtils
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe


/**
 * A simple [Fragment] subclass.
 */
const val ITEM_DETAIL_LOAD_DATA_LEFT = "ITEM_DETAIL_LOAD_DATA_LEFT"
const val ITEM_DETAIL_LOAD_DATA_RIGHT = "ITEM_DETAIL_LOAD_DATA_RIGHT"
const val ITEMS_OPENED = "ITEMS_OPENED"

class ItemsFragment : BaseFragment(), ItemsContract.View {

    override lateinit var presenter: ItemsContract.Presenter
    override var isActive: Boolean = false
        get() = isAdded

    private var bFirst = false

    override fun onAttach(context: Context) {
        super.onAttach(context)
        presenter = ItemsPresenter(requireContext(), this)
        presenter.start()
        EventBus.getDefault().register(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_items, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fragItemIconBack.setOnClickListener {
            findNavController().popBackStack()
        }


        val listViewPager = resources.getStringArray(R.array.view_pager_item_layout)
        viewPagerItems.adapter = ViewPagerAdapter(listViewPager, childFragmentManager)
        // viewPagerItems.setPagingEnabled(false)  // disable scrolling


        txtBasic.setOnClickListener {
            if (viewPagerItems.currentItem != 0) viewPagerItems.currentItem = 0
        }
        txtUpgrades.setOnClickListener {
            if (viewPagerItems.currentItem != 1) viewPagerItems.currentItem = 1
        }

        // Default
        if (!bFirst) {
            presenter.getItemBase()
            presenter.getItemDetail("id_clarity")
            showItemCount()
        }

        val adRequest: AdRequest = AdRequest.Builder().build()
        adView.loadAd(adRequest)
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.end()
    }

    @SuppressLint("SetTextI18n")
    private fun showItemCount() {
        Handler().postDelayed({
            if (itemCount > 0) {
                txtItemsOpened.visibility = View.VISIBLE
                txtItemsOpened.text = "[ $itemCount Items ]"
            } else {
                txtItemsOpened.visibility = View.GONE
            }
        }, 100)

    }


    @SuppressLint("SetTextI18n")
    private fun showItemDetail(data: ItemDetail, parent: View) {

        val imageItem: ImageView = parent.findViewById(R.id.imgView)
        val nameItem: TextView = parent.findViewById(R.id.txtNameItem)
        val activeInfo: TextView = parent.findViewById(R.id.txtActive)
        val propertyInfo: TextView = parent.findViewById(R.id.txtProperty)
        val up2Item: TextView = parent.findViewById(R.id.txtUp2Item)
        val upgradeBy: LinearLayout = parent.findViewById(R.id.llUpgradeBy)
        val upgradeTo: CustomGridView = parent.findViewById(R.id.gvUpgradeTo)

        // Set image item
        imageItem.setImageBitmap(BitmapUtils.ByteToBitmap(data.dota2!!))

        // Set name
        nameItem.text = data.name

        // Set active
        val description: String? = data.desciption?.en
        if (description.isNullOrEmpty()) {
            activeInfo.visibility = View.GONE
        } else {
            activeInfo.visibility = View.VISIBLE
            activeInfo.text = description
        }

        // Set property
        val value = data.property!![0].value
        val en = data.property!![0].en
        if (value.isNullOrEmpty() || en.isNullOrEmpty()) {
            propertyInfo.visibility = View.GONE
        } else {
            propertyInfo.visibility = View.VISIBLE
            propertyInfo.text = value + en
        }

        // Set up by
        upgradeBy.removeAllViews()
        data.upByItem.forEach {
            showItemPhotoSmall(it, upgradeBy, parent)
        }

        // Set up to
        //if (parent == id_item_layout_detail_right) {
        if (data.upToItem.size > 0) {
            up2Item.visibility = View.VISIBLE
            upgradeTo.visibility = View.VISIBLE
            val upgradeToGridViewAdapter = GridViewAdapter(data.upToItem)
            upgradeToGridViewAdapter.isUp2Item = true
            upgradeToGridViewAdapter.isLayoutLeft = parent == id_item_layout_detail_right
            upgradeTo.resetNumColumns()
            upgradeTo.adapter = upgradeToGridViewAdapter
        } else {
            up2Item.visibility = View.GONE
            upgradeTo.visibility = View.GONE
        }
        //}
    }

    private fun showItemPhotoSmall(itemPhoto: ItemPhoto, viewGroup: LinearLayout, parent: View) {
        val image = itemPhoto.dota2 ?: return

        val inflater = requireActivity().getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.item_view_holder_small, null)
        val imageView = view.findViewById<ImageView>(R.id.imgItemSmall)
        imageView.setImageBitmap(BitmapUtils.ByteToBitmap(image))
        imageView.setOnClickListener {
            val id = itemPhoto.id_item!!
            if (parent == id_item_layout_detail_right) {
                presenter.getItemDetailLeft(id)
            } else {
                presenter.getItemDetail(id)
            }

        }
        viewGroup.addView(view)
    }

    fun showDataNotAvailable() {}


    @Subscribe
    fun onEvent(event: Child) {
        when (event.message) {
            ITEM_FRAG_BASE_VIEW_CREATED -> presenter.getItemBase()
            ITEM_FRAG_UPGRADES_VIEW_CREATED -> presenter.getItemUpgrades()
            ITEM_DETAIL_LOAD_DATA_LEFT -> {
                if (event.idItem.isNotEmpty()) presenter.getItemDetail(event.idItem)
            }
            ITEM_DETAIL_LOAD_DATA_RIGHT -> {
                // Load data
                id_item_layout_detail_left.setOnClickListener {
                    // setOnClickListener to disable child click
                }
                presenter.getItemDetailLeft(event.idItem)
            }
        }
    }

    private fun showDetailLeft() {
        // Detail left go in
        if (id_item_layout_detail_left.x < 0) {
            ObjectAnimator.ofFloat(id_item_layout_detail_left, "translationX", 0f).apply {
                duration = 500
                start()
                disableTouch.visibility = View.VISIBLE

                // Go out
                disableTouch.setOnClickListener {
                    disableTouch.visibility = View.GONE

                    ObjectAnimator.ofFloat(
                        id_item_layout_detail_left,
                        "translationX",
                        -ScreenUtils.dpToPx(300f).toFloat()
                    ).apply {
                        duration = 200
                        start()
                    }
                }
            }
        }
    }


    override fun getItemBaseFinish(data: GroupItemObject) {
        EventBus.getDefault().post(Child(ITEM_FRAG_BASE_LOAD_DATA, data))
        bFirst = true
    }

    override fun getItemDetailFinish(data: ItemDetail) {
        if (data.dota2 != null) {
            val parent = id_item_layout_detail_right
            showItemDetail(data, parent!!)
        }
    }

    override fun getItemDetailLeftFinish(data: ItemDetail) {
        if (data.dota2 != null) {
            val parent = id_item_layout_detail_left
            showItemDetail(data, parent!!)
            showDetailLeft()
        }
    }

    override fun getItemUpgradesFinish(data: GroupItemObject) {
        EventBus.getDefault().post(Child(ITEM_FRAG_UPGRADES_LOAD_DATA, data))
    }
}

/*Event bus*/
class Child(val message: String, val items: GroupItemObject?, val idItem: String = "")
