package l2.app.guide_dota_2.data.source.local.other_table

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import l2.app.guide_dota_2.data.model.table_sqlite_other.NewData

@Dao
interface OtherTableDao {

    /*Insert*/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNewData(newData: NewData)


    /*Get*/
    @Query("SELECT * FROM NewData")
    fun getNewData(): List<NewData>


    /*Update*/


    /*Delete*/


}