package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SkillDescription")
class SkillDescription {
    @PrimaryKey
    @ColumnInfo(name = "id_skill")
    @NonNull
    var idSkill: String? = null

    @ColumnInfo(name = "description_skill")
    var description: String? = null

    @ColumnInfo(name = "description_skill_sp")
    var descriptionSp: String? = null

    @ColumnInfo(name = "description_skill_vi")
    var descriptionVi: String? = null
}