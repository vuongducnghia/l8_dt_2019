package l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_hero_detail_skill.*
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.source.local.hero.HeroRepository
import l2.app.guide_dota_2.data.source.local.skill.SkillRepository
import l2.app.guide_dota_2.data.source.local.skill.SkillRepositoryContract
import l2.app.guide_dota_2.ui.base.BaseFragment
import l2.app.guide_dota_2.ui.fragment.detail_hero.HeroDetailFragment
import l2.app.guide_dota_2.ui.fragment.detail_hero.SKILL_DETAIL
import l2.app.guide_dota_2.util.BitmapUtils
import l2.app.guide_dota_2.util.EventBusAction
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe

class HeroDetailSkill : BaseFragment() {

    private lateinit var parent: HeroDetailFragment
    private lateinit var dbHeroGetRepository: HeroRepository
    private lateinit var dbSkillRepository: SkillRepository

    override fun onAttach(context: Context) {
        super.onAttach(context)
        EventBus.getDefault().register(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = inflater.inflate(R.layout.fragment_hero_detail_skill, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadSkill()
    }

    override fun onDetach() {
        super.onDetach()
        EventBus.getDefault().unregister(this)
    }

    private var idSkillDetail: String = ""

    @Subscribe
    fun onEvent(event: EventBusAction.SkillAdapterEventBus) {
        if (event.idEvent == SKILL_DETAIL) {
            idSkillDetail = event.message.trim()
            loadSkill()
        }
    }

    private fun loadSkill() {
        if (idSkillDetail.length > 0) {
            dbSkillRepository.getSkillDetail(idSkillDetail, object : SkillRepositoryContract.ResultSkillDetail {

                override fun result(data: SkillRepository.SkillDetail) {
                    imgSkill.setImageBitmap(data.photo?.let { it1 -> BitmapUtils.ByteToBitmap(it1.dota2!!) })
                    txtNameSkill.text = data.baseInfo?.name ?: ""
                    txtPlaySkill.text = data.play?.play ?: ""
                    txtDescriptionSkill.text = data.description?.description ?: ""
                    txtInfoSkill.text = data.moreInfo?.info ?: ""
                    txtNoteSkill.text = data.baseInfo?.note ?: ""
                }

                override fun noData() {}

            })
        }
    }
}