package l2.app.guide_dota_2.data.source.firebase.item

/**
 * Created by nghia.vuong on 23,June,2020
 */
class ItemFirebaseHelper : ItemFirebaseContract {

    /*
    * 1. Get list id first
    * 2. Get item by id
    * */
    override fun createItemTable() {
        GetItem().getAll()
    }

    override fun postAll() {
       PostItem().postAll()
    }

    override fun postListIDItem() {
       PostItem().postListIDItem()
    }

    override fun postItem(id: String) {
       PostItem().postItem(id)
    }

    override fun updateByFirebase(id: String) {
        GetItem().updateByFirebase(id)
    }

    override fun insertByFirebase(id: String) {
        GetItem().insertByFirebase(id)
    }

    override fun getListIDItem(callBack: ItemFirebaseContract.ResultListIDItem) {
       GetItem().getListIDItem(callBack)
    }


}