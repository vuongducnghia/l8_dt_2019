package l2.app.guide_dota_2.ui.base


interface BaseView<T> {
    var presenter: T
    // The view may not be able to handle UI updates anymore
    var isActive: Boolean

    /*fun getAPIError(tag: String)
    fun getAPIFinish()*/
}