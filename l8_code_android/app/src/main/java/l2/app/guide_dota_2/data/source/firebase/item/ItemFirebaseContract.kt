package l2.app.guide_dota_2.data.source.firebase.item

/**
 * Created by nghia.vuong on 23,June,2020
 */
interface ItemFirebaseContract {

    fun createItemTable()
    fun postAll() // Post all table Item to firebase
    fun postListIDItem() // Post table id Item to firebase
    fun postItem(id: String) // Post one Item to firebase

    fun updateByFirebase(id: String) // Get all table whit a id_Item at firebase
    fun insertByFirebase(id: String) // Insert new item firebase to sqlite
    fun getListIDItem(callBack: ResultListIDItem) // Get data table Item at firebase
    interface ResultListIDItem : Result {
        fun result(data: List<String>)
    }

    interface Result {
        fun noData(message: String){}
    }

}