package l2.app.guide_dota_2.ui.base

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment

open class BaseDialog : DialogFragment() {

    open fun show(context: Context, tag: String) {
        if (context is AppCompatActivity) {
            val manager = context.supportFragmentManager
            val transaction = manager.beginTransaction()
            val prevFragment = manager.findFragmentByTag(tag)
            if (prevFragment != null) {
                transaction.remove(prevFragment)
            }
            transaction.addToBackStack(null)
            super.show(manager, tag)
        }
    }
}