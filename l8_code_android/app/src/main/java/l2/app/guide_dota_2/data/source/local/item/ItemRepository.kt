package l2.app.guide_dota_2.data.source.local.item

import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.table_sqlite_item.ItemDescription
import l2.app.guide_dota_2.data.model.table_sqlite_item.ItemNote
import l2.app.guide_dota_2.data.model.table_sqlite_item.ItemPhoto
import l2.app.guide_dota_2.data.model.table_sqlite_item.ItemProperty
import l2.app.guide_dota_2.data.source.local.L8Database
import l2.app.guide_dota_2.util.AppExecutors
import l2.app.guide_dota_2.util.Constant.ACCESSORIES
import l2.app.guide_dota_2.util.Constant.ARMOR
import l2.app.guide_dota_2.util.Constant.ARTIFACETS
import l2.app.guide_dota_2.util.Constant.ATTRIBUTES
import l2.app.guide_dota_2.util.Constant.CONSUMABLES
import l2.app.guide_dota_2.util.Constant.EQUIPMENT
import l2.app.guide_dota_2.util.Constant.MAGICAL
import l2.app.guide_dota_2.util.Constant.MISCELLANEOUS
import l2.app.guide_dota_2.util.Constant.SECRET_SHOP
import l2.app.guide_dota_2.util.Constant.SUPPORT
import l2.app.guide_dota_2.util.Constant.WEAPONS

var itemCount = 0

class ItemRepository : ItemRepositoryContract {

    private var db: L8Database = L8Application.instance.db
    private var appExecutors: AppExecutors = AppExecutors()

    private var dataItems = ArrayList<ItemPhoto>()


    init {
        initDataItem()
    }

    private fun initDataItem() {
        appExecutors.diskIO.execute {
            if (dataItems.size == 0) {
                dataItems = db.itemTableQuery().getPhotoAndGroup() as ArrayList<ItemPhoto>
                itemCount = db.itemJoinQuery().getItemCount()
            }
        }
    }

    override fun getItemTable(callBack: ItemRepositoryContract.ResultItemTable) {
        appExecutors.diskIO.execute {
            val data =  L8Application.instance.db.itemTableQuery().getItemTable()
            appExecutors.mainThread.execute {
                callBack.result(data)
            }
        }
    }

    override fun getItemBase(callBack: ItemRepositoryContract.ResultItemBase) {
        appExecutors.diskIO.execute {
            val data = loadItemPhotoAndGroup()
            appExecutors.mainThread.execute {
                callBack.result(data)
            }
        }
    }

    override fun getItemUpgrades(callBack: ItemRepositoryContract.ResultItemUpgrades) {
        appExecutors.diskIO.execute {
            val data = loadItemPhotoAndGroup()
            appExecutors.mainThread.execute {
                callBack.result(data)
            }
        }
    }

    override fun getItemDetail(idItem: String, callBack: ItemRepositoryContract.ResultItemDetail) {
        appExecutors.diskIO.execute {
            val data = ItemDetail()
            val itemBaseInfo = db.itemQuery().getBaseInfo(idItem)
            data.id_item = itemBaseInfo.id_item
            data.name = itemBaseInfo.name
            val itemPhoto = db.itemQuery().getPhoto(idItem)
            data.dota1 = itemPhoto.dota1
            data.dota2 = itemPhoto.dota2
            data.desciption = db.itemQuery().getDescription(idItem)
            data.note = db.itemQuery().getNote(idItem)
            data.property = db.itemQuery().getProperty(idItem)
            val itemUp = db.itemQuery().getUp(idItem)

            if (itemUp.upBy != null && itemUp.upBy != "0") {
                itemUp.upBy!!.split(",").forEach {
                    data.upByItem.add(db.itemQuery().getPhoto(it))
                }
            }

            if (itemUp.upTo != null && itemUp.upTo != "0") {
                itemUp.upTo!!.split(",").forEach {
                    data.upToItem.add(db.itemQuery().getPhoto(it))
                }
            }

            appExecutors.mainThread.execute {
                callBack.result(data)
            }
        }
    }

    private fun loadItemPhotoAndGroup(): GroupItemObject {
        val data = GroupItemObject()
        dataItems = db.itemTableQuery().getPhotoAndGroup() as ArrayList<ItemPhoto>
        data.consumables.addAll(dataItems.filter { it.group_item == CONSUMABLES })
        data.attributes.addAll(dataItems.filter { it.group_item == ATTRIBUTES })
        data.equipment.addAll(dataItems.filter { it.group_item == EQUIPMENT })
        data.miscellaneous.addAll(dataItems.filter { it.group_item == MISCELLANEOUS })
        data.secretShop.addAll(dataItems.filter { it.group_item == SECRET_SHOP })
        data.accessories.addAll(dataItems.filter { it.group_item == ACCESSORIES })
        data.support.addAll(dataItems.filter { it.group_item == SUPPORT })
        data.magical.addAll(dataItems.filter { it.group_item == MAGICAL })
        data.weapons.addAll(dataItems.filter { it.group_item == WEAPONS })
        data.armor.addAll(dataItems.filter { it.group_item == ARMOR })
        data.artifacets.addAll(dataItems.filter { it.group_item == ARTIFACETS })
        return data
    }

}

class GroupItemObject {
    var consumables = ArrayList<ItemPhoto>()
    var attributes = ArrayList<ItemPhoto>()
    var equipment = ArrayList<ItemPhoto>()
    var miscellaneous = ArrayList<ItemPhoto>()
    var secretShop = ArrayList<ItemPhoto>()
    var accessories = ArrayList<ItemPhoto>()
    var support = ArrayList<ItemPhoto>()
    var magical = ArrayList<ItemPhoto>()
    var weapons = ArrayList<ItemPhoto>()
    var armor = ArrayList<ItemPhoto>()
    var artifacets = ArrayList<ItemPhoto>()
}

class ItemDetail {
    var id_item: String? = null
    var name: String? = null
    var dota1: ByteArray? = null
    var dota2: ByteArray? = null
    var desciption: ItemDescription? = null
    var property: List<ItemProperty>? = null
    var note: ItemNote? = null
    var upByItem = ArrayList<ItemPhoto>()
    var upToItem = ArrayList<ItemPhoto>()
}




