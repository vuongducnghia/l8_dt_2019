package l2.app.guide_dota_2.data.source.local.item_dao

import androidx.room.*
import l2.app.guide_dota_2.data.model.table_sqlite_item.*

@Dao
interface ItemDao {

    /*Insert*/
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertItemTable(data: Item)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertBaseInfo(data: ItemBaseInfo)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDescription(data: ItemDescription)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertNote(data: ItemNote)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPhoto(data: ItemPhoto)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProperty(data: ItemProperty)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUp(data: ItemUp)

    /*Get*/
    @Query("SELECT * FROM ItemBaseInfo WHERE id_item like :idItem")
    fun getBaseInfo(idItem: String): ItemBaseInfo

    @Query("SELECT * FROM ItemDescription WHERE id_item like :idItem")
    fun getDescription(idItem: String): ItemDescription

    @Query("SELECT * FROM ItemNote WHERE id_item like :idItem")
    fun getNote(idItem: String): ItemNote

    @Query("SELECT * FROM ItemPhoto WHERE id_item like :idItem")
    fun getPhoto(idItem: String): ItemPhoto

    @Query("SELECT * FROM ItemProperty WHERE id_item like :idItem")
    fun getProperty(idItem: String): List<ItemProperty>

    @Query("SELECT * FROM ItemUp WHERE id_item like :idItem")
    fun getUp(idItem: String): ItemUp

    /*Update*/
    @Update
    fun updateBaseInfo(data: ItemBaseInfo)

    @Update
    fun updateDescription(data: ItemDescription)

    @Update
    fun updateNote(data: ItemNote)

    @Update
    fun updatePhoto(data: ItemPhoto)

    @Update
    fun updateProperty(data: ItemProperty)

    @Update
    fun updateUp(data: ItemUp)

    /*Delete*/
    @Delete
    fun deleteBaseInfo(data: ItemBaseInfo)

    @Delete
    fun deleteDescription(data: ItemDescription)

    @Delete
    fun deleteNote(data: ItemNote)

    @Delete
    fun deletePhoto(data: ItemPhoto)

    @Delete
    fun deleteProperty(data: ItemProperty)

    @Delete
    fun deleteUp(data: ItemUp)


}