package l2.app.guide_dota_2.data.source.local.item_dao

import androidx.room.Dao
import androidx.room.Query
import l2.app.guide_dota_2.data.model.table_sqlite_item.*

@Dao
interface ItemTableDao {

    /*Insert*/
    /*Get*/
    @Query("SELECT * FROM Item")
    fun getItemTable(): List<String>

    @Query("SELECT * FROM ItemBaseInfo")
    fun getBaseInfo(): List<ItemBaseInfo>

    @Query("SELECT * FROM ItemDescription")
    fun getDescription(): List<ItemDescription>

    @Query("SELECT * FROM ItemNote")
    fun getNote(): List<ItemNote>

    @Query("SELECT * FROM ItemPhoto")
    fun getPhotoAndGroup(): List<ItemPhoto>

    @Query("SELECT * FROM ItemProperty")
    fun getProperty(): List<ItemProperty>

    @Query("SELECT * FROM ItemUp")
    fun getUp(): List<ItemUp>
    /*Update*/

    /*Delete*/
    @Query("DELETE FROM Item")
    fun deleteItemTable()

    @Query("DELETE FROM ItemBaseInfo")
    fun deleteBaseInfo()

    @Query("DELETE FROM ItemDescription")
    fun deleteDescription()

    @Query("DELETE FROM ItemNote")
    fun deleteNote()

    @Query("DELETE FROM ItemPhoto")
    fun deletePhotoAndGroup()

    @Query("DELETE FROM ItemProperty")
    fun deleteProperty()

    @Query("DELETE FROM ItemUp")
    fun deleteItemUp()


}