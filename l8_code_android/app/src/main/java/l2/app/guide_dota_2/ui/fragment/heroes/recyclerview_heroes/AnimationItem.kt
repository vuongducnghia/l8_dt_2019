package l2.app.guide_dota_2.ui.fragment.heroes.recyclerview_heroes

data class AnimationItem(val name: String, val resourceId: Int)