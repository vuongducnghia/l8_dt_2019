package l2.app.guide_dota_2.data.source.local.other_table

import l2.app.guide_dota_2.L8Application
import l2.app.guide_dota_2.data.model.table_sqlite_other.NewData
import l2.app.guide_dota_2.util.AppExecutors


class OtherRepository : OtherRepositoryContract {

    private var TAG = "OtherRepository"

    private var appExecutors: AppExecutors = AppExecutors()

    override fun getNewData(callBack: OtherRepositoryContract.ResultData) {
        appExecutors.diskIO.execute {
            val newDataSqlite = L8Application.instance.db.otherTableQuery().getNewData()
            appExecutors.mainThread.execute {
                callBack.result(newDataSqlite)
            }
        }
    }

    override fun insertNewData(newData: NewData) {
        appExecutors.diskIO.execute {
            L8Application.instance.db.otherTableQuery().insertNewData(newData)
        }
    }


}
