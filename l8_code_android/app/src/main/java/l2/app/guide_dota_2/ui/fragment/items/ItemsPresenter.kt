package l2.app.guide_dota_2.ui.fragment.items

import android.content.Context
import l2.app.guide_dota_2.data.source.local.item.GroupItemObject
import l2.app.guide_dota_2.data.source.local.item.ItemDetail
import l2.app.guide_dota_2.data.source.local.item.ItemRepository
import l2.app.guide_dota_2.data.source.local.item.ItemRepositoryContract

/**
 * Created by nghia.vuong on 24,June,2020
 */
class ItemsPresenter(var context: Context?, var view: ItemsContract.View) : ItemsContract.Presenter {

    private var db: ItemRepository

    init {
        view.presenter = this
        db = ItemRepository()
    }

    override fun start() {

    }

    override fun end() {

    }

    override fun getItemBase() {
        if (context != null && view.isActive) {
            db.getItemBase(object : ItemRepositoryContract.ResultItemBase {
                override fun result(data: GroupItemObject) {
                    view.getItemBaseFinish(data)
                }

                override fun noData() {}

            })
        }
    }


    override fun getItemUpgrades() {
        if (context != null && view.isActive) {
            db.getItemUpgrades(object : ItemRepositoryContract.ResultItemUpgrades {
                override fun result(data: GroupItemObject) {
                    view.getItemUpgradesFinish(data)
                }

                override fun noData() {}

            })
        }
    }

    override fun getItemDetail(id: String) {
        if (context != null && view.isActive) {
            db.getItemDetail(id, object : ItemRepositoryContract.ResultItemDetail {
                override fun result(data: ItemDetail) {
                    view.getItemDetailFinish(data)
                }

                override fun noData() {}

            })
        }
    }

    override fun getItemDetailLeft(id: String) {
        if (context != null && view.isActive) {
            db.getItemDetail(id, object : ItemRepositoryContract.ResultItemDetail {
                override fun result(data: ItemDetail) {
                    view.getItemDetailLeftFinish(data)
                }

                override fun noData() {}

            })
        }
    }

}