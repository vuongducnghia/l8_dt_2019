package l2.app.guide_dota_2.data.model.table_sqlite_item

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ItemProperty")
class ItemProperty {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id_auto")
    @NonNull
    var idAuto: Int? = null
    @ColumnInfo(name = "id_item")
    var id_item: String? = null
    @ColumnInfo(name = "value")
    var value: String? = null
    @ColumnInfo(name = "en")
    var en: String? = null
    @ColumnInfo(name = "sp")
    var sp: String? = null
    @ColumnInfo(name = "vi")
    var vi: String? = null
}