package l2.app.guide_dota_2.ui.fragment.detail_hero.view_pager

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_hero_detail_build_item.*
import l2.app.guide_dota_2.R
import l2.app.guide_dota_2.data.model.respond.HeroDetailBuildItemsRespond
import l2.app.guide_dota_2.data.source.local.hero.HeroRepository
import l2.app.guide_dota_2.data.source.local.hero.HeroRepositoryContract
import l2.app.guide_dota_2.ui.base.BaseFragment
import l2.app.guide_dota_2.ui.fragment.detail_hero.HeroDetailFragment
import l2.app.guide_dota_2.ui.fragment.detail_hero.adapters.BuildItemAdapter

class HeroDetailBuildItem : BaseFragment() {

    private lateinit var parent: HeroDetailFragment
    private lateinit var dbHeroGetRepository: HeroRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        parent = (parentFragment as HeroDetailFragment)
        dbHeroGetRepository = HeroRepository()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_hero_detail_build_item, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        parent.idHeroDetail?.let {
            dbHeroGetRepository.getHeroBuildItems(parent.idHeroDetail!!, object : HeroRepositoryContract.ResultHeroBuildItems {
                override fun result(data: HeroDetailBuildItemsRespond) {
                    val adapterStarting = data.listStarting?.let { it1 -> BuildItemAdapter(it1) }
                    gvStarting.adapter = adapterStarting
                    // gvStarting.resetNumColumns()

                    val adapterEarly = data.listEarly?.let { it1 -> BuildItemAdapter(it1) }
                    gvEarly.adapter = adapterEarly
                    // gvEarly.resetNumColumns()

                    val adapterCode = data.listCode?.let { it1 -> BuildItemAdapter(it1) }
                    gvCode.adapter = adapterCode
                    // gvCode.resetNumColumns()

                    val adapterSituational = data.listSituational?.let { it1 -> BuildItemAdapter(it1) }
                    gvSituational.adapter = adapterSituational
                    //  gvSituational.resetNumColumns()
                }

                override fun noData() {}

            })


        }
    }
}