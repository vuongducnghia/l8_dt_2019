package l2.app.guide_dota_2.data.model.table_sqlite_hero

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "HeroTalent")
class HeroTalent {

    @PrimaryKey
    @ColumnInfo(name = "id_hero")
    @NonNull
    var idHero: String? = null // Not encode idHero

    @ColumnInfo(name = "lv_10_a")
    var lv10a: String? = null
    @ColumnInfo(name = "lv_10_b")
    var lv10b: String? = null

    @ColumnInfo(name = "lv_15_a")
    var lv15a: String? = null
    @ColumnInfo(name = "lv_15_b")
    var lv15b: String? = null

    @ColumnInfo(name = "lv_20_a")
    var lv20a: String? = null
    @ColumnInfo(name = "lv_20_b")
    var lv20b: String? = null

    @ColumnInfo(name = "lv_25_a")
    var lv25a: String? = null
    @ColumnInfo(name = "lv_25_b")
    var lv25b: String? = null

}
