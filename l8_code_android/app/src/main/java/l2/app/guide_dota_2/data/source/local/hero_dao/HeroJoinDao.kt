package l2.app.guide_dota_2.data.source.local.hero_dao

import androidx.room.Dao
import androidx.room.Query
import l2.app.guide_dota_2.data.model.respond.HeroDetailCounterRespond
import l2.app.guide_dota_2.data.model.respond.HeroDetailFragmentRespond
import l2.app.guide_dota_2.data.model.respond.HeroesFragmentRespond

@Dao
interface HeroJoinDao {

    @Query("SELECT HeroBaseInfo.*, HeroPhotos.avatar, HeroRole.en, HeroRole.vi, HeroRole.sp FROM HeroBaseInfo INNER JOIN HeroPhotos ON HeroBaseInfo.id_hero = HeroPhotos.id_hero INNER JOIN HeroRole ON HeroBaseInfo.id_hero = HeroRole.id_hero")
    fun getHeroesFragment(): List<HeroesFragmentRespond>

    @Query("SELECT HeroBaseInfo.*, HeroPhotos.avatar, HeroRole.en, HeroRole.vi, HeroRole.sp FROM HeroBaseInfo INNER JOIN HeroPhotos ON HeroBaseInfo.id_hero = HeroPhotos.id_hero INNER JOIN HeroRole ON HeroBaseInfo.id_hero = HeroRole.id_hero WHERE HeroBaseInfo.id_hero like :idHero")
    fun getHeroDetailFragmentRespond(idHero: String): HeroDetailFragmentRespond

    @Query("SELECT HeroCounterMe.*, HeroPhotos.dota2, HeroPhotos.avatar FROM HeroCounterMe INNER JOIN HeroPhotos ON HeroCounterMe.id_hero_counter = HeroPhotos.id_hero WHERE HeroCounterMe.id_hero like :idHero")
    fun getHeroCounterMe(idHero: String): List<HeroDetailCounterRespond>

    @Query("SELECT HeroCounterMeItem.*, ItemPhoto.dota2 FROM HeroCounterMeItem INNER JOIN ItemPhoto ON HeroCounterMeItem.id_item = ItemPhoto.id_item WHERE id_hero like :idHero")
    fun getHeroCounterMeItem(idHero: String): List<HeroDetailCounterRespond>

    @Query("SELECT HeroCounterYou.*, HeroPhotos.dota2, HeroPhotos.avatar FROM HeroCounterYou INNER JOIN HeroPhotos ON HeroCounterYou.id_hero_counter = HeroPhotos.id_hero WHERE HeroCounterYou.id_hero like :idHero")
    fun getHeroCounterYou(idHero: String): List<HeroDetailCounterRespond>

    @Query("SELECT count(*) FROM Hero")
    fun getHeroCount(): Int


}