package l2.app.guide_dota_2.data.model.table_sqlite_item

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "ItemUp")
class ItemUp {
    @PrimaryKey
    @ColumnInfo(name = "id_item")
    @NonNull
    var id_item: String? = null
    @ColumnInfo(name = "from_item")
    var upBy: String? = null
    @ColumnInfo(name = "to_item")
    var upTo: String? = null
}